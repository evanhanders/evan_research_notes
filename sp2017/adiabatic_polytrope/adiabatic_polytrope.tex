\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{Creating an Adiabatic Polytrope}
\cfoot{\thepage}
\rfoot{Feb. 2, 2017}

\newcommand{\FT}{\ensuremath{\text{FT}}}
\newcommand{\grad}{\ensuremath{\nabla}}

\begin{document}
\section{The new adiabatic polytrope}
This still has all of the main pieces of the old polytrope, but it's set up in a much more clever way.
So we have
\begin{equation}
\begin{split}
T(z) = T_i (z_0- z) \\
\rho(z) = \rho_i (z_0 - z)^{m_{ad} - \epsilon}
\end{split}
\end{equation}
as our initial density and temperature profiles, but we split things up a bit.  The trick is to make BOTH
our background conditions (sub 0) AND our perturbation conditions (sub 1) of the polytropic form.

We have:
\begin{equation}
\begin{split}
T = T_0 + T_1 \\
\rho = \rho_0 \rho_1
\end{split}
\end{equation}
The background conditions will be a perfect polytrope: they'll be in hydrostatic equilibrium, thermal equilibrium,
and they'll take the form:
\begin{equation}
\begin{split}
T_0(z) = T_b (z_0- z) \\
\rho_0(z) = \rho_b (z_0 - z)^{m_{ad}}.
\end{split}
\end{equation}

The perturbations will generally be of the same form, but will cause superadiabaticity.
\begin{equation}
\begin{split}
T_1(z) = T_p (z_0- z) \\
\rho_1(z) = \rho_p (z_0 - z)^{-\epsilon}.
\end{split}
\end{equation}

The difficult thing is not the general form, but getting the specific values of $T_b$, $T_p$, $\rho_b$, $\rho_p$.

At this point, I want to note that \emph{I've been uncomfortable with an observation Mark and Axel have made about
our polytropes}: different epsilons have different quantities of mass.  The easy fix for this? \emph{peg the length
of the atmosphere using the adiabatic background, and then adjust $\rho_b$ so that different values of $\epsilon$
all have the same mass and have the same CHANCE of being apples-to-apples comparable}.  So the depth of the atmosphere
will be:
$$
L_z = e^{n_{\rho}/m_{ad}} - 1.
$$
Always, period.  We can then integrate the mass of the background (adiabatic) polytrope and the mass of the (full)
polytrope, and find that
$$
\rho_p = \left(\frac{m_{ad} + 1 - \epsilon}{m_{ad} + 1}\right)\left( \frac{e^{n_{\rho}(m_{ad} + 1)/m_{ad}} - 1}{e^{n_{\rho}(m_{ad} + 1 - \epsilon)/m_{ad}} - 1}\right).
$$
This way, regardless of what $\rho_b$ is, we \emph{don't change the mass of the atmosphere} by changing the superadiabaticity.
Great.  Then, for simplicity, let's just always set
$$
\rho_b = 1.
$$

This still leaves us with $T_p$ and $T_b$. If the background satisfies hydrostatic equilibrium,
$$
T_b = \frac{g}{R}\frac{1}{(m_{ad} + 1)}.
$$
If the \emph{whole system} satisfies hydrostatic equilibrium, then
$$
T_p = T_b \frac{\epsilon}{m_{ad} + 1 - \epsilon}.
$$
I now make the choice to peg the TOTAL temperature to have a value of 1 at the top of the atmosphere, just as
we used to.  This gives us
$$
T_b = \frac{m_{ad} + 1 - \epsilon}{m_{ad} + 1},
$$
and
$$
T_p = \frac{\epsilon}{m_{ad} + 1}.
$$
This choice coincidentally also gives us the same gravity as we used to have:
$$
g = R(m_{ad} + 1 - \epsilon),
$$
where we'll set $R = 1$ again, because it's easy to do so.

\textbf{There's one remaining difficulty, here}.  We used to have constant conductive flux, but we no longer do, because
the \emph{background} is in thermal equilibrium but the whole polytrope isn't.  Never fear!  Recall that we used to do
$$
F_{cond} = -\rho_0 \frac{\chi_{top}}{\rho_0} \grad T_0,
$$
and in the case of constant Prandtl with $\grad T_0 = -1$,
$$
F_{cond} = \chi_{top} = \sqrt{\frac{g L_z^3 (\Delta S/c_P)}{\text{Ra Pr}}}.
$$
So basically we set the value of the conductive flux via chi and, by proxy, via the definition of the Rayleigh number.
We want to do something equivalent.  In our new atmosphere, we have
$$
F_{cond} = -\rho_0\rho_1 \frac{chi_{top}}{\rho_0} \grad T.
$$
Fortunately, we set $\grad T = -1$ again, so really what we end up with is
$$
F_{cond} = \rho_p (z_0 - z)^{-\epsilon} \chi_{top}.
$$
Yes, this is a function of height which is bad, but we knew that our system wasn't stable to begin with, and
the initial state is just something that conditions our numerics anyways.  So we take a height average of this
by integrating and dividing by the depth to get basically the `average' conductive flux, and we get this out of
the Rayleigh number and prandtl number like before:
$$
F_{cond} = \chi_{top}\rho_p \frac{(L_z + 1)^{1 - \epsilon} - 1}{L_z(1-\epsilon)} = \sqrt{\frac{g L_z^3 (\Delta S/c_P)}{\text{Ra Pr}}}.
$$
Or,
$$
\chi_{top} = \frac{1}{\rho_p}\sqrt{\frac{g L_z^3 (\Delta S/c_P)}{\text{Ra Pr}}}\frac{L_z(1-\epsilon)}{(L_z + 1)^{1 - \epsilon} - 1}
$$

\subsection{Summary}
So in summary, we construct the following atmosphere:
\begin{equation}
\begin{split}
T = T_b(z_0 - z) + T_p(z_0 - z) \\
\rho = \left[\rho_b(z_0 - z)^{m_{ad}}\right]\left[\rho_p(z_0 - z)^{m_{ad}}\right].
\end{split}
\end{equation}
As is usual, this is an ideal gas with $P = R\rho T$.  In order for this atmosphere to work, you need to
set it up using the following values:
\begin{equation}
\begin{split}
T_b &= \frac{m_{ad} + 1 - \epsilon}{m_{ad} + 1} \\
T_p &= \frac{\epsilon}{m_{ad} + 1} \\
\rho_b &= 1 \\
\rho_p &= T_b\left( \frac{e^{n_{\rho}(m_{ad} + 1)/m_{ad}} - 1}{e^{n_{\rho}(m_{ad} + 1 - \epsilon)/m_{ad}} - 1}\right)\\
R &= 1 \\
g &= (m_{ad} + 1 - \epsilon) \\
L_z &= e^{n_\rho/m_{ad}} - 1 \\
\Delta S/c_P &= \frac{\gamma-1}{\gamma}\epsilon\ln(L_z + 1) \,\,\,\,\,\,\,\text{same form as before} \\
\chi_{top} &= \frac{1}{\rho_p}\sqrt{\frac{g L_z^3 (\Delta S/c_P)}{\text{Ra Pr}}}\frac{L_z(1-\epsilon)}{(L_z + 1)^{1 - \epsilon} - 1}\\
\nu_{top} &= \chi_{top}\text{Pr} \\
\chi &= \chi_{top}/\rho_0 \\
\nu &= \nu_{top}/\rho_0 \,\,\,\,\,\,(\text{constant Pr case}) \\
\end{split}
\end{equation}
Using these formulae, specification of $n_\rho, \epsilon,$ Ra, and Pr will create a polytropic atmosphere.  All
atmospheres of a given $n_\rho$ will have the \emph{same mass}, rather than the same number of initial density scale
heights, as we know that high epsilon cases slump a LOT pretty quickly.


\newpage
\section{The old polytrope}
I'm straight up lifting this from my notes a year ago.  This is how we used to set up polytropes, and we did it
ENTIRELY in the `0's and left the `1's open.  This is easy, but it had some issues.  Anyways, a polytrope looks like
\begin{equation}
\begin{split}
T_0 &= T_{t}(z_0 - z) \\
\rho_0 &= \rho_{t}(z_0 - z)^m \\
P_0 &= P_{t}(z_0 - z)^{m+1}.
\end{split}
\end{equation}
Where all of the ``$t$'' subscripts refer to the values of those variables at
the top of my domain.  Here, $m$ is the polytropic index and $z_0 = L_z + 1$, where
$L_z$ is the depth of the domain.  Due to our non-dimensionalized ideal gas law,
We used to set $P_{t} = \rho_{t} = T_{t} = 1$.

Using the entropy equation, you can show that in an adiabatic system, 
with $\nabla S = 0$, 
\begin{equation}
m_{ad} = \frac{1}{\gamma - 1} = \frac{3}{2} \text{ for }\gamma = \frac{5}{3}.
\end{equation}

Thus, we can use this value to determine when a system is adiabatic,
subadiabatic, or superadiabatic.  For $\gamma = 5/3$,
\begin{equation}
m = \left\{
\begin{array}{lr}
1.5 - \epsilon & \nabla S < 0\text{, superadiabatic} \\
1.5 & \nabla S = 0\text{, adiabatic} \\
1.5 + \Delta & \nabla S > 0\text{, subadiabatic}
\end{array}
\right.,
\end{equation}
where $\epsilon$ is some small positive value and $\Delta$ is some positive
value of any magnitude.  We always ran things with $\epsilon$ positive, so it was
superadiabatic.

We specified Ra, Pr, $n_{\rho}$, and $\epsilon$, and then we let things run.  We
calculated the depth of the domain from $n_{\rho}$ and $\epsilon$ based on the
density profile as $L_z = e^{n_{\rho}/m} - 1$.  Then we got diffusivites from Ra
and Pr and let it run.

This worked fine, but honestly when $\epsilon$ was small it had some pretty pitiful
performance compared to when $\epsilon$ was a nice value. And thus, this new adiabatic
polytrope was born.



\end{document}
