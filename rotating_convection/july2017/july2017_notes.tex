%\documentclass[letterpaper,12pt]{article}
\documentclass{aastex6}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}
\usepackage{cancel}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{Summer notes on rotating convection}
\cfoot{\thepage}
\rfoot{July 2017}

\newcommand{\FT}{\ensuremath{\text{FT}}}
\newcommand{\Div}[1]{\ensuremath{\nabla\cdot\bm{#1}}}
\newcommand{\eqs}{\begin{equation}}
\newcommand{\eqe}{\end{equation}}
\newcommand{\angles}[1]{\ensuremath{\left\langle #1 \right\rangle}}
\newcommand{\grad}{\ensuremath{\nabla}}
\newcommand{\RB}{Rayleigh-B\'{e}nard }


\begin{document}
\section{Project Outline thoughts}
OK, so here's the sketch of things I could do right now based on my
current understanding.
\begin{enumerate}
\item Figure out how boundary layers in polytropes scale.
\item Figure out how rotational (ekman) boundary layers scale (this would be no-slip).
Is no-slip even worth looking at?  I think so, to see if the scaling there is the
same as in non-stratified (6/5 according to \cite{king&all2009}).
\item Calculate transitional nusselt number at various values of Ek.
\item Calculate onset at various values of Ek.
\item Run sims that are clearly in the not rotationally constrained regime,
and others that are in the clearly rotationally constrained regime.
\item Compare Nu scalings to one another, and to the RB case.
\end{enumerate}

\section{Free-fall velocity in \RB}
I \emph{finally} managed to scrounge up something that gave me an idea of how
to actually get the free-fall velocity (some conference proceeding from
1969 that had a vague sentence that got me started and then I had to do math).
Basically the idea is this.  If you have the Boussinesq momentum equation:
$$
\frac{D \bm{u}}{Dt} = -\grad P - \alpha g T_1 \hat{z} + \nu \grad^2 \bm{u},
$$
and you're not worried about the $P$ term or the viscous term (we're just looking
at a free-falling particle), then you get the following problem:

We have a momentum equation of the form:
$$
\frac{dw}{dt} = -\alpha T_1 g,
$$
where this is basically $a = F / m$ (Newton's second law, rearranged).
If you have a box which is temperature $T_t$ at the top and temperature
$T_b$ at the bottom, and there is a linear profile connecting the two, then
the temperature profile is
$$
T(z) =  T_b + \Delta T\frac{z}{L},
$$
where $\Delta T \equiv T_t - T_b$.
A particle detaching from the top of the atmosphere and falling has a temperature
difference (compared to the background)
$$
T_1 = T_t - T(z) = \Delta T \left( 1 - \frac{z}{L}\right).
$$

Looking back at our acceleration,
\begin{equation}
\frac{dw}{dt} = -\alpha g \Delta T \left( 1 - \frac{z}{L}\right),
\end{equation}
and recalling from physics 1 that if you start with no energy (which
this parcel does), and then you add \emph{work} to the parcel, you
can get the final kinetic energy.  In other words,
\begin{equation}
0 + \text{work} = \frac{1}{2} m v_{ff}^2
\end{equation}
is how we'll get the free-fall velocity.  Work is defined:
\begin{equation}
\text{work} = \int \bm{f}\cdot d\bm{r}.
\end{equation}
For our problem,
\begin{equation}
\frac{1}{2} v_{ff}^2 = \int \frac{F}{m} dz = \int_L^0 \frac{dw}{dt} dz.
\end{equation}
Doing the integral on the RHS, we get
$$
\int_L^0 \left(1 - \frac{z}{L}\right)dz = -\frac{L}{2}.
$$
Plugging in, we get
$$
\frac{1}{2} v_{ff}^2 = \frac{1}{2}\alpha g \Delta T L,
$$
or the classic value that I keep seeing in every paper,
\begin{equation}
\boxed{
v_{ff} = \sqrt{\alpha g L \Delta t}}.
\end{equation}

\subsection{Non-dimensionalizing Boussinesq equations on free-fall time}
Here, we're going to non-dimensionalize our equations on the free-fall time, such
that our non-dimensionalization is
\begin{equation}
\begin{split}
\bm{u}^* &= v_{ff} \bm{u} \\
\grad^*  &= \frac{1}{L} \grad \\
t^*      &= \tau t \\
T_1^*    &= \Delta T T_1 \\
\text{Ra}&= \frac{v_{ff}^2 L^2}{\nu \chi} \\
\text{Pr}&= \frac{\nu}{\chi},
\end{split}
\end{equation}
where here, the non-dimensional length scale $\tau$ is the crossing time of a
particle traveling at the free-fall velocity, $v_{ff}$ over a distance of the
depth of the atmosphere, $L$.  Here, things with $*$ are dimension-full.  The
dimension-full Boussinesq equations of momentum and energy are
\begin{equation}
\begin{split}
\frac{\partial \bm{u}^*}{\partial t^*} + \bm{u}^*\cdot \grad^* \bm{u}^* =
   -\grad^* P^* - \alpha T_1^* g \hat{z} + \nu (\grad^*)^2 \bm{u}^*, \\
\frac{\partial T_1^*}{\partial t^*} + \bm{u}^*\cdot \grad^* T_1^* = \chi (\grad^*)^2 T_1^*,
\end{split}
\end{equation}
I'm going to just magically say that the $P$ term is non-dimensional, because it's just
basically there to set up the system properly (nothing ever updates it, it's initial
condition based, and it just does what we tell it to).

For the momentum equation, non-dimensionalize everything.  Then, multiply both sides by $L$.
Then, divide both sides by $v_{ff}^2$.  Then multiply the diffusion term by $\sqrt{\chi/\chi}$.

For the energy equation, non-dimensionalize everything, multiply both sides by $\tau$,
then multiply the diffusion term by $\sqrt{\nu/\nu}$.

By doing both of these, you retrieve
\begin{equation}
\begin{split}
\frac{\partial \bm{u}}{\partial t} + \bm{u}\cdot \grad \bm{u} =
   -\grad P - T_1 \hat{z} + \sqrt{\frac{\text{Pr}}{\text{Ra}}} \grad^2 \bm{u}, \\
\frac{\partial T_1}{\partial t} + \bm{u}\cdot \grad T_1 =
   \frac{1}{\sqrt{\text{Pr}\cdot\text{Ra}}}\grad^2 T_1^*,
\end{split}
\end{equation}




\section{Notes on Published Papers}
\subsection{Rotating \RB studies}
\subsubsection{Notes on \cite{king&all2009}}
\begin{enumerate}
\item The transition from rotationally-constrained to non-rotationally-constrained
is determined by the relative thicknesses of the thermal and Ekman boundary layers.
\item Conventional knowledge is that rotational effects dominate convection
dynamics when ratio of global buoyancy force to coriolis force is less than 1.  But..
see above point.
\item For $Nu \propto Ra^{\alpha}$, many non-rotating studies yield $\alpha = 2/7$,
and some rotating studies find it, too, but others fine $\alpha = 6/5$.  This
discrepancy can be explained by boundary layer dynamics.
\item There's a good description of bulk vs. boundary layer in the 4th paragraph.
\item In non-rotating fluids, the Nusselt number is proportional to the inverse
of the thermal boundary layer thickness.  The Ekman layer is the important
layer in rotating fluids, and $\delta_{E} \propto E^{1/2}D$.
\item The smallest boundary layer is the one that is NOT part of the bulk, and
it therefore is not affected by the bulk, and so we have to consider the entirety
of its effect.  Check out first lines of second page.
\item By equating thermal and ekman boundary layers, you can figure out Nu at which
rotation starts dominating.
\item Non-rotating agree with $Ra^{2/7}$ in cylinders and in numerical cartesian
boxes with no-slip tops and bottoms.
\item On Fig. 2, the 6/5 regime (which I previously thought was the lame ``just
above onset regime'' is actually the part of the regime in which rotation dominates
over diffusion.  Once it goes to 2/7, it's not rotationally dominated. BUT in this
regime you also have the ``rotation is so strong convection is suppressed'' part
of parameter space.
\item Nu is suppressed in the rotational regime, and the strong scaling is bascially
so that once you get to the non-rotational regime, you fall onto the same
scaling as you would without rotation (interesting).
\item Last paragraph on pg. 2 above fig. 3 -- \textbf{TODO: maybe I should take a look at 
these references here to see how to measure BL thicknesses}.
\item Fig. 4 -- convective rossby number is not the parameter that controls rotational
constraint.
\end{enumerate}
This paper is awesome.

Here's notes on the supplementary material for \cite{king&all2009}.
\begin{enumerate}
\item Thermal boundary layer is defined by the height of the peak value of temperature
variance, which is to say if you take the stdev of the temperature around the mean profile,
and then take a profile of the stdev, the location of the peak of that variance is the
extent of the thermal boundary layer, as this is where thermal plumes develop and depart.
\item Fig. 2 in the supplementary info is a really beautiful demonstration of how Nu
can basically be calculated a-priori if you know what you're doing.
\item A free-fall time is defined in sec. 3.  Is this roughly where buoyancy time is
derived?  Need to work through this.

I sent this off to Ben, but for sanity's sake I want to type this up in nice format.
The free-fall velocity is defined as:
$$
u_{ff} = \sqrt{\alpha g L \Delta T},
$$
which is just the sqrt of the numerator of the Rayleigh number divided by $L^2$.  
IF it's that simple, then a stratified equivalent becomes:
$$
u_{ff-s} = \sqrt{g L \Delta S / c_P},
$$
and the free-fall time is
$$
t_{ff-s} = L/u_{ff-s} = \sqrt{\frac{L c_P}{g \Delta S}}.
$$
Getting this into more familiar units, this is just
\begin{equation}
t_{b-s} = \sqrt{\frac{ m c_P L}{g \epsilon n_\rho}},
\end{equation}
as in AB17.  So this is the buoyancy time, but multiplied by $\sqrt {m c_P / n_\rho}$.
And, in our paper where $n_\rho = 3$ and $c_P = 2.5$, this extra factor is
$\sqrt{5 m / 6}$, which is $\sqrt{5/4}$ when $\epsilon$ is small, and which is
$\sqrt{5/6}$ for $\epsilon = 0.5$ and $\sqrt{5/12}$ for $\epsilon = 1$.  Either way,
regardless of which parameters we're at, it's an order 1 change.  The nice thing about
it is that it makes our scaling clearer.

If we go to the paper and we define $\chi = \sqrt{\frac{g L^3 \Delta S / c_P}{Ra_t}}$,
then the diffusive timescale is, as we defined it,
$$
t_{\chi} = \frac{L^2}{\chi} = \sqrt{\frac{Ra_t\,L}{g\,\Delta S / c_P}} =
\sqrt{\frac{Ra_t\,L\,c_P\,m}{g\,\epsilon\,n_\rho}}.
$$
This is \emph{wonderfully nice}, because if we look at eqn 3 in AB17, it should actually
be
$$
\frac{t_{\chi}}{t_{b-s}} = \sqrt{Ra_t}.
$$
And that's it.  Which makes our experiments very clear.

\item It is argued that (due to Taylor-Proudman), we would expect structures to basically
be 2-D for a rotationally constrained system.  But for low values of convective rossby,
3D structures have been seen, and so coRossby is not what describes the influence of rotation
on a convective system.
\item There's mention of the 1/3 law not being supported by experimental studies.
\item \textbf{Section 4 talks about free-slip boundary conditions and the free-slip version
of the Ekman layer, the ``thermal Ekman layer,'' which is the vertical distance
from the boundary over which rotation is able to respond to density perturbations
and influence plume formation (verbatim).  Basically there's a rough argument that
the boundary layer theory in the main paper should apply here to free-slip, thermal
Ekman layers, just like plain old normal Ekman layers for no-slip.}
\end{enumerate}


\subsubsection{Notes on \cite{stellmach&all2014}}
\begin{enumerate}
\item for sanity's sake, here's the fluid numbers used in RB studies like this:
\begin{equation}
\text{Ra} = \frac{g\alpha \Delta T H^3}{\kappa\nu}\qquad
\text{E}  = \frac{\nu}{2\Omega H^2}\qquad
\text{Pr} = \frac{\nu}{\kappa}\qquad
\text{Ro}_{\text{co}} = \sqrt{\frac{\text{Ra}}{\text{Pr}}}\text{E}.
\end{equation}
Here, Ra is the rayleigh number, E is the Ekman number, Pr is the Prandtl number,
and Ro$_{\text{co}}$ is the convective Rossby number.  When Ro$_{\text{co}}$ is
very very small, the system is rotationally constrained. We often speak in terms
of the Taylor number, Ta = E$^{-2}$. Note: I tapped into \cite{julien&all1996} to
get the Ta defn properly.
\item fundamentally, the purpose of this paper is to connect the asymptotic equations
to DNS and experiments.
\item Central result of this study is that the BULK behavior is well-described by the
asymptotic equations, but the viscous boundary layer dynamics largely control
heat transfer scaling when the fluid is between no-slip boundaries.
\item Fig 1 is Nu v. supercriticality scalings at E $\approx 10^{-7}$, and open symbols are
asymptotic predictions.  For stress-free boundaries, they see a 3/2 law.  For no-slip
boundaries, they see a 3/2 law at high supercriticality, and a 3 law at low supercriticality.
\item In eqns 1, they parameterize the Ekman pumping effect on stress-free boundary
conditions in order to see if that is what is causing the scaling differences (they
do ignore the viscous dissipation of energy needed in most non-rotating boundary layer
theories).
\item They look at a few different Ekman numbers in Fig. 5, and I think this one
is going to be important to compare to \cite{king&all2009} and other papers to
make sure that I understand how these regimes connect.
\item (GOD I'm frustrated about the different scaling in King's paper and this paper.
I think that the supercriticality scales with Ra in the paper here, but a Ra$^{3/2}$
law is HUGE compared to the 2/7 and 6/5 laws reported in \cite{king&all2009}. Also,
Fig. 2 seems to go from really laminar to really turbulent just by increasing Ra by
a factor of 10, and that also seems sketchy.) \textbf{nevermind. Looked at
\cite{julien&all2012}, and now I realize that Stellmach is ENTIRELY in the low-Ro
regime.}
\item OK, so if I'm understanding properly, in the mega rotationally constrained regime,
the Nu scaling is insanity.  As you approach the non-rotational regime, it lowers
(to 6/5?), and then it goes to 2/7 in the completely non-rotation regime.  Hmm.
\end{enumerate}

\subsubsection{Notes on \cite{ecke2015}}
\begin{enumerate}
\item The first page of this paper is a detailed discussion of the range of Ra, Nu, Ek
at which you can actually achieve the Geostrophic regime.
\item The Ra$^3$ power law in \cite{stellmach&all2014} is called out, and explained
as actually being the result of a superposition of power laws in weakly nonlinear theory.
\item OK, so the big power law in Stellmach can be the result of a weakly nonlinear
expansion.  If you call $Ra/Ra_c - 1 \equiv \epsilon$, then $Nu - 1 = a \epsilon + b \epsilon^2$,
the magnitudes of $a$ and $b$ can actually directly determine the exponent on a power
law of the form $Nu - 1 = (Ra/Ra_c)^\alpha$, and that's what they're suggesting
is happening here.
\item Note that fig. 3 shows the range of $\epsilon$ over which the weakly
nonlinear theory is applicable.
\end{enumerate}


\subsubsection{Notes on \cite{wei&all2015}}
This is an experimental paper.
\begin{enumerate}
\item As far as I can tell, this paper holds Ra constant while changing $\Omega$.
\item There are multiple turbulent transitions as $\Omega$ is increased, and 
they appear at many different Ra values.  Basically they assume this is due to the
large-scale structure of the flow changing abruptly as the parameters change.
\item Relative Nu scaling between transitions is well-represented by straight lines.
\item Interesting paper, which approaches the problem in a different and intersting way.
I like the ``relative nu'' measure here, and it's amazing how much detail it was able to reveal.
\end{enumerate}

\bibliographystyle{apj}
\bibliography{../../mendeley_bibs/RotatingConvection}
\end{document}
