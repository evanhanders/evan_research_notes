\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{FC Navier Stokes, Polytrope}
\cfoot{\thepage}
\rfoot{May 21-June 12, 2015}

\newcommand{\FT}{\ensuremath{\text{FT}}}

\begin{document}
\section{Fully Compressible Navier Stokes Equations}
The Navier Stokes equations describe the motions of fluid elements.  In order
to fully describe their motion, we require four equations: a continuity
equation, a momentum equation, an energy equation, and an equation of
state (for closure).

The first of these three equations are as follows:
\begin{align}
\partial_t \rho + \nabla\cdot(\rho\bm{u}) =
\partial_t \rho + \bm{u}\cdot\nabla\rho + \rho\nabla\cdot\bm{u} = 0
&\text{ (continuity)}, \label{continuity_full}\\
\rho(\partial_t\bm{u} + \bm{u}\cdot\nabla\bm{u}) + \nabla P =
\bm{g}\rho - \nabla\cdot\bm{\Pi}
&\text{ (momentum)}, \label{momentum_full}\\
\partial_t S + \bm{u} \cdot \nabla S =
-\frac{1}{\rho T}\nabla\cdot\bm{Q} - \frac{1}{\rho T}\Pi_{ij}\partial_{x_i}u_j
&\text{ (energy)} \label{energy_full},
\end{align}
where $\rho$ is the density, $\bm{u}$ is the velocity vector, $P$ is the
pressure field, $\bm{g}$ is the gravity vector, $\bm{\Pi}$ is the stress
tensor, $S$ is the entropy field, $T$ is the energy (``temperature") field, and
$\bm{Q}$ is the heat flux.  The $\Pi_{ij}$ term in the energy equation is
a summation over all dimensions (x, y, z) of that term for each $i$th and $j$th
dimensional component.  Furthermore, in my derivation which follows,
I will assume that the fluid is an ideal gas, such that
\begin{equation}
PV = MR_sT, \label{gas_law}
\end{equation}
where $M$ is mass, $V$ is volume, $T$ is temperature, and
$R_s$ is the \emph{specific} gas constant.  In order to simplify my future
calculations, I will assume that \emph{mass is conserved}, and I will divide
my equation by $M$ such that $V \rightarrow V^*$, the specific volume, or the
volume per unit mas of material.  Further, I will absorbe $R_s$
into $T$, my temperature, such that $T \rightarrow T^*$.  
In this formulation, asterisks refer to specific variables and
both sides of the equation have
units of energy / mass.  My gas law takes the form
\begin{equation}
PV^* = T^*,
\end{equation}
where 
\begin{equation}
    \rho \equiv 1/V^*.
\end{equation}
In order to simplify notation, I will drop the ``$*$'' superscripts in the
rest of my derivations.

I'll need one further bit of information to solve for my equations: the stress
tensor.  The stress tensor is defined as
\begin{equation}
\Pi_{ij} = -\mu\left(\partial_{x_i}u_j + \partial_{x_j}u_i 
- \frac{2}{3}\delta_{ij}\nabla\cdot\bm{u}\right)\label{stress_tensor},
\end{equation}
where $\delta_{ij}$ is the Kronecker delta and $\mu$ is the dynamic viscosity.
I will solve for the fully
compressible equations in \emph{two dimensions}, where my velocity vector takes
the form $\bm{u} = u\hat x + w\hat z$, so my tensor takes the form
\begin{equation}
\bm{\Pi} = -\mu
\begin{pmatrix}
\frac{4}{3}\partial_x u - \frac{2}{3}\partial_z w &
\partial_z u + \partial_x w \\
\partial_z u + \partial_x w &
\frac{4}{3}\partial_z w - \frac{2}{3}\partial_x u
\end{pmatrix}. \label{stress_tensor_full}
\end{equation}

One more note: I'm going to break up most of my fields into background fields
and fluctuations (sort of like what you do when you linearize and solve for
wave solutions, but I won't be linearizing).  So as I go along, I'll be
breaking up into background (subscript 0) and fluctuating (subscript 1) fields,
and I'll assume that \emph{background fields are constant in time}.  Further,
I'll assume that \emph{background fields and spacially invariant in the
$x$-direction}.  This will simplify some things.


\subsection{Continuity Equation}
I'll start by tackling the continutiy equation (Eq. \ref{continuity_full}).  
I'll first divide through by $\rho$ to obtain
\begin{equation}
\frac{\partial_t \rho}{\rho} + \bm{u}\cdot \frac{\nabla\rho}{\rho} + 
\nabla\cdot\bm{u} = 0.
\end{equation}
Using the trick that
\begin{equation}
\frac{\partial x}{x} = \partial \ln x \label{ln_trick},
\end{equation}
my continuity equation becomes
\begin{equation}
\partial_t \ln \rho + \bm{u}\cdot\nabla\ln\rho + \nabla\cdot\bm{u} = 0.
\end{equation}
While at first I thought it was weird, it makes sense in the context of the
equations to come that we will break apart $\ln \rho$ into a background and
fluctuating component, such that $\ln\rho = \ln \rho_0 + (\ln \rho)_1$, and
to clean up notation, I'll stick with the notation of \cite{lecoanet2014}
and call this $\ln\rho = \ln\rho_0 + \Upsilon_1$.  Thus, my equation becomes
\begin{equation}
\partial_t(\ln\rho_0 + \Upsilon_1) + \bm{u}\cdot\nabla(\ln\rho_0 + \Upsilon_1)
+\nabla\cdot \bm{u} = 0,
\end{equation}
where the background term drops out of the time derivative, and I break up
my vectors to obtain the 2D form
\begin{equation}
\partial_t\Upsilon_1 + u\partial_x\Upsilon_1 + w \partial_z \ln\rho_0 +
w\partial_z\Upsilon_1 + \partial_x u + \partial_z w = 0.
\end{equation}
However, my ultimate goal is to ``dedalusize" this, so I want to put
my linear terms on the LHS and m non-linear terms on the RHS.  Thus, this
reduces to
\begin{equation}
\boxed{
\partial_t \Upsilon_1 + w\partial_z\ln\rho_0 + \partial_x u + \partial_z w = 
-u\partial_x\Upsilon_1 - w\partial_z\Upsilon_1
}\label{continuity_dedalus}.
\end{equation}

\subsection{Momentum Equation}
The next equation to tackle will be the momentum equation 
(Eq. \ref{momentum_full}).
My first step will be to divide through by $\rho$ to obtain
\begin{equation}
\partial_t\bm{u} + \bm{u}\cdot\nabla\bm{u} + \frac{1}{\rho}\nabla P =
\bm{g} - \frac{1}{\rho}\nabla\cdot\bm{\Pi}.
\end{equation}

\subsubsection{Material Derivative}
Fortunately, we don't have to employ any special tricks to figure out the
material derivative, but we do have to break this up into the $x$- and $y$-
components.  The $x$ component becomes
\begin{equation}
\partial_t u + u\partial_x u + w\partial_z u \text{ (x-component)},
\end{equation}
and the $z$ component becomes
\begin{equation}
\partial_t w + u\partial_x w + w\partial_z w \text{ (z-component)}.
\end{equation}

\subsubsection{$\nabla P$ and $\bm{g}$}
Recalling that $P = \rho T$ for an ideal gas, I will focus on simplifying out the $\nabla P$ and $\bm{g}$ terms.  It is clear that
\begin{equation}
\frac{1}{\rho}\nabla P - \bm{g} = \frac{\nabla(T\rho)}{\rho} \bm{g} =
T\frac{\nabla\rho}{\rho} + \nabla T - \bm{g} = 
T\nabla\ln\rho + \nabla T - \bm{g}.
\end{equation}
linearizing $\ln\rho$ as I did before and for $T \equiv T_0 + T_1$, I find
that these terms equal
\begin{equation}
(T_0 + T_1)\nabla(\ln\rho_0 + \Upsilon_1) + \nabla(T_0 + T_1) - \bm{g}.
\end{equation}
The $\hat x$ component of this (assuming that $\bm{g} = -g\hat z$) is then
\begin{equation}
T_0\partial_x\Upsilon_1 + T_1\partial_x\Upsilon_1 + \partial_x T_1
\text{ ($x$-component)}
\end{equation}
Where all background $x$ derivatives have dropped out.  The $\hat z$ component
takes the form
\begin{equation}
T_0\partial_z\ln\rho_0 + T_0\partial_z\Upsilon_1 +
T_1\partial_z\ln\rho_0 + T_1\partial_z\Upsilon_1 +
\partial_z T_0 + \partial_z T_1 + g.
\end{equation}
I assume that \emph{my background fields are in hydrostatic equilibrium}, or
that
\begin{equation}
-\rho_0 g = \partial_z P_0 = \partial_z(\rho_0 T_0) = T_0\partial_z\rho_0 +
\rho_0\partial_z T_0.
\end{equation}
Thus,
\begin{equation}
    g = -\left(T_0\partial_z\ln\rho_0 + \partial_z T_0\right),
\end{equation}
and my overall z-component for these parts simplifies to
\begin{equation}
T_0\partial_z\Upsilon_1 + T_1\partial_z\ln\rho_0 + T_1\partial_z\Upsilon_1
+ \partial_z T_1 \text{ (z-component)}.
\end{equation}

\subsubsection{Stress tensor}
The stress tensor is by far the most difficult term.  Recall that the
stress tensor takes the form $-\mu(\bf{S})$, where $\bf{S}$ is a tensor.  
Thus, assuming that
the dynamic viscosity is non-constant, the derivative of this whole mess
becomes pretty ugly.  I got out my plasma formulary and found that, where
$\bf{T}$ is a tensor and $a$ is a non-uniform scalar,
\begin{equation}
\nabla\cdot(a\bm{T}) = \nabla a \cdot\bm{T} + a\nabla\cdot\bm{T}.
\end{equation}
Thus, 
\begin{equation}
-\frac{1}{\rho}\nabla\cdot(-\mu\bm{S}) = 
\frac{1}{\rho}\nabla\mu\cdot\bm{S} + \frac{\mu}{\rho}\nabla\cdot\bm{S}.
\end{equation}
Note also that viscosity of the fluid will be defined as $\nu \equiv \mu/\rho$.

\paragraph{First: $\nabla\mu\cdot\bm{S} / \rho$}
First things first, $\mu = \rho\nu$, so 
\begin{equation}
\frac{\nabla(\rho\nu)}{\rho} = \nu\frac{\nabla\rho}{\rho} + \nabla\nu =
\nu\nabla\ln\rho + \nabla\nu.
\end{equation}
I solved for $\bf{S}$ above when I showed the full form of the stress tensor.
The x-component of this dot product is
\begin{equation}
\begin{split}
\left[(\nu\partial_x\ln\rho + \partial_x\nu)
 \left(\frac{4}{3}\partial_x u - \frac{2}{3}\partial_z w\right) +
 (\nu\partial_z\ln\rho + \partial_z\nu)
 \left(\partial_z u + \partial_x w\right) \right] =\\
\frac{4}{3}(\nu\partial_x\ln\rho\partial_x u + \partial_x\nu\partial_x\ u)- 
\frac{2}{3}(\nu\partial_x\ln\rho\partial_z w + \partial_x\nu\partial_z w) +
\\
\nu\partial_z\ln\rho\partial_z u + \partial_z\nu\partial_z u +
\nu\partial_z\ln\rho\partial_x w + \partial_z\nu\partial_x w
.
\end{split}
\end{equation}
Breaking up into fluctuating and non-fluctuating components, this becomes
\begin{equation}
\begin{split}
\frac{4}{3}([\nu_0+\nu_1]\partial_x\Upsilon_1\partial_xu + 
\partial_x\nu_1\partial_x u)
-\frac{2}{3}([\nu_0+\nu_1]
\partial_x\Upsilon_1\partial_z w + \partial_x\nu_1\partial_z w)
\\
+ (\nu_0 + \nu_1)\partial_z(\ln\rho_0 + \Upsilon_1)(\partial_z u + \partial_x w)
+ \partial_z(\nu_0 + \nu_1)(\partial_z u + \partial_x w) 
\end{split}
\end{equation}
By similar arguments, the z-component takes the form
\begin{equation}
\begin{split}
\nu\partial_x\ln\rho(\partial_z u + \partial_x w) +
\partial_x \nu (\partial_z u + \partial_x w) +\\
\frac{4}{3}(\nu\partial_z\ln\rho + \partial_z\nu)\partial_z w -
\frac{2}{3}(\nu\partial_z\ln\rho + \partial_z\nu)\partial_x u = \\
(\nu_0+\nu_1)\partial_x\Upsilon_1(\partial_z u + \partial_x w) +
\partial_x\nu_1(\partial_z u + \partial_x w) + \\
\frac{4}{3}\left[(\nu_0 + \nu_1)\partial_z(\ln\rho_0 + \Upsilon_1) +
\partial_z(\nu_0+\nu_1)\right]\partial_z w -\\
\frac{2}{3}\left[(\nu_0 + \nu_1)\partial_z(\ln\rho_0 + \Upsilon_1) +
\partial_z(\nu_0 + \nu_1)\right]\partial_x u.
\end{split}
\end{equation}

\paragraph{Second: $\nu\nabla\cdot\bm{S}$}
Now that that disgusting stuff is out of the way, we deal with the other
term.  The x-component of $\nabla\cdot\bm{S}$ takes the following form:
\begin{equation}
\frac{4}{3}\partial_x^2 u -\frac{2}{3}\partial_x\partial_z w +
\partial_z\partial_x w + \partial_z^2 u =
\partial_x^2 u +\partial_z^2 u + 
\frac{1}{3}(\partial_x^2 u + \partial_z\partial_x w).
\end{equation}
In a similar manner, the z-component takes the form
\begin{equation}
\partial_x\partial_z u + \partial_x^2 w + \frac{4}{3}\partial_z^2 w
-\frac{2}{3}\partial_z\partial_x u =
\partial_x^2 w + \partial_z^2 w + \frac{1}{3}(\partial_z^2 w +
\partial_z\partial_x u).
\end{equation}
Then, all I need to do is multiply the viscosity onto the front of these.
Neat.

\paragraph{The full x-component of the momentum equation}
is thus (in dedalus form)
\begin{empheq}[box=\fbox]{equation}
\begin{split}
\partial_t u + T_0\partial_x\Upsilon_1 +\partial_xT_1 -
\partial_z\nu_0(\partial_zu + \partial_xw) -
\\
\nu_0\left(\partial_x^2 u + \partial_z^2 u +
\frac{1}{3}(\partial_x^2 u + \partial_x\partial_z w) + 
\partial_z\ln\rho_0[\partial_zu + \partial_xw]   \right) =
\\
-u\partial_x u - w \partial_z u - T_1\partial_x\Upsilon_1 +
\\
\nu_1\left(\partial_x^2 u + \partial_z^2 u +   
\frac{1}{3}(\partial_x^2 u + \partial_x\partial_z w) +
\frac{4}{3}(\partial_x\Upsilon_1\partial_x u) - 
\right.\\\left.
\frac{2}{3}(\partial_x\Upsilon_1\partial_z w) 
+ \partial_z(\ln\rho_0 + \Upsilon_1)(\partial_zu + \partial_xw) 
\right) +
\\
\nu_0\left(\frac{4}{3}\partial_x\Upsilon_1\partial_xu -
\frac{2}{3}\partial_x\Upsilon_1\partial_zw 
+\partial_z\Upsilon_1(\partial_z u +\partial_x w)
\right) +
\\
\partial_x\nu_1\left(\frac{4}{3}\partial_xu-\frac{2}{3}\partial_zw\right)+
\partial_z\nu_1(\partial_zu + \partial_xw)
\end{split}
\end{empheq}

\paragraph{The full z-component of the momentum equation}
is thus (in dedalus form)
\begin{empheq}[box=\fbox]{equation}
\begin{split}
\partial_tw + T_1\partial_z\ln\rho_0 + T_0\partial_z\Upsilon_1+\partial_zT_1-
\partial_z\nu_0\left(\frac{4}{3}\partial_zw -\frac{2}{3}\partial_xu\right)-\\
\nu_0\left(\partial_x^2w + \partial_z^2 w + \frac{1}{3}[\partial_z^2w + 
\partial_z\partial_xu] + \partial_z\ln\rho_0\left[\frac{4}{3}\partial_zw -
\frac{2}{3}\partial_xu\right]\right)
=\\
-u\partial_xw - w\partial_zw -T_1\partial_z\Upsilon_1 +\\
\nu_1\left(\partial_x^2w + \partial_z^2w + \frac{1}{3}[\partial_z^2w +
\partial_z\partial_xu] + \partial_x\Upsilon_1[\partial_zu + \partial_xw]
+\right.\\
\left.\partial_z[\ln\rho_0 +\Upsilon_1]\left\{\frac{4}{3}\partial_zw -
\frac{2}{3}\partial_xu\right\}
\right) +\\
\nu_0\left( \partial_x\Upsilon_1[\partial_z u + \partial_x w] +
\partial_z\Upsilon_1\left[\frac{4}{3}\partial_zw -\frac{2}{3}\partial_xu\right]
\right)+\\
\partial_x\nu_1(\partial_zu +\partial_xw) +
\partial_z(\nu_0 +\nu_1)\left(\frac{4}{3}\partial_zw - \frac{2}{3}\partial_xu
\right)
\end{split}
\end{empheq}


\subsection{Energy Equation}
Finally, I will take care of the energy equation (Eq. \ref{energy_full}).
I will start by doing two things: first, I will multiply through by $T$. Second,
I will abbreviate $d \equiv \partial_t + \bm{u}\cdot\nabla$.  Doing so, my
Energy equation takes the form
\begin{equation}
TdS = -\frac{1}{\rho}\nabla\cdot\bm{Q} - \frac{1}{\rho}\Pi_{ij}\partial_{x_i}u_j
.
\end{equation}
I will examine each of these three terms individually.

\subsubsection{$TdS$}
This form should be immediately familiar as a term in the thermodynamic
identity,
\begin{equation}
dU = TdS - PdV.
\end{equation}
Clearly, I want to find some sort of substitution for $TdS$ into my original
equation, so I will rearrange such that $TdS = dU + PdV$.  The first term is
easy to deal with.  The definition of $C_V$ is simply
\begin{equation}
    C_V \equiv \left(\frac{\partial U}{\partial T}\right)_V.
\end{equation}
Thus, my thermodynamic identity can be recast as
\begin{equation}
TdS = C_VdT + PdV.
\end{equation}
The $PdV$ term is somewhat more complicated.  Recall from our definition of an
ideal gas that $V = T/P$ and $P = \rho T$.  Thus, this term can be
expanded such that
\begin{equation}
\begin{split}
PdV &= Pd\left(\frac{T}{P}\right) =
P\left(-\frac{TdP}{P^2} + \frac{dT}{P}\right)\\
&= dT - \frac{Td(\rho T)}{\rho T} = dT - Td\ln\rho - dT\\
&= - Td\ln\rho.
\end{split}
\end{equation}
In order to make my future notation a little simpler, I will now notice that
my specific heats can be manipulated to recast this expression. The specific
heats of constant volume and pressure are related such that
\begin{equation}
C_P = C_V + P\left(\frac{\partial V}{\partial T}\right)_P =
C_V + P\frac{1}{P} = C_V + 1.
\end{equation}
Utilizing this along with one other definition from thermo ($C_P/C_V = \gamma$,
the adiabatic index),
I retrieve
\begin{equation}
1 = C_P - C_V = C_V\gamma - C_V = C_V(\gamma - 1).
\end{equation}
At this point I also want to look back at my continuity equation.  Using
this, equation and my ``new definition of one,'' I can re-express $PdV$ as
\begin{equation}
PdV = -Td\ln\rho = TC_V(\gamma-1)(\nabla\cdot\bm{u}).
\end{equation}
Thus, my material derivative for entropy can be fully recast in terms of
a physical variable, specific temperature, as
\begin{equation}
\boxed{
TdS = C_V(dT + (\gamma-1)T[\nabla\cdot\bm{u}])}.
\end{equation}

\subsubsection{$-(\nabla\cdot\bm{Q})/\rho$}
I will next re-express the heat flux term.  This can be dealt with in one of
two ways: I can either diffuse heat through temperature or entropy.  Either way,
I know that for my background field, the divergence of heat flux is zero, or
\begin{equation}
\nabla\cdot\bm{Q}_0 = 0.
\end{equation}
For temperature diffusion, I define heat flux as
\begin{equation}
\bm{Q}_{T1} = -\chi_T\rho\nabla T_1,
\end{equation}
and for entropy diffusion I define heat flux as
\begin{equation}
\bm{Q}_{S1} = -\chi_S\rho T_0 \nabla \frac{S_1}{C_p}.
\end{equation}
While in the following derivation I will derive for temperature fluctuations,
at any point my derivation can be converted to an entropy derivation by
substitution $\chi_T \rightarrow \chi_S$ and $\nabla T_0 \rightarrow
T_0 \nabla (S_1/C_p)$.

Plugging in my definition of heat flux into the energy equation, I retrieve
the following (using my plasma formulary for vector identities):
\begin{equation}
\begin{split}
-\frac{1}{\rho}\nabla\cdot\bm{Q} &=
\frac{1}{\rho}\nabla\cdot(\chi\rho\nabla T_1) =
\frac{1}{\rho}[\chi\rho\nabla\cdot\nabla T_1 + \nabla T_1\cdot\nabla(\chi\rho)]
\\
&= \chi\nabla^2 T_1 +\nabla T_1 \cdot\left(\chi\frac{\nabla\rho}{\rho} +
\nabla\chi\right)\\
&= \chi\nabla^2 T_1 + \nabla T_1\cdot(\chi\nabla\ln\rho) + 
\nabla T_1\cdot\nabla\chi.
\end{split}
\end{equation}
Assuming constant, uniform $\chi$, the final term drops out.
Thus, this term in the energy equation is recast in the form
\begin{equation}
-\frac{1}{\rho}\nabla\cdot\bm{Q} = \chi[\nabla^2 T_1 + 
\nabla T_1\cdot\nabla\ln\rho]
.
\end{equation}

\subsubsection{$-\Pi_{ij}\partial_{x_i}u_j/(\rho)$}
Fortunately, I already calculated $\Pi$ out earlier, so I'll just plug it in
to this expression and find that
\begin{equation}
\begin{split}
-\frac{1}{\rho}\Pi_{ij}\partial_{x_i}u_j =&
\frac{\mu}{\rho}
\left[
\left(\frac{4}{3}\partial_x u - \frac{2}{3}\partial_z w\right)\right.\partial_x u
+ (\partial_z u + \partial_x w)(\partial_x w + \partial_z u)\\
&+ \left.
\left(\frac{4}{3}\partial_z w - \frac{2}{3}\partial_x u\right)\partial_z w
   \right]\\
=&
\nu\left[
\frac{4}{3}\left([\partial_x u]^2 + [\partial_z w`]^2 - \partial_x u\partial_z w
\right) + 2\partial_xw\partial_zu + (\partial_zu)^2+(\partial_xw)^2
\right]
\end{split}
\end{equation}

\subsubsection{Full Equation}
Thus, the full energy equation in two dimensions, becomes
\begin{equation}
\begin{split}
\partial_t T + u\partial_xT + w\partial_zT + &
(\gamma-1)T(\partial_xu + \partial_zw)
= \\
\frac{\chi}{C_V}&(\partial_x^2T_1 +\partial_z^2T_1 +
(\partial_xT_1\hat x +\partial_z T_1\hat z)\cdot
(\hat x \partial_x + \hat z \partial_z)\ln\rho) +\\
\frac{\nu}{C_V}&\left[
\frac{4}{3}
\left([\partial_x u]^2 + [\partial_z w]^2 - \partial_x u\partial_z w
\right) + 2\partial_xw\partial_zu + (\partial_zu)^2+(\partial_xw)^2
\right].
\end{split}
\end{equation}

Split into variations and non-variations, this becomes (in dedalus' linear
and non-linear format),
\begin{empheq}[box=\fbox]{equation}
\begin{split}
\partial_t T_1 + w\partial_z T_0 + (\gamma-1)T_0(\partial_xu+\partial_zw)
-\frac{\chi}{C_V}(\partial_x^2 T_1 + \partial_z^2 T_1 + 
\partial_zT_1\partial_z\ln\rho_0) =\\
-u\partial_xT_1-w\partial_zT_1  -(\gamma-1)T_1(\partial_xu +\partial_zw)
+\frac{\chi}{C_V}(\partial_xT_1\partial_x\Upsilon_1 
+\partial_zT_1\partial_z\Upsilon_1)
\\
-\frac{\nu_0 + \nu_1}{C_V}\left[
\frac{4}{3}\left([\partial_x u]^2 + [\partial_z w]^2 - \partial_x u\partial_z w
\right) + 2\partial_xw\partial_zu + (\partial_zu)^2+(\partial_xw)^2
\right].
\end{split}
\end{empheq}


\section{Entropy Equation}
Starting with the entropy version of the thermodynamic identity,
\begin{equation}
    dS = \frac{1}{T}dU + \frac{P}{T}dV,
\end{equation}
I plug in $V = T/P$ to the second term and find that
\begin{equation}
\begin{split}
    dS &= C_V d\ln T + \frac{P}{T}\left(\frac{dT}{P} - \frac{TdP}{P^2}\right)\\
    &= C_V d\ln T + d\ln T - d\ln P = C_Pd\ln T - d\ln P.
\end{split}
\end{equation}
I will now utilize the facts that $1 + C_V = C_P$ and $P = \rho T$ to show 
\begin{equation}
    dS = C_Pd\ln \left(\frac{P}{\rho}\right) - d\ln P =
    C_Vd\ln P - C_Pd\ln \rho.
\end{equation}
Dividing through by $C_P$, this becomes
\begin{equation}
    \frac{dS}{C_P} = \frac{1}{\gamma}d\ln P - d\ln\rho
\end{equation}

\section{Equation of State}
Taking the integrated entropy equation,
\begin{equation}
    \frac{S}{C_P} = \frac{1}{\gamma}d\ln P - d\ln\rho,
\end{equation}
I find the fluctuating component of this through subtracting out the background
\begin{equation}
    \frac{S_1}{C_P} = \frac{S - S_0}{C_P} =
    \frac{1}{\gamma}\ln\left(\frac{P}{P_0}\right) - 
    \ln\left(\frac{\rho}{\rho_0}\right).
\end{equation}
Plugging in $P = \rho T$,
\begin{equation}
    \frac{S_1}{C_P} = \frac{1}{\gamma}\ln\left(\frac{T}{T_0}\right) +
    \left(\frac{1}{\gamma} - 1\right)\Upsilon_1.
\end{equation}
Expanding $T$ and rexpressing $\gamma$, I find
\begin{equation}
    \frac{S_1}{C_P} = \frac{1}{\gamma}\ln\left(1 + \frac{T_1}{T_0}\right) +
    \left(\frac{C_V - C_P}{C_P}\right)\Upsilon_1 =
    \frac{1}{\gamma}\ln\left(1 + \frac{T_1}{T_0}\right) +
    \frac{1}{C_P}\Upsilon_1.
\end{equation}
I will then rearrange this into dedalus form, and subtract off the (linear) 
waves from both sides of the equations equally, such that the equation
appears in the form
\begin{equation}
    \frac{S_1}{C_P} + \frac{1}{C_P}\Upsilon_1 -\frac{1}{\gamma}\frac{T_1}{T_0}
    = \frac{1}{\gamma}\left[\ln\left(1+\frac{T_1}{T_0}\right) - \frac{T_1}{T_0}
    \right]
\end{equation}


\section{Initial conditions: Polytrope}
I will assume that my background conditions are polytropic, such that
\begin{equation}
\begin{split}
T &= T_{00}(z_0 - z) \\
\rho &= \rho_{00}(z_0 - z)^n \\
P &= P_{00}(z_0 - z)^{n+1}.
\end{split}
\end{equation}
Where all of the ``00'' subscripts refer to the values of those variables at
the top of my domain.  Note also that, due to my assumption of an ideal gas,
I have the constraint that $P_{00} = \rho_{00}T_{00}$.  I will take gradients of all of these properties to find that
\begin{equation}
\begin{split}
\nabla\ln P &= \frac{\nabla P}{P} = -(n+1)(z_0-z)^{-1} \\
\nabla\ln\rho &= \frac{\nabla\rho}{\rho} = -n(z_0-z)^{-1} \\
\nabla\ln T &= \frac{\nabla T}{T} = -(z_0-z)^{-1}.
\end{split}
\end{equation}
Combining these results with the entropy equation, I find
\begin{equation}
\begin{split}
\frac{1}{C_P}\nabla S &= \frac{1}{\gamma}\nabla\ln P - \nabla\ln \rho\\
&= -\left(\frac{n+1}{\gamma} - n \right)(z_0-z)^{-1} \\
&= \frac{n}{\gamma}\left(\gamma-\frac{n+1}{n}\right)(z_0-z)^{-1}.
\end{split}
\end{equation}
For an adiabatic system, $\nabla S = 0$, so $\gamma - (n+1)/n$ must be
zero.  In other words, an adiabatic system has the property that
\begin{equation}
n = \frac{1}{\gamma - 1} = \frac{3}{2} \text{ for }\gamma = \frac{5}{3}.
\end{equation}
Thus, we can use this value to determine when a system is adiabatic,
subadiabatic, or superadiabatic.  For $\gamma = 5/3$,
\begin{equation}
n = \left\{
\begin{array}{lr}
1.5 - \epsilon & \nabla S < 0\text{, superadiabatic} \\
1.5 & \nabla S = 0\text{, adiabatic} \\
1.5 + \Delta & \nabla S > 0\text{, subadiabatic}
\end{array}
\right.,
\end{equation}
where $\epsilon$ is some small positive value and $\Delta$ is some positive
value of any magnitude.

Since I assume hydrostatic equilibrium for my polytrope, $\partial_z = \rho g$,
and I can find that
\begin{equation}
\begin{split}
\partial_z P &= \rho g \\
-P_{00}(n+1)(z_0-z)^{-1} &= \rho_{00}(z_0-z)^n g \\
-P_{00}(n+1) &= \rho_{00}g \\
g &= -\frac{P_{00}}{\rho_{00}}(n+1).
\end{split}
\end{equation}
Which is great, cause now we have a non-dimensional happy gravity expression.
Also, I need to figure out what the entropy gradient is in the case of a
superadiabatic solution. Thus, I return to my derivation of $\nabla S$ and
plug in the superadiabatic $n$ to find
\begin{equation}
\begin{split}
\frac{\nabla S}{C_P} &= -\left(\frac{(\gamma-1)^{-1} + (\gamma-1)(\gamma-1)^{-1}}{\gamma} - \frac{1}{\gamma-1} + \epsilon\right)(z_0-z)^{-1} \\
&= -\left([\gamma-1]^{-1} - [\gamma-1]^{-1} + \epsilon - \frac{\epsilon}{\gamma}
\right)(z_0-z)^{-1} \\
&= -\epsilon\left(\frac{\gamma-1}{\gamma}\right)(z_0-z)^{-1}.
\end{split}
\end{equation}
So the entropy gradient ends up having an order $\epsilon$ change.


\section{Full Dedalus equations assuming $\nu$ is constant and uniform}
Continuity is simple:
\begin{equation}
\partial_t \Upsilon_1 + w\partial_z\ln\rho_0 + \partial_x u + w_z =
-u\partial_x\Upsilon_1 - w\partial_z\Upsilon_1
\label{continuity_dedalus_2}.
\end{equation}
Momentum comes in x:
\begin{equation}
\begin{split}
\partial_t u + \partial_xT_1 + T_0\partial_x \Upsilon_1 -
\nu\left(\partial_x^2u +
\partial_z u_z + \frac{1}{3}(\partial_x^2 u + \partial_x w_z)+
\partial_z\ln\rho_0[u_z + \partial_x w]\right)
=\\
-u\partial_xu - w u_z - T_1\partial_x\Upsilon_1 +
\nu\left(\partial_x\Upsilon_1\left[\frac{4}{3}\partial_xu -
\frac{2}{3}w_z\right] +\partial_z\Upsilon_1[u_z +\partial_xw]\right).
\end{split}
\end{equation}
and z:
\begin{equation}
\begin{split}
\partial_t w + T_1 \partial_z \ln\rho_0 + T_0 \partial_z\Upsilon_1 +
\partial_z T_1 -
\nu\left(\partial_x^2 w + \partial_z w_z +
\frac{1}{3}[\partial_z w_z + \partial_x u_z] +
\partial_z\ln\rho_0\left[\frac{4}{3}w_z-\frac{2}{3}\partial_xu\right]\right)=\\
-u\partial_xw - ww_z - T_1\partial_z\Upsilon_1 +
\nu\left(\partial_x\Upsilon_1[u_z +\partial_xw] +
\partial_z\Upsilon_1\left[\frac{4}{3}w_z - \frac{2}{3}\partial_x u\right]\right)
\end{split}
\end{equation}
The energy equation is then (for entropy)
\begin{equation}
\begin{split}
\partial_t T_1 + w\partial_z T_0 + (\gamma-1)T_0(\partial_xu + w_z)
-\frac{\chi}{C_V}\left(T_0\partial_x^2\frac{S_1}{C_P} - \partial_z \tilde Q_{z1}
- \tilde Q_{z1}\partial_z\ln\rho_0\right) =\\
-u\partial_xT_1 - w\partial_z T_1 - (\gamma-1)T_1(\partial_xu + w_z)
+\frac{\chi}{C_V}\left(T_0\partial_x\frac{S_1}{C_P}\partial_x\Upsilon_1 - 
\tilde Q_{z1}\partial_z\Upsilon_1\right) +\\
\frac{\nu}{C_V}\left[\frac{4}{3}\left(
[\partial_xu]^2 + w_z^2 -\partial_xuw_z\right)
+2\partial_xwu_z + u_z^2 + (\partial_xw)^2\right]
\end{split}
\end{equation}
The equation for heat flux is (for entropy)
\begin{equation}
\tilde Q_{z1} + T_0\partial_z\frac{S_1}{C_P} = 0,
\end{equation}

The equation of state I derived is off by a factor of $M$ from 
\cite{lecoanet2014}
and I'm going to just go ahead and sweep that under the rug for now (I think
this is the same thing as saying $M=1$?), and so the equation of state
in Dedalus is
\begin{equation}
    \frac{S_1}{C_P} + \frac{1}{C_P}\Upsilon_1 - \frac{1}{\gamma}\frac{T_1}{T_0}=
    \frac{1}{\gamma}\left[\ln\left(1+\frac{T_1}{T_0}\right)-\frac{T_1}{T_0}
\right].
\end{equation}

and I also have to define my z derivatives of velocities.
\begin{equation}
w_z - \partial_z w = 0,
\end{equation}
\begin{equation}
u_z - \partial_zu = 0.
\end{equation}


\bibliographystyle{apj}
\bibliography{../biblio.bib}
\end{document}
