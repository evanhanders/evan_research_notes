\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{Two-layer polytrope (Tanh profile)}
\cfoot{\thepage}
\rfoot{June 12, 2015}

\newcommand{\FT}{\ensuremath{\text{FT}}}

\begin{document}
\section{Two-layer analytical polytrope}
Ok, ok, ok, so in an ideal world we would actually be able to use an analytical
expression for a two-layer polytrope in all of our dedalus implementations.
Unfortunately, this isn't really super possible due to the fact that this would
mean infinite bandwidth. Still, it's useful to take
a look at the expressions and get a feeling for what our profiles SHOULD look
like.  For a single polytrope (non-dimensionalized so that all variables
equal zero at the top of the domain), we have the following expressions (for
an ideal gas):
\begin{equation}
    \begin{split}
        T &= (z_0 - z) \\
        \rho &= (z_0 - z)^m \\
        P &= (z_0 - z)^{m+1} \\
        P &= \rho T,
    \end{split}
\end{equation}
where $m$ is the adiabatic index and $z_0 = L_z + 1$, where $L_z$ is the length
of the domain.  Applying this now to a two-layer atmosphere where the
convective zone (henceforth CZ) is above the radiative zone (henceforth RZ),
we have to modify some things and add some new variables.  Instead of a single
polytropic index, we now have two ($m \rightarrow m_{cz}, m_{rz}$).  Further,
we need to keep track of the total length of the domain ($L_z$), as well as the
length of the radiaitive zone ($L_{rz}$), and the length of the convective
zone ($L_{cz}$).  Assuming that $z=0$ is at the base of the convective zone,
my 





\section{Setting up parameters for the atmosphere}
So we're trying to set up a two-layer atmosphere which essentially has a
convective zone and a radiative zone.  The differentiation between these two
layers is caused (essentially) by the entropy gradient, which is determined
by the polytropic indices of the two layers.  The convective zone has a 
negative entropy gradient, which we will impose must be \emph{small}, as this
will result in very low mach number flow and we won't have to worry about
sound waves (note: this is superadiabatic).  The radiative zone has a positive
entropy gradient (subadiabatic) and thus will smooth out any instabilities.

Note that when we talk about the difference between the two layers we will refer
to ``stiffness.''  Stiffness is defined as
\begin{equation}
    S \equiv \frac{n_{rz} - n_{ad}}{n_{ad}-n_{cz}},
\end{equation}
where each $n_{rz}$ is the polytropic index of the radiative zone,
$n_{cz}$ is the polytropic index of the convective zone, and $n_{ad} = 1.5$,
the polytropic index of a perfect adiabat for monotomic ideal gases.  It is
important to note that $n_{cz} = n_{ad} - \epsilon$, where $\epsilon$ must
be small for low mach number flows ($Ma \propto \sqrt{\epsilon}$).  Further,
$n_{rz} > n_{ad}$ for a subadiabatic, stable gradient.  Stiffness essentially
is a measure of how hard of a wall convective motions hit when they encounter
the radiative zone.  A high value of $S$ means that motions hit the radiative
zone and get damped out almost immediately, whereas a low value of $S$ means
that the motions in the convective zone can penetrate deeply into the radiative
zone.

Note that we will often not be working directly with the polytropic indices but
will rather be talking about the thermal conductivities of the two layers of
the atmosphere, which follow the formula
\begin{equation}
    \frac{\kappa_{rz}}{\kappa_{cz}} = \frac{n_{rz} + 1}{n_{cz} + 1}.
\end{equation}
Doing a bit of math-a-magic, we can see that
\begin{equation}
    n_{rz} = n_{ad} + Sn_{ad} - Sn_{cz} 
    = n_{ad} + Sn_{ad} - Sn_{ad} + S\epsilon = n_{ad} + S\epsilon
\end{equation}
and
\begin{equation} 
\begin{split}
    \frac{\kappa_{rz}}{\kappa_{cz}}
    &=\frac{n_{ad} + S\epsilon + 1}{n_{ad} - \epsilon + 1}\\
    &=1 + \epsilon\frac{S + 1}{n_{ad} - \epsilon + 1},
\end{split}
\end{equation}
so by specifying three knobs ($\kappa_{cz}, \epsilon, S$) we can describe a
two-layer atmospheric profile in terms of Mach number and how far motions
should shoot into the radiative zone.

\section{Tanh, for Dedalus}
Note that Dedalus expresses non-constant coefficients in terms of a chebyshev
polynomial series.  Also note that the more terms included in these series, the
higher the bandwidth of our sparse solver (the matrix goes from roughly
diagonal to a big fat thing along the diagonal).  Also note that, for
bandwidth $m$ in an $N$-point domain, the order of the solve is roughly
$O(Nm)$.  So we want low bandwidth.

The problem is, the ``ideal'' atmospheric model we want is essentially a
step function from the CZ polytrope to the RZ polytrope, but that has
infinite bandwidth.  So we've decided we're going to smooth out the transition
between the two layers with a tanh profile.  Note that, as I discuss things
further, I will assume that we have a $\kappa$ tanh profile of the form
\begin{equation}
    \kappa = \frac{\kappa_{rz} - \kappa_{cz}}{2}\left[
    \text{tanh}\left(\frac{z_b-z}{W}\right)+1\right] + \kappa_{cz},
\end{equation}
where $z_b$ is the $z$ coordinate of the boundary between the RZ and the CZ,
$W$ is the ``width'' of the tanh (determining how steep the change is between
the two zones), and the extra shennanigans at the end properly offset 
the curve from zero.

There are, as I see it, actually 6 total knobs for changing the parameters of
the atmosphere here: $S, \epsilon, W, z_b, L_z, and \kappa_{cz}$, where
$L_z$ is the height of the total domain.  $S$ and $\epsilon$ determine how
large of a step the tanh must make, along with the ``pinning'' decision of
setting $\kappa_{cz}$.  $W$ will determine how quickly the step is taken and
will often be referred to in units of $L_z$ ($W = 0.01$ will probably mean
that $W = L_z/100$, unless stated otherwise).  $L_z$ and $z_b$ will affect
the true value of $W$ and the location in the domain where the transition
occurs, but probably don't affect much else.  Note that the primary knobs
which will affect bandwidth wil be $S, \epsilon,$ and $W$.


\subsection{Exploration of $S$ and $W$}
I have created a python script which explores the properties of this tanh and
compares it to a plain old step-function discontinuous boundary.  My script
produces a number of plots, such as those that will be shown below.  I will
comment on a domain of $L_z = 10$ (which I did try varying and saw no difference
in the outcome plots, so I stuck with this), constructed out of 1024 points,
with half of the domain as the supposed convection zone and half as the
supposed radiative zone.  Here I will specifically comment on runs with
$\epsilon = 10^{-5}$ and $S = 10, 10^3, 10^5$.

\subsubsection{Layout of images}
The images are laid out as a series of four colormaps. All four images explore
$W$ space on the y-axis; that is, all four images show solutions of increasing
width on the y-axis in units of $L_z$.

The top left image shows
the power spectrum in Chebyshev space up to the first 100 coefficients.  The
x-axis marks the coefficient number, and any grid point shows the amount of
power in that coefficient (x) for a tanh of a given width (y)

The top right image shows the difference between an ``infinite'' expansion and
a truncated expansion.  At any point on the colormap, it is showing the L2 norm
of the difference between the full expansion and an expansion truncated at the
(xth) coefficient for a tanh of the width specified by (y).

The bottom left image simply shows the absolute value of the difference
between the tanh of the given width (y) and a step function atmosphere at any
z-point in the domain (along the x-axis).

The bottom right image shows the percentage of z-points that are within
0.01\% of the step function solution.  This plot answers:
For a chebyshev series which is truncated
at the (xth) coefficient, what percentage of points in the domain are still
within an acceptable difference ($\leq$0.01\%) of the step function solution
given a tanh of width (y).  Note that for small values of $S$, essentially
all points regardless of width and expansion order will meet this criterion;
it becomes more restrictive as $S$ increases to interesting values.


\newpage
\subsubsection{S = 10}
For a small value of stiffness, the amplitude of the tanh is very small, and
it's actually pretty easy to reliably model the step function to a large
degree of accuracy because the discontinuity is small.  


%\begin{figure}[ht]
%    \centering
%    \includegraphics[width=\textwidth]{./figures/kappa_diagnostics_S_10.png}
%    \caption{S=10.}
%    \label{fig:S10}
%\end{figure}

\newpage
\subsubsection{S = $10^3$}
This is slightly stiffer, so the discontinuity is pretty big, but it's still
fairly easy to model with few coefficients.

%\begin{figure}[ht]
%    \centering
%    \includegraphics[width=\textwidth]{./figures/kappa_diagnostics_S_1000.png}
%    \caption{S=$10^3$.}
%    \label{fig:S10}
%\end{figure}

\newpage
\subsubsection{S = $10^5$}
This is getting into the range of quite stiff.  The discontinuity is large,
and it's hard to model with few coefficients.

%\begin{figure}[ht]
%    \centering
%    \includegraphics[width=\textwidth]{./figures/kappa_diagnostics_S_100000.png}
%    \caption{S=$10^5$.}
%    \label{fig:S10}
%\end{figure}

\newpage
\section{Analysis and code}
For interesting values of stiffness ($S > 10^3$), we're really pretty screwed
except for some specific values of width; even then, it could take up to about
40 coefficients to decently express the tanh, and there's no guarantee that
there aren't a bunch of wiggles wrapped up in those expansions (I haven't
looked in that much detail).

Still, if our domain is huge (about 1000 points like here), then a bandwidth
of 40 really isn't too bad (I mean, it's 20x worse than a bandwidth of 2, but
it could be much worse if we don't truncate it).

I'll need to get a bit more direction on what values of $S$ we want before I
can really do anything definitive.




%%Code Import Styles
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2}

\lstset{style=mystyle}

% Actual Code Import Line
\lstinputlisting[language=Python]{./code/tanh.py}
\bibliographystyle{apj}
\bibliography{../biblio.bib}
\end{document}
