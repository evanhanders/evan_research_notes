'''
Tries out padding functions to see if we can decrease number of orders in
power spectrum
'''
import numpy as np
from dedalus import public as de
from multiple_polytrope import *
from tanh import *

import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

SAVEDIR_BASE    = './figures/'
RA_TOP          = 1e8
PR_TOP          = 1
GAMMA           = 5/3
N_RHO_CZ        = 3
N_RHO_RZ        = 2
M_RZ            = 3
STIFFNESS       = 100
N_PTS           = 256

def setup(ra_top=RA_TOP, pr_top=PR_TOP, gamma=GAMMA, n_rho_cz=N_RHO_CZ,
                n_rho_rz=N_RHO_RZ, m_rz=M_RZ, stiffness=STIFFNESS,
                n_pts=N_PTS, fig_x=20, fig_y=10, n_widths=100, n_coeffs=100):
    fig = plt.figure(figsize=(fig_x, fig_y))
    widths = (1+np.arange(n_widths))/n_widths
    coeffs = np.arange(n_coeffs)

    params = compute_atmosphere_parameters(Rayleigh_top=ra_top,
                    Prandtl_top=pr_top, gamma=gamma, n_rho_cz=n_rho_cz,
                    n_rho_rz=n_rho_rz, m_rz=m_rz, stiffness=stiffness)
    Lz = params['L_tot']
    z_basis = de.Chebyshev('z', n_pts, interval=(0,Lz), dealias=3/2)
    domain = de.Domain([z_basis], grid_dtype=np.float64)
    z = domain.grid(0)

    return_vals = dict()
    return_vals['params'] = params
    return_vals['fig'] = fig
    return_vals['widths'] = widths
    return_vals['coeffs'] = coeffs
    return_vals['z'] = z
    return_vals['domain'] = domain

    return return_vals

def try_smoothing_function(prob, width=0.05):
    L_tot = prob['params']['L_tot']
    width = width * L_tot
    L_rz = prob['params']['L_rz']
    L_cz = prob['params']['L_cz']
    z = prob['z']
    domain = prob['domain']
    flux_top = -prob['params']['chi_top']

    fig = prob['fig']
    fig.clear()
    ax = fig.add_subplot(2,2,1)
    bx = fig.add_subplot(2,2,2)
    cx = fig.add_subplot(2,2,3)
    dx = fig.add_subplot(2,2,4)

    kappa   = domain.new_field()
    del_T   = domain.new_field()
    T       = domain.new_field()
    rho     = domain.new_field()
    dzlnrho = domain.new_field()

    compute_kappa_profile(prob['params'], z, kappa, tanh_width=width)
    T = calculate_T(kappa, del_T, T, flux_top)
    rho = calculate_rho(domain, prob['params'], z, T, rho)
    dzlnrho = calculate_grad_log_rho(rho, dzlnrho)
    ax.plot(z, dzlnrho['g'], 'g')
    ax.set_xlabel('z')
    ax.set_ylabel(r'$\nabla(\ln(\rho))$')
    ax.set_title('Grad log rho, Width={0:.4g}'.format(width))

    smoother = L_tot+1-z
    #smoother = 0.1*(L_rz-z)**2
    cx.plot(z, dzlnrho['g']*smoother, 'r')
    cx.set_xlabel('z')
    cx.set_ylabel(r'$\nabla(\ln(\rho))*smoother$')
    cx.set_title('Grad log rho, Width={0:.4g}, smoother: L_tot+1-z'.format(width))

    power_spectrum_v_width(bx, prob['widths'], prob['coeffs'], domain, z,
            prob['params'], field='grad log rho', cmap='Greens')


    power_spectrum_v_width(dx, prob['widths'], prob['coeffs'], domain, z,
            prob['params'], field='grad log rho', cmap='Reds',
            smoothing=smoother)
    
    plt.subplots_adjust(top=0.88, wspace=0.5, hspace=0.4)
    params = prob['params']
    fig.suptitle('S = {0:.6g}, EPS = {1:.4g}, KTOP = {2:.4g}'.\
                format(params['stiffness'], params['epsilon'], \
                params['chi_top'])\
                +', LRZ = {0:.4g}, LTOT = {1:.4g}'.\
                format(params['L_rz'], params['L_tot']),\
                fontsize=16)
    fig.savefig(SAVEDIR_BASE+'smoother_CZ_T.png', dpi=100)


if __name__ == '__main__':
    problem1 = setup(n_coeffs=50)
    try_smoothing_function(problem1, width=0.22)
    plt.show()

