'''
Tests how the Chebyshev Power spectrum scales with width of tanh function
'''
import numpy as np
from dedalus import public as de

from mpl_toolkits.axes_grid1 import make_axes_locatable
from multiple_polytrope import *
import matplotlib
import matplotlib.pyplot as plt

#Set up some constant, base params
SAVEDIR_BASE    = './figures/'
RA_TOP          = 1e8
PR_TOP          = 1
GAMMA           = 5/3
N_RHO_CZ        = 3
N_RHO_RZ        = 2
M_RZ            = 3
STIFFNESS       = 100
N_PTS           = 256

def get_step_kappa(z, parameters):
    '''
    Creates an analytical step function of kappa that's the same size
    as z

    ######ARGUMENTS######
    z           - The NumPy array of z values in our dedalus domain.
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmosphere_parameters()
    '''
    center = parameters['L_rz']
    top_kappa = parameters['chi_top']
    bot_kappa = parameters['kappa_ratio']*top_kappa
    step_kappa = np.zeros(z.shape[0])
    step_kappa[np.where(z < parameters['L_rz'])] = bot_kappa
    step_kappa[np.where(z >= parameters['L_rz'])] = top_kappa
    return step_kappa


def get_analytic_T(z, parameters):
    '''
    Calculates the analytical T profile of the given polytrope using a
    step-function based derivative and a top boundary condition of T=1.
 
    ######ARGUMENTS######
    domain      - The dedalus domain which we're creating our atmosphere on
    z           - The NumPy array of z values in our dedalus domain.
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmosphere_parameters()
    '''
    L_rz    = parameters['L_rz']
    L_cz    = parameters['L_cz']
    L_tot   = parameters['L_tot']
    kappa_ratio=parameters['kappa_ratio']
    T       = np.zeros(z.shape[0])
    T[np.where(z >=L_rz)] = (L_tot + 1 - z[np.where(z >= L_rz)])
    T[np.where(z < L_rz)] = L_cz + 1 + (L_rz - z[np.where(z < L_rz)])/kappa_ratio
    return T

def get_analytic_rho(z, parameters, T):
    '''
    Calculates the analytical rho profile of the given polytrope using the
    adiabatic indices and a calculated analytical T profile.

    ######ARGUMENTS######
    z           - The NumPy array of z values in our dedalus domain
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmospheric_parameters()
    T           - An analytically calculated T profile, as returned by
                    get_analytic_T()
    '''
    m_rz = parameters['m_rz']
    m_cz = parameters['m_cz']
    L_rz = parameters['L_rz']
    L_tot = parameters['L_tot']
    L_cz = parameters['L_cz']
    rho_a  = np.zeros(z.shape[0])
    rho_a[np.where(z >=L_rz)] = T[np.where(z >= L_rz)]**m_cz
    rho_a[np.where(z < L_rz)] = (L_cz + 1)**m_cz * (T[np.where(z < L_rz)]/(L_cz + 1))**m_rz
    return rho_a

def get_analytic_dzlnrho(z, parameters, T):
    '''
    Calculates the analytical profile of the vertical gradient of ln(rho) of
    the given polytrope using adiabatic indices and a calculated analytical
    T profile

    ######ARGUMENTS######
    z           - The NumPy array of z values in our dedalus domain
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmospheric_parameters()
    T           - An analytically calculated T profile, as returned by
                    get_analytic_T()
    '''
    m_rz = parameters['m_rz']
    m_cz = parameters['m_cz']
    L_rz = parameters['L_rz']
    dzlnrho_a = np.zeros(z.shape[0])
    dzlnrho_a[np.where(z >=L_rz)] = -m_cz/T[np.where(z >=L_rz)]
    dzlnrho_a[np.where(z < L_rz)] = -m_rz/T[np.where(z < L_rz)]
    return dzlnrho_a

def calculate_del_T(kappa, del_T, flux_top):
    '''
    Calculates a numerical del_T profile for the given polytropic atmosphere
    using dedalus' functionality.

    ######ARGUMENTS######
    kappa       - A tanh profile of the atmospheric kappa values.
    del_T       - A dedalus field in which to store the gradient of T
    flux_top    - The heat flux at the top of the domain, used to determine
                    the T profile.
    '''
    del_T.set_scales((1,), keep_data=True)
    del_T['g'] = flux_top/kappa['g']
    del_T.set_scales((1,), keep_data=True)
    return del_T

def calculate_T(kappa, del_T, T, flux_top):
    '''
    Calculates a numerical T profile for the given polytropic atmosphere
    using dedalus' functionality.

    ######ARGUMENTS######
    kappa       - A tanh profile of the atmospheric kappa values.
    del_T       - A dedalus field in which to store the gradient of T
    T           - A dedalus field in which to store T
    flux_top    - The heat flux at the top of the domain, used to determine
                    the T profile.
    '''
    del_T = calculate_del_T(kappa, del_T, flux_top)
    del_T.antidifferentiate('z', ('right',0), out=T)
    T['g'] += 1
    T.set_scales((1,), keep_data=True)
    del_T.set_scales((1,), keep_data=True)
    return T



def calculate_rho(domain, parameters, z, T, rho):
    '''
    Calculates a numerical rho profile for the given polytropic atmosphere
    using dedalus'functionality.

    ######ARGUMENTS######
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmospheric_parameters()
    z           - The NumPy array of z values in our dedalus domain
    T           - A numerically calculated T profile as returned by
                    calculate_T()
    rho         - A dedalus field in which to store rho
    '''
    g = parameters['g']
    del_ln_P = domain.new_field()
    ln_P = domain.new_field()
    P = domain.new_field()

    del_ln_P['g'] = g/T['g']
    del_ln_P.antidifferentiate('z', ('right',0),out=ln_P)
    ln_P.set_scales((1,), keep_data=True)
    P['g'] = np.exp(-ln_P['g'])
    rho['g'] = P['g']/T['g']
    rho.set_scales((1,), keep_data=True)
    return rho

def calculate_grad_log_rho(rho, dzlnrho):
    '''
    Calculates a numerical vertical gradient of ln(rho) using dedalus'
    functionality

    ######ARGUMENTS######
    rho         - A dedalus field containing rho as returned by calculate_rho()
    dzlnrho     - A dedalus field in which to store the grad of ln(rho)
    '''
    rho.differentiate('z', out=dzlnrho)
    dzlnrho['g'] /= rho['g']
    rho.set_scales((1,), keep_data=True)
    dzlnrho.set_scales((1,), keep_data=True)
    return dzlnrho

def calculate_chi_grad_log_rho(domain, kappa, rho, dzlnrho):
    '''
    Calculates chi * grad ln(rho).

    ######ARGUMENTS######
    domain      - The dedalus domain which we're creating our atmosphere on
    kappa       - A dedalus field containing a tanh kappa profile
    rho         - A dedalus field containing rho, as returned by calculate_rho()
    dzlnrho     - A dedalus field containing dzlnrho,
                    as returned by calculate_grad_log_rho()
    '''
    chidzlnrho = domain.new_field()
    chidzlnrho['g'] = kappa['g'] * dzlnrho['g'] / (rho['g']**2)
    return chidzlnrho

def calculate_nu_grad_log_rho(domain, rho, dzlnrho, nu=1):
    '''
    Calculates nu * grad ln(rho).  Assumes a constant value of nu = 1
            Throughout the entire domain unless otherwise specified.

    ######ARGUMENTS######
    domain      - The dedalus domain which we're creating our atmosphere on
    rho         - A dedalus field containing rho, as returned by calculate_rho()
    dzlnrho     - A dedalus field containing dzlnrho,
                    as returned by calculate_grad_log_rho()
    nu          - The value of nu throughout the domain.  Must be a constant
                    or a numpy array of the domain size.
    '''
    nudzlnrho = domain.new_field()
    nudzlnrho['g'] = nu * dzlnrho['g'] / (rho['g']**2)
    return nudzlnrho

def calculate_chi(domain, kappa, rho):
    '''
    Calculates chi.

    ######ARGUMENTS######
    domain      - The dedalus domain which we're creating our atmosphere on
    kappa       - A dedalus field containing a tanh kappa profile
    rho         - A dedalus field containing rho, as returned by calculate_rho()
    '''
    chi = domain.new_field()
    chi['g'] = kappa['g']/rho['g']
    return chi

def calculate_grad_chi(domain, kappa, rho, dzlnrho):
    '''
    Calculates the gradient of chi

    ######ARGUMENTS######
    domain      - The dedalus domain which we're creating our atmosphere on
    kappa       - A dedalus field containing a tanh kappa profile
    rho         - A dedalus field containing rho, as returned by calculate_rho()
    dzlnrho     - A dedalus field containing dzlnrho,
                    as returned by calculate_grad_log_rho()
    '''
    gradchi = domain.new_field()
    chi = calculate_chi(domain, kappa, rho)
    chi.differentiate('z', out=gradchi)
    return gradchi


def power_spectrum_v_width(ax, widths, coeffs, domain, z, parameters,\
                            field='kappa', cmap='Reds', minplot=1e-10,\
                            field_func=None,field_args=[], field_kwargs=dict(),\
                            multiply_T=False, multiply_rho=False,\
                            multiply_rho2=False, smoothing=None):
    '''
    Calculates the power spectrum of dedalus' Chebyshev expansion of the
    given atmospheric parameter for a given range of tanh widths and
    coefficients.  Plots the NORMALIZED (max=1) power spectrum in a color
    map plot.

    ######ARGUMENTS######
    widths      - a 1d NumPy array of tanh widths in units of Lz (e.g. 0.1 in 
                    widths corresponds to a tanh width of 0.1Lz)
    coeffs      - a 1d Numpy array of Chebyshev coefficients starting at 0
                    and going to the maximum coefficient you're interested
                    in seeing plotted.
    ax          - a matplotlib subplot which will be wiped and on which the
                    power spectrum colormap will be plotted
    domain      - The dedalus domain which we're creating our atmosphere on
    z           - The NumPy array of z values in our dedalus domain.
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmosphere_parameters()

    ######KEYWORD ARGUMENTS######
    field       - The field to plot the power spectrum of. 
                    **Accepts 'kappa', 'temp', 'temperature', 'rho', or 
                    'grad log rho', or something
                    else if 'field_args' kwarg != None**
    cmap        - The matplotlib colormap to use in plotting
    minplot     - The floor saturation value of the colormap.  Sets the
                    bottom value of the colormap's color scale.
    field_func  - If field kwarg is not a known string, this must contain a
                    function to calculate the field
    field_args  - arguments for field_func, if contains the strings
                    'kappa' or 'temp' or 'temperature' or 'rho' or 
                    'grad log rho', the function
                    will substitute those args into the passed list.
    field_kwargs - kwargs for field_func
    multiply_T  - Multiply the field by T before viewing power spectrum if
                    set to true
    multiply_rho- Multiply the field by rho before viewing power spectrum if
                    set to true
    multiply_rho2-Multiply the field by rho**2 before viewing power spectrum if
                    set to true.
    smoothing    - A smoothing function to apply before seeing the power
                    spectrum
    '''
    if str(field).lower() not in ['kappa', 'temp', 'temperature',\
                                    'grad log rho', 'rho']:
        if field_func==None:
            print('Unknown plot field -- not plotting power spectrum.')
            return
    elif str(field).lower() == 'rho':
        field='rho'
    elif str(field).lower() == 'grad log rho':
        field='grad log rho'
    elif str(field).lower() in ['temp', 'temperature']:
        field='temp'
    else:
        field='kappa'

    #Unpack parameters
    Lz = parameters['L_tot']
    flux_top = -parameters['chi_top']
    ax.clear()
    coeffs_2d, widths_2d = np.meshgrid(coeffs, widths)
    kappa = domain.new_field()
    del_T = domain.new_field()
    T     = domain.new_field()
    rho   = domain.new_field()
    dzlnrho=domain.new_field()
    powers = np.zeros(widths_2d.shape)

    if field_args != []:
        for i in range(len(field_args)):
            if str(field_args[i]).lower() in ['temp', 'temperature']:
                field_args[i] = T
            if str(field_args[i]).lower() == 'kappa':
                field_args[i] = kappa
            if str(field_args[i]).lower() == 'rho':
                field_args[i] = rho
            if str(field_args[i]).lower() == 'grad log rho':
                field_args[i] = dzlnrho
            if str(field_args[i]).lower() == 'domain':
                field_args[i] = domain
            if str(field_args[i]).lower() == 'z':
                field_args[i] = z

    #Calculate all the profiles' power spectra
    for i in range(widths.shape[0]):
        width = widths[i]*Lz
        compute_kappa_profile(parameters, z, kappa, tanh_width=width)
        if field=='kappa':
            powers[i,:] = kappa['c'][:coeffs.shape[0]]
            continue
        elif field=='temp':
            T = calculate_T(kappa, del_T, T, flux_top)
            if multiply_T:
                T['g'] = T['g']**2
            if multiply_rho:
                rho = calculate_rho(domain, parameters, z, T, rho)
                T['g'] = T['g']*rho['g']
            if multiply_rho2:
                rho = calculate_rho(domain, parameters, z, T, rho)
                T['g'] = T['g']*rho['g']**2
            if smoothing is not None:
                T['g'] = T['g']*smoothing
            powers[i,:] = T['c'][:coeffs.shape[0]]
        elif field=='rho':
            T = calculate_T(kappa, del_T, T, flux_top)
            rho = calculate_rho(domain, parameters, z, T, rho)
            if multiply_T:
                rho['g'] = T['g']*rho['g']
            if multiply_rho:
                rho['g'] = rho['g']**2
            if multiply_rho2:
                rho['g'] = rho['g']**3
            if smoothing is not None:
                rho['g'] = rho['g']*smoothing
            powers[i,:] = rho['c'][:coeffs.shape[0]]
        elif field=='grad log rho':
            T = calculate_T(kappa, del_T, T, flux_top)
            rho = calculate_rho(domain, parameters, z, T, rho)
            dzlnrho = calculate_grad_log_rho(rho, dzlnrho)
            if multiply_T:
                dzlnrho['g'] = T['g']*dzlnrho['g']
            if multiply_rho:
                dzlnrho['g'] = dzlnrho['g']*rho['g']
            if multiply_rho2:
                dzlnrho['g'] = dzlnrho['g']*rho['g']**2
            if smoothing is not None:
                dzlnrho['g'] = smoothing*dzlnrho['g']
            powers[i,:] = dzlnrho['c'][:coeffs.shape[0]]
        else:
            T = calculate_T(kappa, del_T, T, flux_top)
            rho = calculate_rho(domain, parameters, z, T, rho)
            dzlnrho = calculate_grad_log_rho(rho, dzlnrho)
            other = field_func(*field_args, **field_kwargs)
            if multiply_T:
                other['g'] = T['g']*other['g']
            if multiply_rho:
                other['g'] = rho['g']*other['g']
            if multiply_rho2:
                other['g'] = rho['g']**2 * other['g']
            if smoothing is not None:
                other['g'] = other['g']*smoothing
            powers[i,:] = other['c'][:coeffs.shape[0]]

    powers = np.absolute(powers)**2
    powers /= np.max(powers)

    #Plot it all
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    im = ax.pcolormesh(coeffs_2d, widths_2d, powers, \
            norm=matplotlib.colors.LogNorm(),\
            cmap=cmap, vmin=minplot, vmax=1)
    title_string = 'Norm. Power Spectrum: '+field.upper()
    if multiply_T:
        title_string += ' * T'
    if multiply_rho:
        title_string += ' * RHO'
    if multiply_rho2:
        title_string += ' * RHO^2'
    if smoothing is not None:
        title_string += ' * smoother'
    ax.set_title(title_string)
    ax.set_xlabel('Chebyshev Order')
    ax.set_ylabel('Tanh Width (Lx)')
    ax.set_xlim(coeffs[0], coeffs[-1])
    ax.set_ylim(widths[0], widths[-1])
    cbar = plt.colorbar(im, cax=cax)
    im.set_clim(minplot, 1)
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel('Normalized Power', rotation=270)


def plot_residuals(ax, bx, widths, coeffs, domain, z, parameters,\
                    field='kappa', cmap_1='Blues', cmap_2='Greens'):
    '''
    Plots the residual between a truncated series and a non-truncated series,
    as well as between a truncated series and a not-smooth analytical function
 
    ######ARGUMENTS######
    widths      - a 1d NumPy array of tanh widths in units of Lz (e.g. 0.1 in 
                    widths corresponds to a tanh width of 0.1Lz)
    coeffs      - a 1d Numpy array of Chebyshev coefficients starting at 0
                    and going to the maximum coefficient you're interested
                    in seeing plotted.
    ax          - a matplotlib subplot which will be wiped and on which the
                    power spectrum colormap will be plotted
    domain      - The dedalus domain which we're creating our atmosphere on
    z           - The NumPy array of z values in our dedalus domain.
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmosphere_parameters()

    ######KEYWORD ARGUMENTS######
    field       - The field to plot the residua of. 
                    **Accepts 'kappa', 'temp', or 'temperature'**
    cmap_1      - The matplotlib colormap to use for the SERIES residual
    cmap_2      - The matplotlib colormap to use for the ANALYTICAL residual
    '''
    if str(field).lower() not in ['kappa', 'temp', 'temperature', 'rho', \
                                    'grad log rho']:
        print('Unknown plot field -- not plotting residuals.')
        return
    elif str(field).lower() in ['temp', 'temperature']:
        field='temp'
    elif str(field).lower() == 'rho':
        field='rho'
    elif str(field).lower() == 'grad log rho':
        field='grad log rho'
    else:
        field='kappa'

   #Unpack parameters
    Lz = parameters['L_tot']
    flux_top = -parameters['chi_top']
    ax.clear()
    bx.clear()
    coeffs_2d, widths_2d = np.meshgrid(coeffs, widths)
    kappa = domain.new_field()
    kappa_full = domain.new_field()
    del_T = domain.new_field()
    T = domain.new_field()
    T_full = domain.new_field()
    rho = domain.new_field()
    rho_full = domain.new_field()
    dzlnrho = domain.new_field()
    dzlnrho_full = domain.new_field()
    residuals_expansion = np.zeros(widths_2d.shape)
    residuals_analytic = np.zeros(widths_2d.shape)
    nz = z.shape[0]
    max_coeff = coeffs.shape[0]-1

    #get analytical expressions
    step_kappa = get_step_kappa(z, parameters)
    analytic_T = get_analytic_T(z, parameters)
    analytic_rho = get_analytic_rho(z, parameters, analytic_T)
    analytic_dzlnrho = get_analytic_dzlnrho(z, parameters, analytic_T)

    #Calculate residuals
    for i in range(widths.shape[0]):
        width = widths[i]*Lz
        compute_kappa_profile(parameters, z, kappa_full, tanh_width = width)
        compute_kappa_profile(parameters, z, kappa, tanh_width = width)

        if field=='temp':
            T = calculate_T(kappa, del_T, T, flux_top)
            T_full['g'] = T['g']
            T['c'][max_coeff+1:] = np.zeros(nz-max_coeff-1)
        elif field=='kappa':
            kappa['c'][max_coeff+1:] = np.zeros(nz-max_coeff-1)
        elif field=='rho':
            T = calculate_T(kappa, del_T, T, flux_top)
            rho = calculate_rho(domain, parameters, z, T, rho)
            rho_full['g'] = rho['g']
            rho['c'][max_coeff+1:] = np.zeros(nz-max_coeff-1)
        elif field=='grad log rho':
            T = calculate_T(kappa, del_T, T, flux_top)
            rho = calculate_rho(domain, parameters, z, T, rho)
            dzlnrho = calculate_grad_log_rho(rho, dzlnrho)
            dzlnrho_full['g'] = dzlnrho['g']
            dzlnrho['c'][max_coeff+1:] = np.zeros(nz-max_coeff-1)


        for j in range(coeffs.shape[0]):
            if field=='kappa':
                residuals_expansion[i,max_coeff-j] = \
                    np.sqrt(np.sum((kappa['g']-kappa_full['g'])**2))
                residuals_analytic[i,max_coeff-j] = \
                    np.sqrt(np.sum((kappa['g']-step_kappa)**2))
                kappa['c'][max_coeff-j] = 0 #Cut off one coeff each time
            elif field=='temp':
                residuals_expansion[i,max_coeff-j] = \
                    np.sqrt(np.sum((T['g']-T_full['g'])**2))
                residuals_analytic[i,max_coeff-j] = \
                    np.sqrt(np.sum((T['g']-analytic_T)**2))
                T['c'][max_coeff-j] = 0 #Cut off one coeff each time
            elif field=='rho':
                residuals_expansion[i,max_coeff-j] = \
                    np.sqrt(np.sum((rho['g']-rho_full['g'])**2))
                residuals_analytic[i,max_coeff-j] = \
                    np.sqrt(np.sum((rho['g']-analytic_rho)**2))
                rho['c'][max_coeff-j] = 0 #Cut off one coeff each time
            elif field=='grad log rho':
                residuals_expansion[i,max_coeff-j] = \
                    np.sqrt(np.sum((dzlnrho['g']-dzlnrho_full['g'])**2))
                residuals_analytic[i,max_coeff-j] = \
                    np.sqrt(np.sum((dzlnrho['g']-analytic_dzlnrho)**2))
                dzlnrho['c'][max_coeff-j] = 0 #Cut off one coeff each time

    #Plot coefficient residuals
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    im = ax.pcolormesh(coeffs_2d, widths_2d, residuals_expansion, \
                        norm=matplotlib.colors.LogNorm(),\
                        cmap=cmap_1)
    ax.set_title('Residual (Truncation): '+field.upper())
    ax.set_xlabel('Truncation order')
    ax.set_ylabel('Tanh Width (Lx)')
    ax.set_xlim(coeffs[0], coeffs[-1])
    ax.set_ylim(widths[0], widths[-1])
    cbar = plt.colorbar(im, cax=cax)
    im.set_clim(np.min(residuals_expansion), np.max(residuals_expansion))
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel(r'$\ell_2$ norm', rotation=270)
    
    #Plot step residuals
    divider = make_axes_locatable(bx)
    cbx = divider.append_axes('right', size='5%', pad=0.05)
    im = bx.pcolormesh(coeffs_2d, widths_2d, residuals_analytic, \
                        cmap=cmap_2)
    bx.set_title('Residual (Analytic): '+field.upper())
    bx.set_xlabel('Truncation order')
    bx.set_ylabel('Tanh Width (Lx)')
    bx.set_xlim(coeffs[0], coeffs[-1])
    bx.set_ylim(widths[0], widths[-1])
    cbar = plt.colorbar(im, cax=cbx)
    im.set_clim(np.min(residuals_analytic), np.max(residuals_analytic))
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel('$\ell_2$ norm', rotation=270)
   
def plot_analytical_difference(ax, widths, domain, z, parameters,\
                                field='kappa', cmap='Oranges'):
    '''
    Plots the difference between the specified atmospheric parameter and
    the analytic representation we're approximating.
 
    ######ARGUMENTS######
    ax          - a matplotlib subplot which will be wiped and on which the
                    power spectrum colormap will be plotted
    widths      - a 1d NumPy array of tanh widths in units of Lz (e.g. 0.1 in 
                    widths corresponds to a tanh width of 0.1Lz)
    coeffs      - a 1d Numpy array of Chebyshev coefficients starting at 0
                    and going to the maximum coefficient you're interested
                    in seeing plotted.
    domain      - The dedalus domain which we're creating our atmosphere on
    z           - The NumPy array of z values in our dedalus domain.
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmosphere_parameters()

    ######KEYWORD ARGUMENTS######
    field       - The field to plot the difference vs. analytical of. 
                    **Accepts 'kappa', 'temp', or 'temperature'**
    cmap        - The matplotlib colormap to use for the plot

    '''
    if str(field).lower() not in ['kappa', 'temp', 'temperature', 'rho', \
                                    'grad log rho']:
        print('Unknown plot field -- not plotting residuals.')
        return
    elif str(field).lower() in ['temp', 'temperature']:
        field='temp'
    elif str(field).lower() == 'rho':
        field='rho'
    elif str(field).lower() == 'grad log rho':
        field='grad log rho'
    else:
        field='kappa'
    #Unpack parameters
    Lz = parameters['L_tot']
    flux_top = -parameters['chi_top']
    ax.clear()
    zs_2d, widths_2d = np.meshgrid(z, widths)
    kappa = domain.new_field()
    del_T = domain.new_field()
    T = domain.new_field()
    rho = domain.new_field()
    dzlnrho = domain.new_field()
    diffs = np.zeros(widths_2d.shape)
    nz = z.shape[0]

    #get analytic function
    step_kappa = get_step_kappa(z, parameters)
    analytic_T = get_analytic_T(z, parameters)
    analytic_rho = get_analytic_rho(z, parameters, analytic_T)
    analytic_dzlnrho = get_analytic_dzlnrho(z, parameters, analytic_T)


    #Calculate differences
    for i in range(widths.shape[0]):
        width = widths[i]*Lz
        compute_kappa_profile(parameters, z, kappa, tanh_width=width)
        if field=='kappa':
            diffs[i,:] = np.absolute(step_kappa-kappa['g'])
        elif field=='temp':
            T = calculate_T(kappa, del_T, T, flux_top)
            diffs[i,:] = np.absolute(analytic_T - T['g'])
        elif field=='rho':
            T = calculate_T(kappa, del_T, T, flux_top)
            rho = calculate_rho(domain, parameters, z, T, rho)
            diffs[i,:] = np.absolute(analytic_rho - rho['g'])
        elif field=='grad log rho':
            T = calculate_T(kappa, del_T, T, flux_top)
            rho = calculate_rho(domain, parameters, z, T, rho)
            dzlnrho = calculate_grad_log_rho(rho, dzlnrho)
            diffs[i,:] = np.absolute(analytic_dzlnrho - dzlnrho['g'])
    #Plot differences
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    if np.min(diffs) != 0 and np.abs(np.max(diffs)/np.min(diffs)) > 10:
        im = ax.pcolormesh(zs_2d, widths_2d, diffs, \
                        norm=matplotlib.colors.LogNorm(), cmap=cmap)
    else:
        im = ax.pcolormesh(zs_2d, widths_2d, diffs, cmap=cmap)
    ax.set_title('Diff from Analytical: '+field.upper())
    ax.set_xlabel('z')
    ax.set_ylabel('Tanh Width (Lx)')
    ax.set_xlim(z[0], z[-1])
    ax.set_ylim(widths[0], widths[-1])
    cbar = plt.colorbar(im, cax=cax)
    im.set_clim(np.min(diffs), np.max(diffs))
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel(r'Difference', rotation=270)

def plot_entropy_gradient(domain, z, coeffs, parameters, fig, width=0.01):
    '''
    Plots whether the entropy gradient is positive or negative & the absolute
    value of the entropy gradient across the domain for various truncations.

    
    '''
    fig.clear()
    ax = fig.add_subplot(1,2,1)
    bx = fig.add_subplot(1,2,2)


    coeffs_2d, z_2d = np.meshgrid(coeffs, z)
    kappa = domain.new_field()
    T = domain.new_field()
    del_T = domain.new_field()
    rho = domain.new_field()
    dzlnrho = domain.new_field()
    grad_S = domain.new_field()
    sign = np.zeros(z_2d.shape)
    grad_S_abs = np.zeros(z_2d.shape)

    flux_top = -parameters['chi_top']
    compute_kappa_profile(parameters, z, kappa, tanh_width=width)
    T = calculate_T(kappa, del_T, T, flux_top)
    rho = calculate_rho(domain, parameters, z, T, rho)
    dzlnrho = calculate_grad_log_rho(rho, dzlnrho)
    max_coeff = coeffs.shape[0]-1
    nz = z.shape[0]



    gamma = parameters['gamma']
    cp = gamma/(gamma-1)

    grad_S['g'] = (cp/gamma)*del_T['g']/T['g'] - dzlnrho['g']
    grad_S['c'][max_coeff+1:] = np.zeros(nz-max_coeff-1)

    for i in range(len(coeffs)):
        sign[np.where(grad_S['g'] < 0), max_coeff-i] = -1
        sign[np.where(grad_S['g'] > 0), max_coeff-i] = 1
        grad_S_abs[:,max_coeff-i] = np.abs(grad_S['g'])
        grad_S['c'][max_coeff-i] = 0 

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    im = ax.pcolormesh(coeffs_2d, z_2d, grad_S_abs, cmap='Reds')
    ax.set_title('Abs(grad S)')
    ax.set_xlabel('Truncation order')
    ax.set_ylabel('z')
    ax.set_ylim(z[0], z[-1])
    ax.set_xlim(coeffs[0], coeffs[-1])
    cbar = plt.colorbar(im, cax=cax)
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel(r'Absolute grad S', rotation=270)

    divider = make_axes_locatable(bx)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    im = bx.pcolormesh(coeffs_2d, z_2d, sign, cmap='RdBu_r')
    bx.set_title('Sign(grad S)')
    bx.set_xlabel('Truncation order')
    bx.set_ylabel('z')
    bx.set_ylim(z[0], z[-1])
    bx.set_xlim(coeffs[0], coeffs[-1])
    cbar = plt.colorbar(im, cax=cax)
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel(r'Sign(grad S)', rotation=270)
    fig.suptitle('S = {0:.6g}, EPS = {1:.4g}, KTOP = {2:.4g}'.\
                format(params['stiffness'], params['epsilon'], \
                params['chi_top'])\
                +', LRZ = {0:.4g}, LTOT = {1:.4g},'.\
                format(params['L_rz'], params['L_tot'])+\
                'WIDTH = {0:.4g}'.format(width),\
                fontsize=16)
    fig.savefig(SAVEDIR_BASE+'ENTROPY_GRAD_WINV{:3d}'.format(int(1/width))\
                +".png", dpi=100)



def plot_numerical(domain, z, parameters, fig, savename="numerical_profiles"):
    '''
    Plots a simple, two-subplot image of the kappa and T profile for the
    specified atmosphere.

    ######ARGUMENTS######
    fig         - The matplotlib.pyplot figure on which to create these plots
    domain      - The dedalus domain which we're creating our atmosphere on
    z           - The NumPy array of z values in our dedalus domain.
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmosphere_parameters()
    
    ######KEYWORD ARGUMENTS######
    savename    - The name under which to save the output figure in the
                    directory specified by SAVEDIR_BASE
    

    '''
    fig.clear()
    ax = fig.add_subplot(2,2,1)
    bx = fig.add_subplot(2,2,2)
    cx = fig.add_subplot(2,2,3)
    dx = fig.add_subplot(2,2,4)
   
    kappa = domain.new_field()
    T = domain.new_field()
    del_T = domain.new_field()
    rho = domain.new_field()
    dzlnrho = domain.new_field()

    width = 0.01
    flux_top = -parameters['chi_top']
    compute_kappa_profile(parameters, z, kappa, tanh_width=width)
    T = calculate_T(kappa, del_T, T, flux_top)
    rho = calculate_rho(domain, parameters, z, T, rho)
    dzlnrho = calculate_grad_log_rho(rho, dzlnrho)
    
    kappa = kappa['g']
    T = T['g']
    rho = rho['g']
    dzlnrho = dzlnrho['g']
    
    ax.plot(z, kappa)
    bx.plot(z, T)
    cx.plot(z, rho)
    dx.plot(z, dzlnrho)
    ax.axvline(parameters['L_rz'], linestyle='--')
    bx.axvline(parameters['L_rz'], linestyle='--')
    cx.axvline(parameters['L_rz'], linestyle='--')
    dx.axvline(parameters['L_rz'], linestyle='--')

    ax.set_title('Kappa')
    bx.set_title('Temperature')
    cx.set_title('Rho')
    dx.set_title('Grad log rho')
    ax.set_xlabel('z')
    bx.set_xlabel('z')
    cx.set_xlabel('z')
    dx.set_ylabel('z')
    ax.set_ylabel(r'$\kappa$')
    bx.set_ylabel('T')
    cx.set_ylabel('rho')
    dx.set_ylabel('Grad log rho')

    ax.set_xlim(0, parameters['L_tot'])
    bx.set_xlim(0, parameters['L_tot'])
    cx.set_xlim(0, parameters['L_tot'])
    dx.set_xlim(0, parameters['L_tot'])
    ax.set_ylim(np.min(kappa)-0.05*np.max(kappa), np.max(kappa)*1.05)
    bx.set_ylim(0, np.max(T)+1)
    fig.suptitle('S = {0:.6g}, EPS = {1:.4g}, KTOP = {2:.4g}'.\
                format(params['stiffness'], params['epsilon'], \
                params['chi_top'])\
                +', LRZ = {0:.4g}, LTOT = {1:.4g}'.\
                format(params['L_rz'], params['L_tot']),\
                fontsize=16)
    fig.savefig(SAVEDIR_BASE+savename+".png", dpi=100)

def plot_analytical(domain, z, parameters, fig, savename="analytic_profiles"):
    '''
    Plots a simple, two-subplot image of the kappa and T profile for the
    specified atmosphere.

    ######ARGUMENTS######
    fig         - The matplotlib.pyplot figure on which to create these plots
    domain      - The dedalus domain which we're creating our atmosphere on
    z           - The NumPy array of z values in our dedalus domain.
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmosphere_parameters()
    
    ######KEYWORD ARGUMENTS######
    savename    - The name under which to save the output figure in the
                    directory specified by SAVEDIR_BASE
    

    '''
    fig.clear()
    ax = fig.add_subplot(2,2,1)
    bx = fig.add_subplot(2,2,2)
    cx = fig.add_subplot(2,2,3)
    dx = fig.add_subplot(2,2,4)

    kappa = get_step_kappa(z, parameters)
    analytic_T = get_analytic_T(z, parameters)
    analytic_rho = get_analytic_rho(z, parameters, analytic_T)
    analytic_dzlnrho = get_analytic_dzlnrho(z, parameters, analytic_T)
                
    ax.plot(z, kappa)
    bx.plot(z, analytic_T)
    cx.plot(z, analytic_rho)
    dx.plot(z, analytic_dzlnrho)
    ax.axvline(parameters['L_rz'], linestyle='--')
    bx.axvline(parameters['L_rz'], linestyle='--')
    cx.axvline(parameters['L_rz'], linestyle='--')
    dx.axvline(parameters['L_rz'], linestyle='--')

    ax.set_title('Kappa')
    bx.set_title('Temperature')
    cx.set_title('Rho')
    dx.set_title('Grad log rho')
    ax.set_xlabel('z')
    bx.set_xlabel('z')
    cx.set_xlabel('z')
    dx.set_ylabel('z')
    ax.set_ylabel(r'$\kappa$')
    bx.set_ylabel('T')
    cx.set_ylabel('rho')
    dx.set_ylabel('Grad log rho')

    ax.set_xlim(0, parameters['L_tot'])
    bx.set_xlim(0, parameters['L_tot'])
    cx.set_xlim(0, parameters['L_tot'])
    dx.set_xlim(0, parameters['L_tot'])
    ax.set_ylim(np.min(kappa)-0.05*np.max(kappa), np.max(kappa)*1.05)
    bx.set_ylim(0, np.max(analytic_T)+1)
    fig.suptitle('S = {0:.6g}, EPS = {1:.4g}, KTOP = {2:.4g}'.\
                format(params['stiffness'], params['epsilon'], \
                params['chi_top'])\
                +', LRZ = {0:.4g}, LTOT = {1:.4g}'.\
                format(params['L_rz'], params['L_tot']),\
                fontsize=16)
    fig.savefig(SAVEDIR_BASE+savename+".png", dpi=100)

def explore_stiffness(fig, start_pow, end_pow, numfigs, widths, coeffs,\
                        savename='S_exploration', field='kappa'):
    '''
    Creates a series of plots over a logarithmic stiffness space in order
    to better understand the affects of varying stiffness on a certain field.

    ######ARGUMENTS######
    fig         - The matplotlib.pyplot figure on which to create these plots
    start_pow   - The power of 10 at which to start exploring stiffness 
                    (1 corresponds to S=10, 2 corresponds to S=100, etc.)
    end_pow     - The power of 10 at which to end exploring stiffness
    numfigs     - The number of steps to take between start_pow and end_pow
                    (this many figures will be saved)
    widths      - The tanh widths (in units of Lz) to test (NumPy 1d array)
    coeffs      - A NumPy 1d array from 0 to the highest order coefficient
                    to test.

    ######KEYWORD ARGUMENTS######
    savename    - The name under which to save the output figure in the
                    directory specified by SAVEDIR_BASE.  The number of the
                    figure will be appended to the end of the file.
    field       - The field to examine the changes of
                        ['temp', 'temperature', or 'kappa'] 
    '''
    S_space = np.logspace(start_pow, end_pow, numfigs)
    for i in range(N_plots):
        fig.clear()
        ax = fig.add_subplot(2,2,1)
        bx = fig.add_subplot(2,2,2)
        cx = fig.add_subplot(2,2,3)
        dx = fig.add_subplot(2,2,4)

        stiff = S_space[i]
        print("Iter {0}; stiffness {1}".format(i+1, stiff))

        #Setup atmosphere & Domain
        params = compute_atmosphere_parameters(Rayleigh_top=RA_TOP,\
                Prandtl_top=PR_TOP, gamma=GAMMA, n_rho_cz=N_RHO_CZ,\
                n_rho_rz=N_RHO_RZ, m_rz=M_RZ, stiffness=stiff)

        Lz = params['L_tot']
        z_basis = de.Chebyshev('z', N_PTS, interval=(0,Lz), dealias=3/2)
        domain = de.Domain([z_basis], grid_dtype=np.float64)
        z = domain.grid(0)

        fig.suptitle('S = {0:.6g}, EPS = {1:.4g}, KTOP = {2:.4g}'.\
                format(params['stiffness'], params['epsilon'], \
                params['chi_top'])\
                +', LRZ = {0:.4g}, LTOT = {1:.4g}'.\
                format(params['L_rz'], params['L_tot']),\
                fontsize=16)
        power_spectrum_v_width(ax, widths, coeffs, domain, z, params,\
                field=field)
        plot_residuals(bx, dx, widths, coeffs, domain, z, params, field=field)
        plot_analytical_difference(cx, widths, domain, z, params, field=field)
        plt.subplots_adjust(top=0.88, wspace=0.5, hspace=0.4)
        if field == 'grad log rho':
            savefield = 'dzlnrho'
            fig.savefig(SAVEDIR_BASE+savename+"_"+savefield+\
                '_{0:04d}.png'.format(i+1), dpi=100)
            continue
        fig.savefig(SAVEDIR_BASE+savename+"_"+field+\
                '_{0:04d}.png'.format(i+1), dpi=100)


def plot_ncc_powers(domain, z, parameters, widths, coeffs, fig, \
                                            savename="NCCs_powers"):
    '''
    Plots the power spectra of all NCCs which show up in dedalus expansions.

    ######ARGUMENTS######
    fig         - The matplotlib.pyplot figure on which to create these plots
    domain      - The dedalus domain which we're creating our atmosphere on
    z           - The NumPy array of z values in our dedalus domain.
    parameters  - A dictionary of atmospheric parameters, as returned by
                    multiple_polytrope.compute_atmosphere_parameters()
    
    ######KEYWORD ARGUMENTS######
    savename    - The name under which to save the output figure in the
                    directory specified by SAVEDIR_BASE
    '''
    fig.clear()
    XPLOTS=2
    YPLOTS=2
    ax = fig.add_subplot(YPLOTS,XPLOTS,1)
    bx = fig.add_subplot(YPLOTS,XPLOTS,2)
    cx = fig.add_subplot(YPLOTS,XPLOTS,3)
    dx = fig.add_subplot(YPLOTS,XPLOTS,4)
    '''
    ex = fig.add_subplot(YPLOTS,XPLOTS,5)
    fx = fig.add_subplot(YPLOTS,XPLOTS,6)
    gx = fig.add_subplot(YPLOTS,XPLOTS,7)
    hx = fig.add_subplot(YPLOTS,XPLOTS,8)
    '''
    power_spectrum_v_width(ax, widths, coeffs, domain, z, parameters, \
                field='CHI', cmap='Greens',\
                field_func=calculate_chi,\
                field_args=['domain', 'kappa', 'rho'])
    power_spectrum_v_width(bx, widths, coeffs, domain, z, parameters, \
                field='CHI', multiply_T=True,\
                field_func=calculate_chi,\
                field_args=['domain', 'kappa', 'rho'])

    power_spectrum_v_width(cx, widths, coeffs, domain, z, parameters, \
                field='CHI', multiply_rho=True, cmap='Oranges',\
                field_func=calculate_chi,\
                field_args=['domain', 'kappa', 'rho'])

    power_spectrum_v_width(dx, widths, coeffs, domain, z, parameters, \
                field='CHI', multiply_rho2=True, cmap= 'Blues',\
                field_func=calculate_chi,\
                field_args=['domain', 'kappa', 'rho'])

    '''
    power_spectrum_v_width(bx, widths, coeffs, domain, z, parameters,\
                field='grad log rho')
    power_spectrum_v_width(cx, widths, coeffs, domain, z, parameters,\
                field='NU*DZLNRHO', field_func=calculate_nu_grad_log_rho,\
                field_args=['domain', 'rho', 'grad log rho'])
    power_spectrum_v_width(dx, widths, coeffs, domain, z, parameters,\
                field='CHI*DZLNRHO', field_func=calculate_chi_grad_log_rho,\
                field_args=['domain', 'kappa', 'rho', 'grad log rho'])
    power_spectrum_v_width(ex, widths, coeffs, domain, z, parameters,\
                field='CHI', field_func=calculate_chi,\
                field_args=['domain', 'kappa', 'rho'])
    power_spectrum_v_width(fx, widths, coeffs, domain, z, parameters,\
                field='DEL CHI', field_func=calculate_grad_chi,\
                field_args=['domain', 'kappa', 'rho', 'dzlnrho'])
    power_spectrum_v_width(gx, widths, coeffs, domain, z, parameters,\
                field='T*CHI', field_func=calculate_chi, cmap='Blues',\
                field_args=['domain', 'kappa', 'rho'], multiply_T=True)
    power_spectrum_v_width(hx, widths, coeffs, domain, z, parameters,\
                field='T*CHI*DZLNRHO', cmap='Blues',\
                field_func = calculate_chi_grad_log_rho,\
                field_args=['domain', 'kappa', 'rho', 'grad log rho'],\
                multiply_T=True)
    '''
    fig.suptitle('S = {0:.6g}, EPS = {1:.4g}, KTOP = {2:.4g}'.\
                format(params['stiffness'], params['epsilon'], \
                params['chi_top'])\
                +', LRZ = {0:.4g}, LTOT = {1:.4g}'.\
                format(params['L_rz'], params['L_tot']),\
                fontsize=16)
    plt.subplots_adjust(top=0.88, wspace=0.5, hspace=0.4)
    fig.savefig(SAVEDIR_BASE+savename+".png", dpi=100)


if __name__ == '__main__':
    #Set up the figure that we'll just recycle. a lot.
    fig = plt.figure(figsize=(20,10))

    #We're interested in widths from 0.01-1Lz and coeffs from 0-99th order
    n_interest = 100
    widths = (1+np.arange(n_interest))/n_interest
    coeffs = np.arange(n_interest)

    #Plot analytical domain
    params = compute_atmosphere_parameters(Rayleigh_top=RA_TOP,\
                Prandtl_top=PR_TOP, gamma=GAMMA, n_rho_cz=N_RHO_CZ,\
                n_rho_rz=N_RHO_RZ, m_rz=M_RZ, stiffness=STIFFNESS)
    Lz = params['L_tot']
    z_basis = de.Chebyshev('z', N_PTS, interval=(0,Lz), dealias=3/2)
    domain = de.Domain([z_basis], grid_dtype=np.float64)
    z = domain.grid(0)
    
    '''
    plot_analytical(domain, z, params, fig)
    plot_numerical(domain, z, params, fig)
    #Let's explore the stiffness parameter space
    N_plots = 3
    explore_stiffness(fig, 2, 4, N_plots, widths, coeffs, \
            field='kappa')
    explore_stiffness(fig, 2, 4, N_plots, widths, coeffs, \
            field='temp')
    explore_stiffness(fig, 2, 4, N_plots, widths, coeffs, \
            field='rho')
    explore_stiffness(fig, 2, 4, N_plots, widths, coeffs, \
            field='grad log rho')
    fig.clear()
    ax = fig.add_subplot(1,2,1)
    bx = fig.add_subplot(1,2,2)

    T = domain.new_field()
    del_T = domain.new_field()
    rho = domain.new_field()
    dzlnrho = domain.new_field()
    kappa = domain.new_field()

    width = 0.1*Lz
    flux_top = -params['chi_top']
    compute_kappa_profile(params, z, kappa, tanh_width=width)
    T = calculate_T(kappa, del_T, T, flux_top)
    rho = calculate_rho(params, z, T, rho)
    dzlnrho = calculate_grad_log_rho(rho, dzlnrho)

    ax.plot(z, T['g'])
    bx.plot(z, T['g']*rho['g']**2)
    ax.set_ylabel('T')
    bx.set_ylabel('T*rho**2')
    plt.show()
     
    #plot_ncc_powers(domain, z, params, widths, coeffs, fig, \
    #savename="NCC_chi_comp")
    '''
