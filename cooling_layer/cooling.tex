\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{Energy Equation -- cooling layer}
\cfoot{\thepage}
\rfoot{June 2, 2015}

\newcommand{\FT}{\ensuremath{\text{FT}}}

\begin{document}
\section{Introducing a cooling layer to the energy equation}

The energy equation, in its full, glorious, non-dedalus form, looks like
\begin{equation}
    C_V\left\{dT + (\gamma-1)T(\nabla\cdot\bm{u})\right\} =
    -\frac{1}{\rho}\left\{\nabla\cdot\bm{Q} + \Pi_{ij}\partial_{x_i}u_j\right\},
\end{equation}
where $dT \equiv \partial_t T + \bm{u}\cdot\nabla T$.  Note that in my
polytropic formulation, $P = \rho T$, my ``temperature'' is actually a
specific temperature with units of $[$energy/mass$]$.  The definition of
specific heat at constant volume,
\begin{equation}
    C_V = \left(\frac{\partial U}{\partial T}\right)_V
\end{equation}
has units of \emph{mass} in this formulation (which, consequently, also means
that entropy has units of mass).  As such, it is clear to see that my energy
equation has overall units of $[$energy/time$]$, which makes sense. I want to
be evolving energy with respect to time using that equation, and that's what
it's doing.  Hooray!

\subsection{ Introducing a cooling term }
I will now modify my energy equation by adding a cooling term to the right-hand
side,
\begin{equation}
    C_V\left\{dT + (\gamma-1)T(\nabla\cdot\bm{u})\right\} =
    -\frac{1}{\rho}\left\{\nabla\cdot(\bm{Q} + \bm{Q}_{cool}) 
    + \Pi_{ij}\partial_{x_i}u_j\right\}
\end{equation}
where $\bm{Q}_{cool}$ is a background cooling flux.  Now I want
something that produces a (somewhat) thick cooling layer at the top of the
domain but essentially \emph{nothing} else throughout the rest of the
domain.  A Gaussian seems like a pretty good starting point.  If I define
$z = 0$ as the bottom of my domain and $z = L_z$ at the top of my domain,
then a Gaussian of the general form
\begin{equation}
    \bm{Q}_{cool} = \bm{A}e^{-(z-L_z)^2/2\sigma^2}
\end{equation}
should work quite nicely.  I will define my cooling layer as having a width of
$W_{cool} = 2\sigma$, such that 95\% of the cooling happens within $W_{cool}$.
Under this formulation, my Gaussian can be recast as
\begin{equation}
    \bm{Q}_{cool} = \bm{A}e^{-2(z-L_z)^2/W^2} \text{ ($W$ = $2\sigma$)}.
\end{equation}
At this point, all that I must do is specify an amplitude and direction. I
assume that the cooling flux goes upwards (or, more importantly, I assume that
$\bm{A}$ is independent of $x$ and $z$) and I further assume that the sign
of $A$ is negative such that this layer actually \emph{cools}.
\begin{equation}
    \boxed{
    Q_{cool} = -|A|e^{-2(z-L_z)^2/W^2}
    },
\end{equation}
where I will have to choose an amplitude which matches both my boundary
conditions and which makes sense with my time steps and buoyancy timescale
(small for small $\epsilon$).

\subsection{Matching Boundary Conditions}
It is important to note that my equation now violates thermal equilibrium,
which is to say that
\begin{equation}
    \nabla\cdot\bm{Q}_{cool} \neq 0.
\end{equation}





\bibliographystyle{apj}
\bibliography{../biblio.bib}
\end{document}
