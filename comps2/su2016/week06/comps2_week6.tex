%\documentclass[letterpaper,12pt]{article}
\documentclass{aastex6}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}
\usepackage{cancel}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{(Su2016) Week 6 -- comps 2}
\cfoot{\thepage}
\rfoot{July Aug. 13, 2016}

\newcommand{\FT}{\ensuremath{\text{FT}}}
\newcommand{\Div}[1]{\ensuremath{\nabla\cdot\bm{#1}}}
\newcommand{\eqs}{\begin{equation}}
\newcommand{\eqe}{\end{equation}}
\newcommand{\angles}[1]{\ensuremath{\left\langle #1 \right\rangle}}
\newcommand{\grad}{\ensuremath{\nabla}}


\begin{document}
\section{Defining a new Nusselt Number}
\subsection{Hurlburt to the rescue}
So, as always, the great \cite{hurlburt&all1984} came to the rescue.  His equation 2.21 defines a Nusselt
number that works for compressible convection:
\begin{equation}
N(z, t) = \frac{F_T - F_A}{F - F_A},
\end{equation}
where $F_A$ is the radiative flux carried by an adiabatic temperature gradient spanning the domain (so this is
the ``final polytrope'' that Ben and made in the lemma), $F$ is the flux that would be carried by a linear temperature
profile between the top and bottom boundary temperatures, and $F_T$ is the sum of all of the total fluxes, neglecting
the potential energy flux, such that
\begin{equation}
F_T = F_{enth} + F_{KE} + F_{visc} + F_{rad}.
\end{equation}

In our system, as in \cite{gilman&glatzmaier1981}, the temperature gradient of the coresponding adiabatic polytrope is
\begin{equation}
(\grad T)_{ad} = -\frac{g}{c_P}\hat{z} = - g \frac{\gamma - 1}{R \gamma}\hat{z}.
\end{equation}
Such that the adiabatic radiative flux takes the form
\begin{equation}
\begin{split}
F_A &= -\kappa(\grad T)_{ad} = \kappa g \frac{\gamma - 1}{R \gamma}
= -\kappa \frac{\gamma - 1}{R\gamma}R(\grad T)_{0}(\frac{\gamma}{\gamma-1} - \epsilon)
= -\kappa(\grad T)_{0}(1 - \epsilon\frac{\gamma - 1}{\gamma}) \\
&= \boxed{ -\kappa(\grad T)_{0} + \epsilon\kappa(\grad T)_{0}\frac{R}{c_P} }
\end{split}
\end{equation}
such that the adiabatic flux contains the background flux of the superadiabatic polytropic reference state
as well as an additional order $\epsilon$ contribution on top of that.

Furthermore, the linear profile connecting the top and bottom boundary conditions has a temperature gradient
of the form
\begin{equation}
\grad T_{\ell} = (T_t - T_b) / L_z = \frac{\Delta T}{L_z}
\end{equation}
where $T_t$ is the top value, $T_b$ is the bottom value, $\Delta T = T_t-T_b$ is the temperature jump across
the domain, and $L_z$ is the height of the domain.  This means that,
in general, the \emph{bottom} of the Nusselt number formula takes the form
\begin{equation}
F - F_A = \kappa\left( -\frac{\Delta T}{L_z} + (\grad T)_0 - \epsilon (\grad T)_0 \frac{R}{c_P}\right),
\end{equation}
and for the specific case of \emph{constant temperature boundary conditions}, we have 
$\Delta T / L_z = (\grad T)_0$ for all times, such that
\begin{equation}
F - F_A = -\epsilon\frac{R}{c_P}\kappa(\grad T)_0.
\end{equation}

The numerator of the Nusselt number is a bit harder, as it contains all of the other fluxes.  The total flux
term looks about like (in the z direction)
\begin{equation}
F_T = w\left(\rho\left[\frac{1}{2}|\bm{u}|^2 + (C_v + R) T + gz \right] + 
\frac{\partial}{\partial x}\left[2 - \frac{2}{3}u\right] + 
\frac{\partial}{\partial z}\left[u + \frac{4}{3} w\right]   \right) 
- \kappa (\grad T)_0 - \kappa (\grad T)_1
\end{equation}
which is disgusting.  But an important note to make here is that we have a bunch of roughly order $\epsilon$
quantities and one big $\kappa (\grad T)_0$, but when we hit this in the numerator with a $-F_A$, the background
radiative fluxes get knocked out.  What we're left with is:
\begin{equation}
\boxed{
\text{N}(z,t) = \frac{F_{enth} + F_{KE} + F_{visc} - \kappa (\grad T)_1 -\epsilon \kappa \frac{R}{c_P}(\grad T)_0}
{\kappa\left( -\frac{\Delta T}{L_z} + (\grad T)_0 - \epsilon (\grad T)_0 \frac{R}{c_P}\right)}},
\end{equation}
or in the case of \textbf{constant $T$ boundary conditions},
\begin{equation}
\text{N}(z,t) = \boxed{\frac{F_{enth} + F_{KE} + F_{visc} - \kappa (\grad T)_1 -\epsilon \kappa \frac{R}{c_P}(\grad T)_0}
{\kappa\left( -\epsilon\frac{R}{c_P}\kappa(\grad T)_0\right)}},
\end{equation}
So this says that we have roughly and order $\epsilon$ quantity on top of an order $\kappa\epsilon$ type quantity.
Or, in other words, we roughly have $N \propto \kappa^{-1} \propto Ra^{1/2}$.  So if we were in the ultimate
regime of convection, we'd have a Nusselt number scaling of $Ra^{1/2}$, which is the same as RB convection.
Looks like we have a useful Nusselt number to play around with!



\subsection{My older attempt}
OK, so there's been a lot of pretty horrible work done on this, and we now need to define a new, useful
Nusselt number that shows that low $\epsilon$ is actual useful, regardless of what \cite{brandenburg&all2005}
might say about small $\epsilon$.

We start by making a pretty big assumption: \emph{our final atmosphere is a polytrope}.  That means that polytropic
relations between $\rho$, $T$, etc. still hold and our atmosphere is in hydrostatic equilibrium.  We make another assumption,
which is to say that \emph{our final atmosphere is adiabatic}, such that its polytropic index is $m = m_{ad}$. What
this means is that our final atmosphere has the form
\begin{equation}
\rho_f = \frac{\rho_i}{T_i^{m_f}} T_f^{m_f}
\end{equation}
Differentiating, we see that $\grad\ln\rho_f = m_f (\grad T_f) / T_f$, which is just...true for polytropes.  
That's what they are.  This will be true for any constant $\grad T_f$. Anyways,
hydrostatic equilibrium for an EOS of $P = R\rho T$ gives us
\begin{equation}
\grad T_f + T_f \grad\ln\rho_F = \grad T_f(1 + m_f) = -\frac{g}{R},
\end{equation}
and this equation is true of the initial polytrope, too.  Rearranging, we can see that the initial polytrope gives us
\begin{equation}
-g = R\grad T_0 (m_0 + 1),
\end{equation}
and we never change gravity, so this is true in the end.  I also just want to quickly note that, for the final state,
$\nabla T_f = - g / c_P$.  Solving for the final $T$ profile, we see that
\begin{equation}
\grad T_f = -\frac{g}{R(m_f + 1)} = -\grad T_0 \frac{R  (m_{0} + 1)}{R (m_f + 1)}
\end{equation}
For $m_0 = m_{ad} - \epsilon$ and $m_f = m_{ad}$, this takes the form
\begin{equation}
\grad T_f = \grad T_0 \frac{m_{ad} - \epsilon + 1}{m_{ad} + 1} = \grad T_0\left(1 - \frac{\epsilon}{m_{ad} + 1}\right).
\end{equation}

Such a state would have a radiative flux of
\begin{equation}
-\kappa\grad T_f = -\kappa \grad T_0\left(1 - \frac{\epsilon}{m_{ad} + 1}\right),
\end{equation}
where the ``free flux'' in the system is described as the part of the flux that isn't carried by our initial conditions,
\begin{equation}
F_{\text{free}} = F_{\text{rad, tot}} - F_{\text{rad, init}} = -\kappa (\grad T_f - \grad T_0) = \kappa\grad T_0\frac{\epsilon}{m_{ad}+1}
\end{equation}
So we expect that our system will adjust such that it can carry some amount of flux of order $\epsilon$ and of order our initial
temperature gradient.  OK.  That's the total flux that we have to carry--ish.  So what we're INTERESTED in is how much flux we're
carrying OVERALL by radiation, as a DEFICIT from this value.  So if our radiative flux is at this adiabatic value, it's pretty much
lame and doing nothing.  If our radiative flux is non-adiabatic, then it's doing something.  This is basically an analog for the
``grad T is zero'' of the RB convection that Tayler is dealing with, basically.

Anyways, this means that the radiative flux we're INTERESTED In is the part of radiative flux that exists as a deviation from adiabatic,
\begin{equation}
F_{\text{rad, dev}} = F_{\text{rad, ad}} - F_{\text{rad, tot}} = 
-\kappa \grad T_0\left(1 - \frac{\epsilon}{m_{ad}+1}\right) + \kappa \grad(T_0 + T_1).
\end{equation}
We're basically interested in the plane-averaged and volume-averaged version of this.  Then, we can define an \emph{compressible Nusselt number}
where
\begin{equation}
\boxed{
\text{Nu}_c = \frac{F_{\text{rad, dev}} + F_{\text{conv}}}{F_{\text{rad, dev}}} = 
1 +  \frac{F_{\text{enth}} + F_{\text{KE}}}{F_{\text{rad, dev}}}},
\end{equation}
where $F_{\text{enth}}$ is the enthalpy flux and $F_{\text{KE}}$ is the kinetic energy flux.

\section{A quick note on epsilon}
This really isn't hugely important, but I think it's interesting.  Recall that for our polytrope,
\begin{equation}
\grad S = -\epsilon (z_0 - z)^{-1},
\end{equation}
and at the top of the domain, $z_0 - z = 1$, such that
\begin{equation}
(\grad S)_{top} = -\epsilon.
\end{equation}
Actually, for any arbitrary $m = m_{ad} + \Delta$, we would have $(\grad S)_{top} = \Delta$.  So while it's
true that $\epsilon$ is our control parameter for adiabaticity, it's also more precisely the value that we peg
the entropy gradient (not the specific entropy gradient, just the entropy gradient) at the top of the domain.

I realized this while reading \cite{gilman&glatzmaier1981}, where they talk about low $\epsilon$ and even mention
that $\epsilon \propto \text{Ma}^2$.  I need to look at their arguments for that, but it seemed a bit weak (maybe
they refer to older papers though?).


\bibliographystyle{apj}
\bibliography{../../../biblio.bib}
\end{document}
