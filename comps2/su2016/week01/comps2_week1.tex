\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{(Su2016) Week 1 -- comps 2}
\cfoot{\thepage}
\rfoot{June 18, 2016}

\newcommand{\FT}{\ensuremath{\text{FT}}}

\begin{document}

\section{Preface to Summer notes/writeup}
So this is really the same thing as I was doing over the semester.  I realize that June 18 is
definitely not the end of ``week 1'' of the summer, but it's the end of week 1 of when I started
taking notes again (I fell off the wagon).  Once again, this is so that I can organize my
thoughts and stitch together my comps 2 based off of weekly updates when I actually know what I'm
doing (as opposed to scatter-brained stuff later down the road trying to look back on what I've
done).

\section{The Meat of Comps II}
For my comps II project, I'm going to study boundary layer structure over polytropic convection.
This is the same work as I did on my SPD 2016 poster, but I will further that work and make it
more complete.  Also, I'm aiming for a Rayleigh Number span at low/moderate Mach number, rather
than a Mach number span at low/moderate Rayleigh number.  At least, that's the plan for now.

My hunch is that somewhere between about $10^4R_c$ and $10^9R_c$, where $R_c$ is the critical
Rayleigh number where convection sets on, there's a transition from a laminar regime to a truly
turbulent regime.  This is the ``ultimate regime'' that Rayleigh-Benard papers refer to, and it's
definitely not a regime that we've achieved in most of our global simulations.

\subsection{The flagship runs}
I'm going to do two ``flagship'' runs for my comps 2: Two polytropic convection simulations
which span 5 density scale heights and which do a span of Rayleigh number from about $10^3 R_c$
to about $10^9 R_c$.  I'll do the first, low Ra run for a LONG ASS time.  It'll be my convergence
run.  It's where I'll let the atmosphere settle.

THEN I'll step up the Rayleigh number by about a factor of $10^{1/4}$, run for about 40-50
buoyancy times, rinse, repeat.  This means that each span will actually be about 
$4x6(+1?)=25$ish short runs (with one of those runs being a long run to atmospheric sagging
convergence).

I'll measure top \& bottom thermodynamic (entropy) and velocity boundary layers, I'll plot
them vs. Ra. I'm expecting to see two slopes, one in the laminar regime and a separate one in
the turbulent regime.  But we'll find out!  I'll start the runs soon (this week or next).

\section{Ways to win?}
Starting from HS equilibrium and running through a transient is rough.  It takes a long time for
the atmosphere to actually fully converge.  So what I've been doing in the past is running
for a long time at low Ra, stepping up the Ra, and running for a short amount of time until the
solution ``relaxes'' to the new Ra final profile.  This probably works well, but I should verify
it.  I'll converge another run at higher Ra than I start at and compare its final solution to
the solution that I've bootstrapped up.

I might also be able to win with eigenvectors.  If I do an EVP solve on my atmosphere and use that
$T$ and $\rho$ in my initial conditions, then maybe I'll save some time on convergence runs.  It's
worth a shot.  I haven't really been successful with this so far, but I haven't tried hard to make it work.

\section{Convective Onset}
I've been using Jeff's \texttt{eigentools} to create a better onset solver than I had last summer,
and it seems to be working well!  Essentially I'm doing an adaptive mesh grid on
wavenumber-Ra space.  I do a coarse grid, find the min, bracket around it and increase resolution,
find the min, etc.  I've got some decent results (see Fig. \ref{fig:onset}), but there are still
a few bugs.  Regardless, this gives me a place to start my Ra sweep for both constant $\mu$ and 
constant $\nu$.

\begin{figure}[ht!]
\includegraphics[width=0.8\textwidth]{figs/onset_bothMus_nrho5_eps1e-4.png}
\caption{Convective onset for Mu = constant and Nu = constant for $\epsilon=10^{-4}$, $n_\rho=5$,
Pr=1.  From IVP problems, I've verified that onset has happened by about a Ra of
$0.3$ and $25$ for the two curves, respectively (a little above the min found, but this is
from an infinitesimal kick). \label{fig:onset}}.
\end{figure}

\section{Conditioning a polytrope over an adiabatic background}

If we look at the equations describing a polytrope:
\begin{equation}
\begin{split}
T_0 &= T_{00}(z_0 - z) \\
\rho_0 &= \rho_{00}(z_0 - z)^m \\
P_0 &= P_{00}(z_0 - z)^{m+1}.
\end{split}
\end{equation}
And the subsequent equations that we use to determine the atmosphere of a polytrope with an
adiabatic index $m = m_{ad} - \epsilon$:
\begin{equation}
\begin{split}
L(n_\rho, \epsilon) &= \text{exp}\left(\frac{n_\rho}{m_{ad}-\epsilon}\right) - 1\\
\frac{\Delta S}{C_P}(n_\rho, \epsilon) &= -\left(\frac{n_\rho}{m_{ad}-\epsilon}\right) \
\left(\frac{m_{ad}(1-\gamma) + 1}{\gamma} + \epsilon\frac{\gamma-1}{\gamma}\right)\\
g(\epsilon) &= \frac{k_B}{\mu}T_{00}(m_{ad} + 1 - \epsilon) \\
\kappa(\text{Pr, Ra}, n_\rho, \epsilon) &= \sqrt{\frac{g L^3 |\Delta S / C_P|}{\text{Pr Ra}}} \\
\nu(\text{Pr, Ra}, n_\rho, \epsilon) &= \sqrt{\frac{\text{Pr}}{\text{Ra}} gL^3 |\Delta S / C_P|}
\end{split}
\end{equation}
we can see a couple things:
\begin{enumerate}
\item The temperature profile is completely independent of adiabaticity.
\item Every other parameter is dependent on epsilon.
\end{enumerate}

For visualization purposes, it's beneficial for us to run on an atmosphere where the
background $T_0$, $\rho_0$, and $P_0$ are adiabatic.  Luckily, if we set up our
atmosphere appropriately (get the proper $L$, $g$, etc for a given epsilon), then
all we really have to do is change $\ln \rho_1$ in order to get our atmosphere from
adiabatic to superadiabatic.  So....that's the goal.  I just want to note a couple
of implications of this:
\begin{enumerate}
\item Variable $\nu$ or $\chi$ will no longer be divided by $\rho_0$, they'll be
divided by a different $\rho_0$ that describes the superadiabatic solution.
This has the not-so-nice affect of getting rid of the ``easy rho'' terms in
the momentum and temperature equation for non-constant $\nu$ and $\chi$.
\item Setting $g$ based on an $\epsilon$ that doesn't describe $\rho_0$
means that our background state is no longer in HS equilibrium.  We need
to put these terms back in the momentum equation.
\end{enumerate}
But beyond that, we really don't need to do much.  If we have a background described by:
\begin{equation}
\begin{split}
T_0 &= (z_0 - z) \\
\rho_0 &= (z_0 - z)^{m_{ad}}
\end{split}
\end{equation}
then by setting $\ln \rho_1 = -\epsilon \ln (T_0)$ we should retrieve the initial conditions we want.
Proof: $\ln \rho = \ln \rho_0 + \ln \rho_1 = m_{ad}\ln(T_0) - \epsilon \ln(T_0) = (m_{ad} - \epsilon)\ln(T_0)$,
or $\rho = T_0^{m_{ad}-\epsilon}$, which is what we want!  Whoo.  Ok, so this just needs to be
implemented intelligently.

\subsection{Update: take 2, adiabatic deviation via post-processing.}
After talking with Ben for a while, I'm realizing that the best way to do this is NOT by having the actual
background be adiabatic.  That actually harms us insofar as it makes our system of equations harder to solve
(it adds in the HS equilibrium term to the equations and it gets rid of the easy rho terms).  So
rather than setting the background as adiabatic, I'll just change the s\_fluc being printed out to file
by subtracting off the necessary profile such that I'm only showing deviations from adiabatic.

Basically, this is pretty simple.  Our current implementation of s\_fluc is:
\begin{equation}
\text{s\_fluc} = C_V \ln (1 + T1/T0) - C_V(\gamma - 1) \ln \rho_1.
\end{equation}
As I showed above, the T0 profile we have is adiabatic regardless of epsilon, so clearly the first term in this
equation need not be changed.  Also, $C_V * (\gamma - 1) = 1$, so that pre-factor on the second term drops out.
We know that $\rho_0$ has the form T0$^{m}$ and thus, if the full $\rho$ profile were adiabatic, we would know that
$\rho_1 = \rho_{f}/\rho_0 = T_0^{m_{ad}}/T_0^{m_{poly}} = T_0^{\epsilon}$, or $\ln\rho_1 = \epsilon \ln T_0$.  Thus,
if we take the above equation and set $\ln \rho_{1-mod} = \ln \rho_1 - \epsilon \ln T_0$, then what we'll have left is
deviations from adiabatic.  This just means that the entropy perturbations that we want to plot out are simply
\begin{equation}
\text{s\_fluc\_from\_adiabatic} = \text{s\_fluc} + \epsilon\ln T_0,
\end{equation}
but since $\epsilon$ is not a fundamental parameter of our FC\_equations and we want to keep things simple, we have
to cast this in terms of $\rho_0$ and $T_0$, so really we have:
\begin{equation}
\text{s\_fluc\_from\_adiabatic} = \text{s\_fluc} + \ln\left(\frac{T_0^{1/(\gamma-1)}}{\rho_0}  \right).
\end{equation}

This should give us a nice measure of what convection actually looks like.  Also, unlike the basic entropy
fluctuations off of a polytrope, this one should be \emph{zero} on average once we're converged, which
should make it easier to measure the BLs, or at least make it clearer where they are.

\bibliographystyle{apj}
\bibliography{../../biblio.bib}
\end{document}
