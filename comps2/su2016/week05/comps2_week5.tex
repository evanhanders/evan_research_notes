\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}
\usepackage{cancel}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{(Su2016) Week 5 -- comps 2}
\cfoot{\thepage}
\rfoot{July 30, 2016}

\newcommand{\FT}{\ensuremath{\text{FT}}}
\newcommand{\Div}[1]{\ensuremath{\nabla\cdot\bm{#1}}}
\newcommand{\eqs}{\begin{equation}}
\newcommand{\eqe}{\end{equation}}
\newcommand{\angles}[1]{\ensuremath{\left\langle #1 \right\rangle}}


\begin{document}
\section{Summer solar meeting (and SALT) talk notes}
OK, I need to get my thoughts together for SALT and the summer solar meeting, and I need to write up equations and do
all sorts of stuff like that.  I think this is a good way to make myself do that.  So...time to type away!

\subsection{Motivation}
The outer 30\% by radius of the Sun is a convective zone.  This convective zone has a lot of complexities (spherical
geometry, differential rotation, many density/pressure scale heights).  Furthermore there are many complex flows
in this zone.  There are zonal flows which change over the course of a solar cycle and which sunspot motions track
fairly well -- these introduce both vertical and horizontal shears in convection (\cite{christensen-dalsgaard2002},
\cite{hanasoge&all2015}).  Furthermore, there are meridional flows within the solar convection zone.  These flows carry
plasma from the solar equator towards the poles and vice versa.  It's unclear whether there is a single meridional cell
or whether there are two cells, but either way this will introduce a shear flow in the opposite direction of
convective motion (\cite{hanasoge&all2015}, \cite{zhao&all2013}).  As such, it's important to understand the effects
that shear flows have on stratified convective motions.  Most previous experiments in stratified convection have been
in complex laboratories with differential rotation, spherical geometry, and many other ``muddying'' effects.  Here
we study shear flows that arise in 2D compressible convection in the context of polytropic stratification.  We study
spontaneously formed shear flows and also the parameter space in which shear flows can be imposed and maintained.

\subsection{Equations}
We solve the fully compressible Navier Stokes Equations with an ideal gas equation of state:
\begin{equation}
\begin{split}
\frac{\partial \rho}{\partial t} + \nabla\cdot(\rho\bm{u}) &= 0 \\
\rho\left(\frac{\partial \bm{u}}{\partial t} + \bm{u}\cdot\nabla\bm{u}\right) &= \nabla P + \rho \bm{g} - \nabla\cdot\bar{\bar{\bm{\Pi}}} \\
\rho c_V\left( \frac{\partial T}{\partial t} + \bm{u}\cdot \nabla T \right)
&= -\rho c_V(\gamma-1) T \Div{u} + \nabla\cdot(\kappa\nabla T) - (\bar{\bar{\bm{\Pi}}}\cdot\nabla)\cdot\bm{u} \\
P &= \rho T \\
\Pi_{ij} &= -\mu\left(\frac{\partial u_j}{\partial x_i} + \frac{\partial u_i}{\partial x_j} - \frac{2}{3}\delta_{ij}\Div{u}\right)
\end{split}
\end{equation}

But that's a bit of a ridiculous number of variables to solve over, so we actually plug in our EOS, do some re-arranging, and arrive
at the following system of equations that we evolve in time:
\begin{equation}
\begin{split}
\frac{D \ln\rho}{Dt} &= - \Div{u} \\
\frac{D \bm{u}}{Dt} &= -T\nabla\ln\rho - \nabla T + \bm{g} - \frac{1}{\rho}\nabla\cdot{\bar{\bar{\bm{\Pi}}}} \\
\frac{D T}{Dt} &= -(\gamma - 1) T\Div{u} + \frac{1}{\rho c_V}\left[\nabla\cdot(\kappa\nabla T) - (\bar{\bar{\bm{\Pi}}}\cdot\nabla)\cdot\bm{u}\right]
\end{split}
\end{equation}
Which means we actually only evolve four variables with respect to time: $(\bm{u}, T, \ln\rho)$.


\subsection{Reference State: Polytrope}
We use initial conditions of a polytrope, which has the form
\begin{equation}
\begin{split}
P &= P_{00} (z_0 - z)^{m+1} \\
\rho &= \rho_{00} (z_0 - z)^{m} \\
T &= T_{00} (z_0 - z) \\
z_0 &= L_{cz} + 1 \\
P_{00} &= \rho_{00} = T_{00} = 1
\end{split}
\end{equation}
where we define
\begin{equation}
m = \frac{1}{\gamma-1} - \epsilon = m_{ad} - \epsilon = 
\begin{cases}
\geq m_{ad} & \text{stable} \\
> m_{ad}    & \text{unstable}
\end{cases}
\end{equation}
So for positive $\epsilon$, we have an unstable atmosphere which will convect.  Note that by 'unstable' here we mean
that there's a negative entropy gradient.  This means that things which are cool will buoyantly sink and things
which are hot will buoyantly rise.  In order to quantify this, we turn to the definition of the entropy gradient,
which comes from the first law of thermodynamics and our choice of an ideal gas equation of state:
\begin{equation}
\frac{\nabla S}{c_P} = \frac{1}{\gamma} \nabla\ln P - \nabla \ln \rho = \frac{1}{\gamma}\nabla\ln T + \frac{1-\gamma}{\gamma}\nabla\ln\rho
\end{equation}

I also want to note that the spirit of the polytrope is this: it's a system in hydrostatic equilibrium (such that gravity is set by the
polytropic index), and it's a system which has
no flux divergence.  Since all fluxes other than the radiative flux depend on $\bm{u}$, this means that
\begin{equation}
\nabla\cdot(\kappa\nabla T) = 0,
\end{equation}
and so it's customary to set $\kappa = const$ and $\nabla T = const$ to mamke this happen.

\subsection{Control Parameters}
We use four control parameters total: $(n_\rho, \epsilon, \text{Ra}, \text{Pr})$.  $n_\rho$ is the number of
density scale heights that the polytropic reference state spans.  $\epsilon$ is the level of superadiabaticity,
and we define the Rayleigh number and the Prandtl number as
\begin{equation}
\begin{split}
\text{Ra} &= \frac{g L^3 (\Delta S/c_P)}{\chi\nu} \\
\text{Pr} &= \frac{\nu}{\chi}
\end{split}
\end{equation}
where the Rayleigh number is the ratio of buoyant driving to diffusivity and the Prandtl number is the ratio of
viscous damping to thermal damping. Sometimes the Rayleigh number is defined with an $L^4 d_z S / c_P$, but we
choose to use the entropy jump across the domain rather than the derivative at the midplane. It's probably important
to mention here that $\nu = \mu/\rho$ and $\chi = \kappa/\rho$.

Oh, and I guess the aspect ratio is sort of a control parameter too, huh.

\subsection{Stratified convection basics}
We start off our system in hydrostatic equilibrium such that the pressure field perfectly balances gravitational forces.
Even though we have an unstable ($\epsilon > 0$) polytrope, this equilibrium point is stable and if you leave it alone
it won't convect.  Thus, we introduce a small-amplitude kick (noise on the order of $10^{-6}$ times the refernce
state's temperature field), and these small perturbations grow into convective instabilities.  We call this the ``transient,''
and it's obvious in movies, system energy traces, etc.  After the transient, the system settles into a steady state of
convection which has \emph{very} slow time evolution of the density and temperature profiles.  In the interior, the
entropy gradient is actually mostly isentropic when you take horizontal averages and time-average over those once
the system starts convecting.  

Some basic features of downflows and upflows are very different from those in rayleigh-benard convection.  Upflows and
downflows no longer appear symmetrical but rather have very different forms.  Downflows are cool and have very high
velocity and shrink as they travel down into the higher density lower portions of the atmosphere.  Upflows are actually
hard to see, as they start off at a very high density and are therefore very small in the lower part of the atmosphere.
As they buoyantly rise they expand and are most obviously seen in large sweeping motions near the surface.  At moderate
Rayleigh number, such as around $10^6$ or higher, diffusivities are low enough that cold parcels from the downflows can
actually be swept upwards by the powerful upflows before fully diffusing -- which shows that at higher Rayleigh number
we're not only ``more turbulent ooo'' but also we're transporting heat in a very different way.

\subsection{Spontaneous shear flows}
We first saw shear flows at a Ra of $10^{5.75}$, $\epsilon=0.5$, $n_\rho=3$.  They happened because one convective roll
basically squashed the other, and the downflow passed over the upflow, and then...well, boom.  The interesting thing is
that these happened at an aspect ratio of 4, rather than previous work (e.g. \cite{Hurlburt1984}) which saw it around
AR of 0.5, and a little around 3, and Tayler's work which is more around AR2.  My hunch is that this might be due to the fact
that even though we're at a high-ish aspect ratio, we're not necessarily capturing all of the driving scales of the system in our box.
The results of \cite{Lord2014} show that the driving scale of convection is 4$H_\rho$ (locally).  Thus, the largest local driving scale
is at the bottom of the domain and it's roughly
\begin{equation}
L_{drive} = 4H_{\rho, \text{bot}} = \frac{4z_0}{m} = \frac{4(L_{cz} + 1)}{m_{ad}-\epsilon}.
\end{equation}
At least, that's what it is in the hydrostatic reference state.  As $m$ approaches $m_{ad} = 1.5$, the deviations from the reference state are
small, so we can pretty much say that the evolved state is the reference state.  However, at higher epsilon (e.g. 0.5), 
we have a couple problems.
First, $m = 1$, so the REFERENCE driving scale is $4 *(L_{cz} + 1)$, which means we need...a bigger box.  
And since the atmosphere sags downwards, we proably
\emph{really} need a bigger box.  I've also seen spontaneous shear at $\epsilon = 0.3$, which has an OK driving scale 
at reference, but whose driving scale may grow greater than $4L_{cz}$ once the system is evolved.  
All of my runs below $\epsilon=0.3$ failed to spontaneously shear, and I think this might be why.

Anyways, the kinetic energy of the whole system pretty much stays constant when it transitions from a non-shear state to a shear state, it just
converts from being in time-dependent structures to being in large-scale time-dependent flows which the convection feeds and sustains.  

\subsection{Basics of imposing a shear}
Ok, so here's some things that we know about imposing a shear:

\begin{enumerate}
\item We want the shear to be a net-zero momentum shear.  This means that
\begin{equation}
\int_0^{L_{cz}} \rho(z) u(z) dz = 0,
\end{equation}
which is actually shockingly difficult to achieve.  The fact that $\rho(z)$ is basically $z^m$ where $m$ is a strange
decimal number makes things hard.
\item it's hard to know what other properties of the shear are important.  For example, how important is the center of
shear?  What should be the magnitude of the shear?  What is a good measure for the magnitude of the shear?  These
questions are still a bit unknown...
\end{enumerate}

\subsection{Imposed shear flow 1: cosines to COM}
Even though we can't spontaneously get shear at high aspect ratio and low epsilon, we're pretty sure it's a solution that's available to those
parameters, so we want to impose it.  Keep in mind that fourier domains are \emph{infinite} in extent, so if a low
aspect ratio domain can achieve a shear, then it's technically shearing in an infinitely big domain.  That means that
even if we have a higher aspect ratio, we're only tiling a bigger chunk of space inifinitely -- the shear flow shouldn't
necessarily care much about that if its extent is infinite.

We inject a flow of the form:
\begin{equation}
u = \begin{cases}
u_t \cos\left(\frac{\pi}{2(L_{cz}-L_c)}[L_{cz}-z]\right) & z > L_c \\
u_b \cos\left(\frac{\pi}{2L_c}z\right)                   & z < L_c
\end{cases},
\end{equation}
where 
\begin{equation}
L_c = \frac{1}{(m+1)(z_0^{m+1}-1)}\left(z_0^{m+2} - L_{cz}[m+2] - 1\right)
\end{equation}
is the center of mass of the polytropic atmosphere.  We specify the flow velocity at the top of the atmosphere
($u_t$), and then we numerically solve for the flow at the base of the atmosphere:
\begin{equation}
u_b = -u_t\frac{\int_{L_c}^{L_{cz}}(z_0 - z)^m\cos\left(\frac{\pi}{2(L_{cz}-L_c)}[L_{cz}-z]\right) dz}
{\int_{0}^{L_c}(z_0-z)^{m}\cos\left(\frac{\pi}{2L_c}z\right) dz},
\end{equation}
where this integral comes from imposing that the flow has a net momentum of zero throughout the atmosphere.

It's hard to choose the right value of $u_t$, but it seems to me that the correct diagnostic to use is the 
average kinetic energy of the flow.  We often times use the average values of the system energies as diagnostics when
running convective simulations.  After the convective transient, the kinetic energy reaches a roughly constant value which
slowly declines over time.  When the shear flow starts spontaneously, it essentially ``eats up'' all of this available
kinetic energy in the system.  It doesn't inject a lot more kinetic energy, it simply steals that kinetic energy from
upflows and downflows such that the energy moves from being a temporally changing phenomenon to being a time-independent
phenomenon.   So my hunch is that if we inject the post-transient kinetic energy of the system in the initial shear flow,
then it should just ``hang out''.  The convection should work to feed and maintain it, and it won't have an inappropriately
large amount of kinetic energy that it has to slow down and get rid of it.  I want to note here that if we put too low
of a shear flow velocity into the system, the convective transient squashes the shear quickly and the system reverts
to the standard ``rolls'' solution.

The kinetic energy of the shear goes like the square of $u_t$ specified, but its specific value depends on the
atmosphere's parameters ($n_\rho, \epsilon$), which means that I really need to do some basic convective sims to a
little past transient, see how much kinetic energy those systems gain over the course of the overturn, figure out what
initial shear flow value ($u_t$) corresponds to the post-transient KE, and then use that value of $u_t$ to try to
spin up the shear.

Another problem here is that it's possible that this flow violates the Richardson criter for stability in
stratified shear flows \cite{miles1986}:
\begin{equation}
\text{R} \equiv - g \frac{d(s/c_P)}{dz}\left(\frac{dU}{dz}\right)^{-2} > \text{R}_{\text{crit}} \rightarrow \,\,\text{stability}
\end{equation}
so when R gets too small, we have instabilities arise from the shear flow, and that's not really the goal. The
problem is that our derivative isn't smooth at the match point, here.  And that means that we have sadness, because
un-smooth derivatives are \emph{large} and that means R is \emph{small}$^2$.  So we're almost certainly going to have
something like a Kelvin-Helmholtz instability to kick things off.

\subsection{Imposed shear 2: Linear profile}
Ben suggested the obvious: a linear shear profile.  This, in theory, works out pretty well but it also has one problem:
it doesn't fit with the community's generally accepted stress-free boundary conditions of $d_z u = 0$.  It requires
$d_z^2 u = 0$, which has...some not great effects on shearing in RB convection, and unknown consequences in FC
convection.  It's also not clear that this is correct and truly stress free.

Regardless, a shear of this form, where we specify the velocity at the top of the domain, $u_t$, simply takes the form
\begin{equation}
u = \frac{u_t}{1 - \frac{L_{cz}}{L_c}}\left(1 - \frac{z}{L_c}\right),
\end{equation}
where $L_c$ is the center of mass of the atmosphere.  It's a super nice profile, and it has an average kinetic energy
which takes the form:
\begin{equation}
\begin{split}
\angles{\text{KE}}_{\text{vol}} &= C\frac{z_0^{m+1}}{m+1}\left(1 - \frac{2 z_0}{L_c (m+2)} + \frac{2 z_0^2}{L_c^2(m+2)(m+3)}\right)
\\        &+ \frac{C}{m+1}\left( -1 + \frac{2}{L_c}\left[L_{cz} + \frac{1}{m+2}\right] - \frac{L_{cz}^2m(m+1) + 2(m+1+z_0^2)}{L_c^2(m+2)(m+3)}\right),
\end{split}
\end{equation}
where
\begin{equation}
C \equiv u_t^2 \frac{1}{2L_{cz}\left(1 - \frac{L_{cz}}{L_c}\right)}
\end{equation}

Once again, this is a really nice, mathematically easy to manipulate formula, but it's unclear if it's actually useful.

\subsection{Imposed shear 3: Centered Cosine}
This is Axel's suggestion.  It's also quite easy.  It's also very unclear how useful it is.  Here, we impose a shear
of the form
\begin{equation}
u(z) = u_t \frac{\cos(kz)}{\rho(z)},
\end{equation}
where
\begin{equation}
k \equiv \frac{n \pi}{z} \,\,\,\,\,\, n = (1, 2, 3, \cdots),
\end{equation}
and where $n$ can be half-integer values if you subtract off an appropriate constant.  This is great because of the fact
that it doesn't violate the classic no-stress boundary conditions.  It's not great because the top and bottom values of
velocity are roughly an order of magnitude apart for an appreciably stratified domain due to the lowered location of
the center of mass.  This profile essentially looks...very similar to $1/\rho$.  But it does cross through zero and it
does have a net zero momentum.  It'd be worth seeing if any of these are....well, great.


\subsection{Imposed shear 4: Tanh profile}
This shear takes the form
\begin{equation}
u(z) = A(\text{tanh}(w[z - L_{cs}]) + C),
\end{equation}
where $w$ is the tanh ``width'' (currently set to $L_{cz}/10$) and $L_{cs}$ is the center of the shear,
which I'm currently setting to
\begin{equation}
L_{cs} = \frac{L_c + L_{cc}}{2},
\end{equation}
where $L_c$ is the atmospheric center of mass (center of shear for a linear profile), and $L_{cc}$
is the center of shear for a step-function shear (where the shear velocity goes from $u_s$ to $-u_s$ at
$L_{cc}$ in order to have a zero momentum flow).  I choose this because I know that the spontaneous center
of shear appears below the center of mass, and because a wide tanh is roughly like a partial linear flow
and a partial step function flow.  I determine $C$ by numerically integrating over the momentum profile of
the tanh, and then integrating over a constant velocity momentum profile in order to see what value of
a constant velocity offset from the tanh would bring the tanh to zero momentum.  Then, after adding this in,
I determine $A$ by setting the $z = L_{cz}$ value of the whole function equal to the specified top velocity,
$u_t$.

This has a couple nice properties:
\begin{enumerate}
\item It's easy for us to specify the center of circulation and put it deep in the atmosphere.
\item It follows the no-stress boundary conditions quite well.
\item By adjusting the width, we can ensure that avoid the Richardson criterion.
\item The top and bottom values of shear are fairly comparable, which is something I'm seeing in
spontaneous shears.
\end{enumerate}



\section{Math with the Boussinesq Equations}
\subsection{Non-dimensionalization and the Rayleigh Number}
The dimensional Boussinesq equations are:
\begin{equation}
\begin{split}
&\Div{u} = 0 \\
&\rho\frac{D\bm{u}}{Dt} = -\nabla P + \rho\nu\nabla^2 \bm{u} + \rho\bm{g} \\
&\frac{D T}{D t} = \kappa\nabla^2 T,
\end{split}
\end{equation}
where
$$
\frac{Df}{Dt} = \frac{\partial f}{\partial t} + \bm{u}\cdot\nabla f,
$$
and we use the Boussinesq approximation, which is to say that $\rho = \rho_0$ on all terms in the equations except
for on the buoyancy term where $\rho \equiv \rho_0(1 - \alpha[T - T_0])$, and I will define $T_1 \equiv T - T_0$.

\subsubsection{The continuity equation}
Is already dimensionless even when it's dimensional, which is to say that we have no work to do here. Yay!

\subsubsection{The energy equation}
Before we get started, let's start moving our variables from dimensional to dimensionless:
\begin{equation}
\begin{split}
t = \tau t^* \\
\nabla = \frac{1}{\ell}\nabla^* \\
T = (\Delta T) T^*
\end{split}
\end{equation}
where the $*$ terms are the dimensionless variables and the things in front of the ($\tau$, $\ell$, and $\Delta T$)
are the characteristic values of time, length, and temperature in our system.  With these non-dimensionalizations, our
equation becomes:
$$
\frac{\Delta T}{\tau}\frac{D T^*}{D t^*} = \frac{\kappa \Delta T}{\ell^2}\nabla^{*2} T^*,
$$
Dividing both sides by $\Delta T / \tau$, I will now define the characteristic time scale of the system as the
characteristic thermal diffusion timescale, $\tau = \ell^2/\kappa$.  With this substitution, everything cancels out,
and we have the non-dimensional temperature equation (where now I'm going to drop the $*$'s on all of the dimensionless
terms for ease):
\begin{equation}
\frac{D T}{D t} = \nabla^2 T
\end{equation}

\subsubsection{The momentum equation}
Now that we've defined a few non-dimensional fun times, let's also define
\begin{equation}
u = u_0 u^* = \frac{\ell}{\tau}u^*
\end{equation}
And now we're going to divide the whole equation by $\rho_0$ anddefine POMEGA 
because it's awesome and it simplifies our lives (and it wraps up the constant part of
the gravity term so we can just examine the buoyancy term):
\begin{equation}
\varpi \equiv \frac{P}{\rho_0} + g z
\end{equation}
So the equation is:
\begin{equation}
\frac{D\bm{u}}{Dt} = -\nabla \varpi + \nu\nabla^2 \bm{u} + \alpha T_1 g \hat{z}
\end{equation}
which is truly the same momentum equation as we had above, we've just used $\varpi$ to make ourselves happier.
Let's (for now) say that $P = P_n P^*$ and $\rho = \rho_n \rho^*$ such that $\varpi = (P_n/\rho_n)\varpi^*$.
Now let's pull a bunch of things out of our different terms:
\begin{equation}
\frac{\ell}{\tau^2}\frac{D \bm{u}^*}{D t^*} = -\frac{P_n}{\rho_n\ell}\nabla^* \varpi^* + \nu\frac{1}{\ell\tau}\nabla^{*2}\bm{u}^* + (\Delta T)\alpha T_1^* g\hat{z}
\end{equation}
and now, for kicks, let's multiply the equation by $\ell^3$ and recall that $\kappa = \tau/\ell^2$, such that our equation
becomes 
\begin{equation}
\kappa^2\frac{D\bm{u}^*}{D t^*} = -\frac{P_n \ell^2}{\rho_n}\nabla^*\varpi^* + \nu\kappa\nabla^{*2}\bm{u}^* + (\Delta T)\alpha\ell^3 T_1^* g\hat{z},
\end{equation}
Dividing by $\kappa^2$ and defining $P_n = \rho_n \kappa^2 / \ell^2$ (check it dimensionally!), and multiplying the
buoyancy term by $\nu/\nu$, we retrieve
\begin{equation}
\frac{D\bm{u}^*}{D t^*} = -\nabla^*\varpi^* + \frac{\nu}{\kappa}\nabla^{*2}\bm{u}^* + \frac{\alpha\ell^3 g \Delta T}{\nu\kappa}\frac{\nu}{\kappa} T_1 \hat{z},
\end{equation}
We now define the dimensionless Rayleigh number and Prandtl numbers:
\begin{equation}
\begin{split}
&\text{Ra} \equiv \frac{\alpha g \ell^3 \Delta T}{\nu \chi} \\
&\text{Pr} \equiv \frac{\nu}{\kappa},
\end{split}
\end{equation}
such that our momentum equation is, in dimensionless form (dropping the stars),
\begin{equation}
\frac{D\bm{u}}{D t} = -\nabla\varpi + \text{Pr}\nabla^2 \bm{u} + \text{Pr Ra }T_1\hat{z}
\end{equation}

\subsection{Complete set of dimensionless Boussinesq equations}
The complete dimensionless Boussinesq equations are thus:
\begin{equation}
\begin{split}
&\nabla\cdot\bm{u} = 0 \\
&\frac{D\bm{u}}{D t} = -\nabla\varpi + \text{Pr}\nabla^2 \bm{u} + \text{Pr Ra }T_1\hat{z} \\
&\frac{D T}{D t} = \nabla^2 T 
\end{split}
\end{equation}

\subsection{Mean-field decomposition of the x-component of momentum eqn}
In order to determine what happens to the $m = 0$ mode of the horizontal velocity (such as zonal flows in periodic
domains, aka the shear we're studying), we can decompose all of the variables we track in 2D Boussinesq convection as
\begin{equation}
\begin{split}
u \equiv \langle u \rangle + u_1 \\
w \equiv \langle w \rangle + w_1 \\
T \equiv \langle T \rangle + T_1 \\
\varpi \equiv \langle \varpi \rangle + \varpi_1
\end{split}
\end{equation}
where
\begin{equation}
\langle f \rangle \equiv \frac{1}{L_x} \int_0^{L_x} f dx
\end{equation}
is the horizontally-averaged profile of $f$ and the subscript 1 terms are simply the residual after the mean is subtracted,
\begin{equation}
f_1 \equiv f - \langle f \rangle.
\end{equation}
note also that the $T_1$ of the equations above is NOT the same $T_1$ as it is here.  I've called $T_1$ from the equations
above just $T$ here in order to keep consistent notation across all variables.

We can thus examine the x-component of the momentum equation (where $\bm{u} = u\hat{x} + w\hat{z}$),
\begin{equation}
\partial_t u + \bm{u}\cdot\nabla u =
-\partial_x \varpi +  \text{Pr}\nabla^2 u
\end{equation}
where we will plug in our decompositions into mean-field and fluctuating fields, as above, then take a horizontal mean
of each term, such that:
\begin{equation}
\begin{split}
&\langle\partial_t u\rangle = \langle\partial_t\langle u \rangle + \partial_t u_1 \rangle \\
&\langle\bm{u}\cdot\nabla u\rangle = \langle \angles{\bm{u}}\cdot\nabla\angles{u} + \angles{\bm{u}}\cdot\nabla u_1 +
        \bm{u}_1\cdot\nabla\angles{u} + \bm{u_1}\cdot\nabla u_1\rangle \\
&\angles{\partial_x \varpi} = \angles{\partial_x\angles{\varpi} + \partial_x \varpi_1    } \\
&\angles{\text{Pr}\nabla^2 u} = \text{Pr}\angles{ \nabla^2\angles{u} + \nabla^2 u_1   }
\end{split}
\end{equation}
By definition, the horizontal average of any $f_1$ term is exactly zero.  Similarly, this means that any $\angles{g}f_1$
term must also horizontally average to zero, because the presence of a horizontally averaged term does not affect the
inherent non-horizontally-averaged nature of the quantity once it's multiplied by a residual ($f_1$) term.  However,
two cross terms, such as $g_1 f_1$ have fluctuations which multiply with strange phases and which can have a left-over,
significant horizontal average.  Also, the horizontal average of an already-horizontally averaged term is completely
redundant, so I'll drop double brackets.  As a result, our set of terms truly decomposes to
\begin{equation}
\begin{split}
&\langle\partial_t u\rangle = \partial_t\angles{u} \\
&\langle\bm{u}\cdot\nabla u\rangle = \angles{\bm{u}}\cdot\nabla\angles{u} + \angles{ \bm{u_1}\cdot\nabla u_1 } \\
&\angles{\partial_x \varpi} = \partial_x\angles{\varpi} \\
&\angles{\text{Pr}\nabla^2 u} = \text{Pr}\nabla^2\angles{u}
\end{split}
\end{equation}
and our horizontally averaged x-momentum equation takes the form:
\begin{equation}
\frac{\partial \angles{u}}{\partial t} = -\angles{\bm{u}}\cdot\nabla\angles{u} -\bm{u_1}\cdot\nabla u_1
- \partial_x\angles{\varpi} + \text{Pr}\nabla^2 \angles{u}
\end{equation}
Which means that the presence of an $m = 0$ horizontal shear in RB convection means that one of these terms worked
to introduce a shear, and even if some of these terms are working to dissipate that shear, the term that created the
shear is continuing to feed energy into it.  In the same style of Ben's 2010 paper \cite{brown&all2010}, I will define:

\begin{equation}
\begin{split}
&P_{ma} \equiv \angles{\bm{u}}\cdot\nabla\angles{u} \,\,\text{(Production -- mean advection)}\\
&P_{fa} \equiv \angles{\bm{u_1}\cdot\nabla u_1} \,\,\text{(Production -- fluctuating advection)}\\
&P_{mp} \equiv \partial_x\angles{\varpi} \,\,\text{(Production -- mean pressure)}\\
&P_{mv} \equiv \text{Pr}\nabla^2\angles{u} \,\,\text{(Production -- mean viscous)}
\end{split}
\end{equation}

I want to quickly note here that any time we see a $\partial_x\angles{f}$, those terms are explicitly zero.  If we've
already horizontally averaged, there cannot be any variations in those terms horizontally.  That's definitional.  That's
also nice, because it means that $P_{mp} \equiv 0$.  Let's examine the other three terms in further expanded form:
\begin{equation}
\begin{split}
&P_{ma} \equiv \angles{u}\partial_x\angles{u} + \angles{w}\partial_z\angles{u} = \angles{w}\partial_z\angles{u}\\
&P_{fa} \equiv \angles{u_1\partial_x u_1 + w_1\partial_z u_1} \\
&P_{mv} \equiv \text{Pr}(\partial_x^2 \angles{u} + \partial_z^2\angles{u}) = \text{Pr}\partial_z^2\angles{u}.
\end{split}
\end{equation}
and
\begin{equation}
\frac{\partial\angles{u}}{\partial t} = - P_{ma} - P_{fa} + P_{mv}.
\end{equation}
If we track these three horizontal profiles, we should be able to see what is producing, sustaining, and trying to
destroy a horizontal shear.  This can give us insight into how to drive shear and how changes in parameters might
change the shear we need to inject.

\section{Math with the Fully Compressible Equations}

\subsection{Finding a Rayleigh Number}
As in Daniel's 2014 paper \cite{lecoanet&all2014}, the format of the FC equations with an entropy energy equation
is
\begin{equation}
\begin{split}
&\frac{\partial \rho}{\partial t} + \nabla\cdot(\rho\bm{u}) = 0 \\
&\rho\left(\frac{D \bm{u}}{D t}\right) = -\nabla P + \rho\bm{g} - \nabla\cdot\bm{\Pi} \\
&\frac{D S}{Dt} = -\frac{1}{\rho T}\Div{-\kappa \nabla S} - \frac{1}{\rho T} (\bm{\Pi}\cdot\nabla)\cdot\bm{u}
\end{split}
\end{equation}
we will further assume that our atmosphere is in hydrostatic equilibrium, such that $-\nabla P_0 + \rho_0 \bm{g} = 0$,
and the momentum equation takes (roughly) the form:
\begin{equation}
\frac{D\bm{u}}{D t} = -\frac{\nabla P_1}{\rho} + \frac{\rho_1}{\rho}\bm{g} - \nu\nabla^2\bm{u}.
\end{equation}
(note that I've fudged the last term to make it easier for non-dimensionalization).  Now let's turn to the entropy
equation,
$$
\frac{dS}{c_P} = \frac{1}{\gamma}d\ln P - d\ln\rho,
$$
where we will now take the entropy equation in the form of the ``linearized equation of state'', as in
Brown \& all 2012 \cite{brown&all2012}, which looks like
\begin{equation}
\frac{\rho_1}{\rho_0} = \frac{1}{\gamma}\frac{P_1}{P_0} - \frac{S_1}{c_P} = \frac{P_1}{P_0} - \frac{T_1}{T_0}.
\end{equation}
plugging this in to our momentum equation, we retrieve:
\begin{equation}
\frac{D\bm{u}}{D t} = -\frac{\nabla P_1}{\rho} + \frac{P_1}{\gamma P_0} \bm{g} - \frac{S_1}{c_P}\bm{g} - \nu\nabla^2\bm{u}.
\end{equation}
examining the first buoyancy term, we see that
$$
\frac{P_1}{\gamma P_0}\bm{g} = \frac{P_1\nabla P_0}{\gamma\rho_0 P_0} = \frac{P_1}{\rho_0}
\left(\frac{\nabla S_0}{c_P} + \nabla\ln\rho_0\right),
$$
which, when plugged into the momentum equation, now looks like:
\begin{equation}
\frac{D\bm{u}}{D t} = -\left(\frac{\nabla P_1}{\rho} - \frac{P_1}{\rho_0}\nabla\ln\rho_0\right)
+ \frac{P_1}{\rho_0}\frac{\nabla S_0}{c_P} - \frac{S_1}{c_P}\bm{g} - \nu\nabla^2\bm{u}.
\end{equation}
For kicks, let's define $\varpi = P_1/\rho_0$, such that our equation becomes
\begin{equation}
\boxed{
\frac{D\bm{u}}{D t} = -\nabla\varpi + \varpi\frac{\nabla S_0}{c_P} - \frac{S_1}{c_P}\bm{g} - \nu\nabla^2\bm{u}
}.
\end{equation}
Now we're in a space to non-dimensionalize.

\subsubsection{Same non-dim as Boussinesq}
Ok, so if we make the following non-dimensionalizations:
\begin{equation}
\begin{split}
& t = \tau t^* \\
& \nabla = \frac{1}{\ell}\nabla^* \\
& \varpi = \frac{P_n}{\rho_n}\varpi^* \\
& u = \frac{\ell}{\tau} u^* \\
& \chi = \frac{\ell^2}{\tau} \\
& S    = (\Delta S) S^* \\
& P_n = \frac{\rho_n \chi^2}{\ell^2} \\
& \text{Pr} = \frac{\nu}{\chi} \\
& \text{Ra} = \frac{g \ell^3 \frac{\Delta S}{c_P}}{\chi\nu}
\end{split}
\end{equation}

Then the non-dimensional momentum equation is:
\begin{equation}
\frac{ D \bm{u}^*}{D t^*} = (-\nabla^* \varpi^* + \frac{\Delta S}{c_P} \varpi^* \nabla^* S_0^*) +
\text{Ra Pr } S_1^* \hat{z} + \text{Pr} \nabla^{*2} \bm{u}^*
\end{equation}

I feel like this is....OK, but not great.  It seems that it should be possible to non-dimensionalize in a different
light than this.  

\subsubsection{Playing around with non-dimensional equations}
If we take the basic parts of non-dimensionalization (and drop all of the stars), then our momentum equation looks
about like
\begin{equation}
\frac{D \bm{u}}{D t} = -\frac{P_n \tau^2}{\rho_n \ell^2} \nabla\varpi + \frac{(\Delta S) P_n \tau^2}{c_P \ell^2\rho_n}\varpi\nabla S_0
- \frac{(\Delta S) g \tau^2}{\ell c_P} S_1\hat{z} - \frac{\nu \tau}{\ell^2}\nabla^2 \bm{u}
\end{equation}


\subsection{Mean-field decomposition of the x-component of momentum eqn}
Just as above, I'm going to decompose all of my variables that we're tracking in 2D FC
convection:
\begin{equation}
\begin{split}
& u = \angles{u} + u_1 \\
& w = \angles{w} + w_1 \\
& T = \angles{T} + T_1 \\
& \ln\rho = \angles{\ln\rho} + \ln\rho_1
\end{split}
\end{equation}
where, again, $\angles{\,}$ represent a horizontal average and $f_1 = f - \angles{f}$.

The $x$ component of the FC momentum equation looks like
\begin{equation}
\frac{\partial u}{\partial t} + \bm{u}\cdot\nabla u = 
- T\partial_x\ln\rho - \partial_x T + 
\left(\nabla\nu\cdot\bm{\sigma_1} + \nu\nabla\ln\rho\cdot\bm{\sigma_1} + \nu\Div{\sigma_1}\right),
\end{equation}
where
\begin{equation}
\bm{\sigma_1} = 
\begin{pmatrix}
2\partial_x u - \frac{2}{3}(\partial_x u + \partial_z w) \\
\partial_z u + \partial_x w
\end{pmatrix}
\end{equation}
is the first column of the viscous stress tensor with the $-\mu$ taken out from in front. 
The three terms that contain this are, expanded out,
\begin{equation}
\begin{split}
& \nabla\nu\cdot\bm{\sigma_1} = \partial_z\nu(\partial_z u + \partial_x w) \\
& \nu\nabla\ln\rho\cdot\bm{\sigma_1} = \nu\left(\partial_x\ln\rho\left[\frac{4}{3}\partial_x u - \frac{2}{3}\partial_z w  \right] +
\partial_z\ln\rho\left[\partial_z u + \partial_x w  \right] \right) \\
& \nu\Div{\bm{\sigma_1}} = \nu\left(\frac{4}{3}\partial_x^2 u - \frac{2}{3}\partial_x\partial_z w + \partial_z^2 u + \partial_x\partial_z w    \right)
\end{split}
\end{equation}
As before, we decompose each of these terms into our above divisions, and keep in mind that 
$\nu$ and $\nabla\nu$ are constant, and we get:
\begin{equation}
\begin{split}
& \partial_t u = \partial_t \angles{u} + \partial_t u_1 \\
& \bm{u}\cdot\nabla u = \angles{\bm{u}}\cdot\nabla\angles{u} + \angles{\bm{u}}\cdot\nabla u_1 +
    \bm{u_1}\cdot\nabla \angles{u} + \bm{u_1} \cdot\nabla u_1 \\
& T\partial_x\ln\rho = \angles{T}\partial_x\angles{\ln\rho} + \angles{T}\partial_x \ln\rho_1 +
    T_1\partial_x\angles{\ln\rho} + T_1\partial_x \ln\rho_1 \\
& \partial_x T = \partial_x \angles{T} + \partial_x T_1 \\
& \nabla\nu\cdot\bm{\sigma_1} = \partial_z\nu(\partial_z\angles{u} + \partial_z u_1 + \partial_x\angles{w} + \partial_x w_1) \\
& \nu\nabla\ln\rho\cdot\bm{\sigma_1} = \nu\left[
\partial_x\angles{\ln\rho}\left(\frac{4}{3}\partial_x\angles{u} - \frac{2}{3}\partial_z\angles{w}\right) +
\partial_x\angles{\ln\rho}\left(\frac{4}{3}\partial_x u_1 - \frac{2}{3}\partial_z w_1\right) \right.\\
&\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,
+ \partial_x\ln\rho_1\left(\frac{4}{3}\partial_x\angles{u} - \frac{2}{3}\partial_z\angles{w}\right) +
\partial_x\ln\rho_1\left(\frac{4}{3}\partial_x u_1 - \frac{2}{3}\partial_z w_1\right) \\
&\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,
\partial_z\angles{\ln\rho}\left(\partial_z\angles{u} + \partial_x\angles{w}\right) +
\partial_z\angles{\ln\rho}\left(\partial_z u_1 + \partial_x w_1\right) \\\
&\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\left.
+ \partial_z\ln\rho_1\left(\partial_z\angles{u} + \partial_x\angles{w}\right) +
\partial_z\ln\rho_1\left(\partial_z u_1 + \partial_x w_1\right) 
\right] \\
& \nu\Div{\sigma_1} = \nu\left(\frac{4}{3}(\partial_x^2\angles{u} + \partial_x^2 u_1) -
\frac{2}{3}(\partial_x\partial_z\angles{w} + \partial_x\partial_z w_1) +
\partial_z^2 \angles{u} + \partial_z^2 u_1 + \partial_x\partial_z\angles{w} + \partial_x\partial_z w_1 \right)
\end{split}
\end{equation}
and now I feel a strong urge to cry after typing that out.  Anyways, we're going to take a horizontal
average of the whole equation.  As above, any term with one $f_1$ gets killed by the horizontal average,
and any term with a $\partial_x \angles{f}$ will also die...by definition.  What we're left with is:
\begin{equation}
\begin{split}
&\angles{\partial_t u} = \partial_t\angles{u} \\
&\angles{\bm{u}\cdot\nabla u} = \angles{\bm{u}}\cdot\nabla \angles{u} + \angles{\bm{u_1}\cdot\nabla u_1} \\
&\angles{T\partial_x\ln\rho} = \angles{T_1\partial_x\ln\rho_1} \\
&\angles{\partial_xT} = 0 \\
&\angles{\nabla\nu\cdot\bm{\sigma_1}} = \partial_z\nu\partial_z\angles{u} \\
&\angles{\nu\nabla\ln\rho\cdot\bm{\sigma_1}} = \\
&\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,
\nu\left[\angles{\partial_x\ln\rho_1\left(\frac{4}{3}\partial_x u_1 - \frac{2}{3}\partial_z w_1\right)}
+ \partial_z\angles{\ln\rho}\partial_z\angles{u}
+ \angles{\partial_z\ln\rho_1(\partial_z u_1 + \partial_x w_1)}\right] \\
&\angles{\nu\Div{\sigma_1}} = \nu\partial_z^2\angles{u}
\end{split}
\end{equation}
As such, we will define the following driving terms in the x-direction:
\begin{equation}
\begin{split}
&\text{Px}_{ma} \equiv \angles{w}\partial_z \angles{u}              \\
&\text{Px}_{fa} \equiv \angles{u_1\partial_x u_1 + w_1\partial_z u_1} \\
&\text{Px}_{fb} \equiv \angles{T_1\partial_x\ln\rho_1} \\
&\text{Px}_{mv} \equiv \partial_z\nu\partial_z\angles{u} + \nu\partial_z^2\angles{u} +
            \nu\partial_z\angles{\ln\rho}\partial_z\angles{u} \\
&\text{Px}_{fv} \equiv \nu\left[\angles{\partial_x\ln\rho_1\left(\frac{4}{3}\partial_x u_1 - \frac{2}{3}\partial_z w_1\right)}
            + \angles{\partial_z\ln\rho_1(\partial_z u_1 + \partial_x w_1}\right]
\end{split}
\end{equation}
where ``ma" = ``mean advection", ``fa" = ``fluctuating advection", ``fb" = ``fluctuating buoyancy", 
``mv" = ``mean viscosity", and ``fv" = ``fluctuating viscosity".  Our horizontally averaged
momentum equation takes the form:
\begin{equation}
\frac{\partial\angles{u}}{\partial t} = -\text{Px}_{ma} - \text{Px}_{fa} - \text{Px}_{fb}
+ \text{Px}_{mv} + \text{Px}_{fv}.
\end{equation}
Chances are, ``ma'' will be zero, so it's up to the other four terms to produce and dissipate
the shear.  Neat.

\bibliographystyle{plain}
\bibliography{../../../biblio.bib}
\end{document}
