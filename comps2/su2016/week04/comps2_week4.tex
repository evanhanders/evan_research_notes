\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}
\usepackage{cancel}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{(Su2016) Week 4 -- comps 2}
\cfoot{\thepage}
\rfoot{July 16, 2016}

\newcommand{\FT}{\ensuremath{\text{FT}}}
\newcommand{\Div}[1]{\ensuremath{\nabla\cdot\bm{#1}}}
\newcommand{\eqs}{\begin{equation}}
\newcommand{\eqe}{\end{equation}}


\begin{document}
\section{3D-ifying the Navier Stokes equations}
\subsection{Fully Compressible Navier Stokes equations}
These equations look about like this
\begin{equation}
\begin{split}
\partial_t\rho + \nabla\cdot(\rho\bm{u}) &= 0 \\
\partial_t \bm{u} + \bm{u}\cdot\nabla{u} &= -\frac{1}{\rho}\nabla P - \bm{g} - \frac{1}{\rho}\nabla\cdot(-\mu\bm{\bar{\sigma}}) \\
\partial_t T + \bm{u}\cdot\nabla T + (\gamma - 1)T \nabla\cdot\bm{u} &= \frac{1}{\rho C_V}\nabla\cdot(\rho\chi\nabla T)
+ \frac{\nu}{C_V}(\bm{\bar{\sigma}}\cdot\nabla)\cdot\bm{u}
\end{split}
\end{equation}
But the problem is that there are too many variables here.  We need to use our equation of state, $P = \rho T$ and some
fancy math tricks to make our lives better.  So doing those, here's the system of equations we actually solve:
\begin{equation}
\begin{split}
\partial_t \ln\rho + \bm{u}\cdot\nabla\ln\rho + \Div{u} &= 0 \\
\partial_t \bm{u} + \bm{u}\cdot\nabla{u} &= -\nabla T - T \nabla\ln\rho - \bm{g} + \nu\Div{\bar{\sigma}} +
(\nu\nabla\ln\rho + \nabla\nu)\cdot\bm{\bar{\sigma}} \\
\partial_t T + \bm{u}\cdot\nabla T + (\gamma - 1)T \nabla\cdot\bm{u} &=\\ 
& -\frac{1}{C_V}\nabla\cdot(\chi \nabla^2 T + \nabla\chi \cdot\nabla T + \chi\nabla\ln\rho\cdot\nabla T) \\
& + \frac{\nu}{C_V}(\nabla^2\bm{u} + \frac{1}{3}\nabla(\Div{u}))\cdot\bm{u}
\end{split}
\end{equation}
where, by the way,
\begin{equation}
\bm{\bar{\sigma}}_{ij} = \partial_{x_i}u_j + partial_{x_j}u_i - \frac{2}{3}\Div{u},
\end{equation}
so, in dedalus speak,
\begin{equation}
\bm{\bar{\sigma}} = \begin{pmatrix}
2 dx(u) - \frac{2}{3}\Div{u}   & dx(v) + dy(u)                & dx(w) + u\_z \\
dx(v) + dy(u)                  & 2 dy(v) - \frac{2}{3}\Div{u} & dy(w) + v\_z \\
dx(w) + u\_z                   & dy(w) + v\_z                 & 2w\_z - \frac{2}{3}\Div{u}
\end{pmatrix},
\end{equation}
where \_z means a partial derivative in the z-direction, and $\bm{u} = (u, v, w)$.





\subsection{Continuity Equation -- Dedalus style}
This is the easy one.  In dedalus, this is just:
\begin{equation}
dt(ln\_rho1) + Div\_u + u*dx(ln\_rho1) + v*dy(ln\_rho1) + w*dz(ln\_rho1) = 0,
\end{equation}
where we've introduced the substitution(s):
\begin{equation}
\begin{split}
Div(fx, fy, f\_z) &= (dx(fx) + dy(fy) + f\_z) \\
Div\_u            &= Div(u, v, w\_z)
\end{split}
\end{equation}



\subsection{Momentum Equation -- Dedalus style}
There are a lot more moving pieces here.  We have to worry about linear and non-linear terms, as well.  Oh,
and this is actually three different equations.  Just as a quick note, we define the substitution:
\begin{equation}
UdotGrad(f, f\_z) = (u*dx(f) + v*dy(f) + w*f\_z)  
\end{equation}

There's a fair bit of nonsense before we even get to the viscous term, so we'll focus on that, here.  On the RHS,
we have:
\begin{equation}
-\nabla T - T\nabla\ln\rho - \bm{g} = -(\underbrace{\nabla T_1 + T_1\nabla\ln\rho_0 + T_0\nabla\ln\rho_1}_{\text{linear}})
-  (\underbrace{T_1\nabla\ln\rho_1}_{\text{non-linear}})
-  (\underbrace{T_0\nabla\ln\rho_0 + \nabla T_0 - \bm{g}}_{\text{HS equilibrium}}).
\end{equation}

Then there are the viscous terms to deal with.  These, as above, take the form:
\begin{equation}
\nu\nabla\cdot\bm{\bar{\sigma}} + (\nu\nabla\ln\rho + \nabla\nu)\cdot\bm{\bar{\sigma}}
\end{equation}
on the RHS of the equation, but most of these terms are honestly linear.  Only terms with a $\ln\rho_1$ from the
second term will be non-linear.  That being said, if you do some math on these, the linear term is
\begin{equation}
linear\_viscous\_vector = \nu\nabla^2\bm{u} + \frac{1}{3}\nu\nabla(\Div{u}) + 
\underbrace{(\nu*dz(\ln\rho_0)+dz(\nu))}_{\text{`easy rho' term}}*(\nabla w + dz(\bm{u}) - \frac{2}{3}\Div{u}\hat{z})
\end{equation}
The `easy rho' term is zero if $\nu\propto\rho^{-1}$.
The result of all of this is that we have the following substitutions:
\begin{equation}
\begin{split}
Lap(f, f\_z) &= (dx(dx(f)) + dy(dy(f)) + dz(f\_z) ) \\
linear\_viscous\_u     &= \nu*[Lap(u, u\_z) + \frac{1}{3}Div(dx(u), dx(v), dx(w\_z))] \\
                       \,\,\,\,&+ (\nu*dz(\ln\rho_0)+dz(v))*\
                         (dx(w) + u\_z) \\
linear\_viscous\_v     &= \nu*[Lap(v, v\_z) + \frac{1}{3}Div(dy(u), dy(v), dy(w\_z))] \\
                       \,\,\,\,&+ (\nu*dz(\ln\rho_0)+dz(\nu))*\
                         (dy(w) + v\_z) \\
linear\_viscous\_w     &= \nu*[Lap(w, w\_z) + \frac{1}{3}Div(u\_z, v\_z, dz(w\_z))] \\
                       \,\,\,\,&+ (\nu*dz(\ln\rho_0)+dz(\nu))*\
                         (2w\_z - \frac{2}{3}*Div\_u)
\end{split}
\end{equation}
The non-linear terms you just kind of have to fight through,
\begin{equation}
\begin{split}
nonlinear\_viscous\_u     = \nu*[dx(\ln\rho_1)*(2*dx(u)-\frac{2}{3}Div\_u) &+ \
                                  dy(\ln\rho_1)*(dx(v) + dy(u)) \\
                                  &+ dz(\ln\rho_1)*(dx(w) + u\_z) ]    \\
nonlinear\_viscous\_v     =  \nu*[dx(\ln\rho_1)*(dx(v) + dy(u)) &+ \
                                  dy(\ln\rho_1)*(2dy(v) - \frac{2}{3}Div\_u) \\
                                  &+ dz(\ln\rho_1)*(dy(w) + v\_z) ]    \\
nonlinear\_viscous\_w     =  \nu*[dx(\ln\rho_1)*(dx(w) + u\_z) &+ \
                                  dy(\ln\rho_1)*(dy(w) + v\_z) \\
                                  &+ dz(\ln\rho_1)*(2w\_z - \frac{2}{3}Div\_u) ]    \\
\end{split}
\end{equation}

So using all of these subs, our x, y, and z momenta equations are:
\begin{equation}
\begin{split}
dt(u) +& dx(T1) + T0*dx(ln\_rho1) - viscous\_linear\_u = \\
 &-T1*dx(ln\_rho1) - UdotGrad(u, u\_z) + viscous\_nonlinear\_u
\end{split}
\end{equation}

\begin{equation}
\begin{split}
dt(u) +& dy(T1) + T0*dy(ln\_rho1) - viscous\_linear\_v = \\
 &-T1*dy(ln\_rho1) - UdotGrad(v, v\_z) + viscous\_nonlinear\_v
\end{split}
\end{equation}

\begin{equation}
\begin{split}
dt(u) +& T1\_z + T0*dz(ln\_rho1) + T1*dz(ln\_rho0) - viscous\_linear\_w = \\
 &-T1*dz(ln\_rho1) - UdotGrad(w, w\_z) + viscous\_nonlinear\_w - HS\_equilib
\end{split}
\end{equation}


\subsection{Energy Equation}
The first three terms on the LHS are fairly straight-forward:
\begin{equation}
\underbrace{\partial_t T + \bm{u}\cdot\nabla T0 + (\gamma-1)T0\Div{u}}_{\text{linear}} +
\underbrace{\bm{u}\cdot\nabla T1 + (\gamma-1)T1\Div{u}}_{\text{non-linear}}
\end{equation}

The next one is way less fun:
\begin{equation}
\begin{split}
\frac{1}{\rho C_V}\nabla\cdot(-\rho\chi\nabla T) &=\\
&-\frac{1}{C_V}\underbrace{\chi\nabla^2 T1 + \nabla\chi\cdot\nabla T1 + \chi(\nabla\ln\rho_0\cdot\nabla T1 + \nabla\ln\rho_1\cdot\nabla T0)}_{\text{linear}} \\
&-\frac{1}{C_V}\underbrace{\chi\nabla\ln\rho_1\cdot\nabla T1}_{\text{non-linear}} \\
&-\frac{1}{C_V}\underbrace{\chi\nabla T0 + \underbrace{(\nabla\chi + \chi\nabla\ln\rho_0)\cdot\nabla T0}_{\text{easy rho}} }_{\text{source terms}}
\end{split}
\end{equation}

And then the viscous term is a bit gross, and it's all nonlinear, but it actually directly parallels stuff in the momentum
equation linear part.  It takes the form
\begin{equation}
\frac{\nu}{C_V}\left(\nabla^2\bm{u} + \frac{1}{3}\nabla(\Div{u})\right)\cdot\bm{u}.
\end{equation}
And so we make the following substitutions:
\begin{equation}
\begin{split}
NL\_visc&\_heat = \frac{nu}{C_V} *\left(\right. Lap(u, u\_z)*u + Lap(v, v\_z)*v + Lap(w, w\_z)*w  \\
& + \frac{1}{3}\left[Div(dx(u), dx(v), dx(w\_z))*u + Div(dy(u), dy(v), dy(w\_z)) + Div(u\_z, v\_z, dz(w\_z))\right]\left.\right) \\
L\_therm& = 1/C_V * (chi*(Lap(T1, T1_z) + dz(ln\_rho0)*dz(T1)+dz(ln\_rho1)*dz(T0))  \\
&+ dz(chi)*dz(T1)) \\
NL\_ther&m = 1/C_V * chi *(dx(ln\_rho1)*dx(T1) + dy(ln\_rho1)*dy(T1) + dz(ln\_rho1)*T1\_z) \\
source\_t&erms = 1/C\_V * (chi*dz(T0) + (dz(chi) + chi*dz(ln\_rho0))*dz(T0))
\end{split}
\end{equation}

with these substitutions, our energy equation is
\begin{equation}
\begin{split}
dt(T1) + &w*dz(T0) + (gamma - 1)*T0*Div\_u - L\_therm =  \\
&-UdotGrad(T1, T1\_z) - (gamma-1)*T1*Div\_u  \\
&+ NL\_visc\_heat + NL\_therm + source\_terms
\end{split}
\end{equation}


\bibliographystyle{plain}
\bibliography{../../../biblio.bib}
\end{document}
