\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}
\usepackage{cancel}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{(Su2016) Week 2 -- comps 2}
\cfoot{\thepage}
\rfoot{June 25, 2016}

\newcommand{\FT}{\ensuremath{\text{FT}}}
\newcommand{\Div}[1]{\ensuremath{\nabla\cdot\bm{#1}}}
\newcommand{\eqs}{\begin{equation}}
\newcommand{\eqe}{\end{equation}}


\begin{document}




\section{Revisiting the total energy equations -- e.g. week 2 of sp2016}
Ok, so now that I actually understand a bit more about the equations and how they work, I'm going to 
revisit the discussion I had in week 2 of the sp2016 notes and expand on it more and actually arrive at
the correct answer.

In general, an energy conserving system should have a total energy that looks like:
\eqs
\dot{E} = -\Div{\text{Fluxes}},
\eqe
where $E$ is the system energy.  Fluxes are the physical ways in which energies move about our system,
and any other terms would be terms that either generate or dissipate energy unphysically.  So generally
we want to avoid terms that we can't put into a conservation form like above.
Our equation set has two equations which deal with energies:
the momentum equation, which controls kinetic energy and a number of other terms, and the
energy/heat equation, which controls internal energy and has extra terms to ensure that energy
is conserved.

I'm going to do some copying+pasting of my arguments from earlier:


\subsection{Kinetic energy equation}
To start, I'm going to move everything in my momentum equation to the LHS, and then I'm going
to dot the whole equation with $\bm{u}$.
\begin{equation}
\bm{u}\cdot\left(
\rho(\partial_t\bm{u} + \bm{u}\cdot\nabla\bm{u}) + \nabla P
+ \rho\nabla\phi + \nabla\cdot\bm{\Pi}\right) = 0.
\end{equation}
where I've replaced $g$ with $-\nabla\phi$. Let's do this step by step.

\subsubsection{$\bm{u}\cdot(\rho\partial_t\bm{u})$}
Just using the chain rule, this is
$$
\boxed{
\bm{u}\cdot(\rho\partial_t\bm{u}) = 
\frac{1}{2}\left(\partial_t[\rho |u|^2] - |u|^2\partial_t\rho\right)  }.
$$

\subsubsection{$\bm{u}\cdot\rho(\bm{u}\cdot\nabla)\bm{u}$}
Ok, before we dot this, let's just examine $(\bm{u}\cdot\nabla\bm{u})$:
$$
(\bm{u}\cdot\nabla)\bm{u} = \frac{1}{2}\nabla(\bm{u}\cdot\bm{u}) - \bm{u}\times(\nabla\times\bm{u}).
$$
Multiplying by $\rho$ and then dotting with $\bm{u}$, the second term disappears
(it is definitionally orthogonal to $\bm{u}$), so
$$
\bm{u}\cdot\rho(\bm{u}\cdot\nabla)\bm{u} = \frac{1}{2}\bm{u}\cdot\rho\nabla(|u|^2) = 
\frac{1}{2}\bm{u}\cdot(\nabla(\rho|u|^2) - |u|^2\nabla\rho) =
\nabla\cdot(\frac{1}{2}\rho|u|^2\bm{u}) - 
\frac{1}{2}\rho|u|^2\nabla\cdot\bm{u} - \frac{1}{2}|u|^2\bm{u}\cdot\nabla\rho.
$$
and thanks to the continuity equation,
$$
\partial_t\rho = -\rho\nabla\cdot\bm{u} - \bm{u}\cdot\nabla\rho.
$$
Thus,
$$
\boxed{
\bm{u}\cdot\rho(\bm{u}\cdot\nabla)\bm{u} = \nabla\cdot(\frac{1}{2}\rho|u|^2\bm{u})
+ \frac{1}{2}|u|^2\partial_t\rho    }.
$$

\subsubsection{$\bm{u}\cdot\nabla P$}
This is just
$$
\boxed{
\bm{u}\cdot\nabla P = \nabla\cdot(\bm{u}P) - P\nabla\cdot\bm{u}
} .
$$

\subsubsection{$\bm{u}\cdot(\rho\nabla\phi)$}
There are two steps to this:
$$
\rho\nabla\phi = \nabla(\rho\phi) - \phi\nabla(\rho)
$$
And then
$$
\boxed{
\bm{u}\cdot(\nabla(\rho\phi) - \phi\nabla(\rho)) = 
\nabla\cdot(\rho\phi\bm{u}) - \rho\phi\nabla\cdot\bm{u} - \bm{u}\cdot(\phi\nabla\rho)
} .
$$

\subsubsection{$\bm{u}\cdot(\nabla\cdot\bm{\Pi})$}
This one doesn't trivially fall out of the formulary, so I'm going to examine in in
a roundabout way.
The first thing I'm going to do is define the two rows of the stress tensor:
\begin{equation}
\begin{split}
\bm{\Pi}_1 &= \begin{pmatrix}\Pi_{11} \\ \Pi_{21}\end{pmatrix} \\
\bm{\Pi}_2 &= \begin{pmatrix}\Pi_{12} \\ \Pi_{22}\end{pmatrix}.
\end{split}
\end{equation}

And in two dimensions,
$$
\nabla\cdot\bm{\Pi} = \nabla\cdot\bm{\Pi}_1\hat{x} + \nabla\cdot\bm{\Pi}_2\hat{z}.
$$
As a result, using some vector identities, and noting that $u = u_x$ and $w = u_z$,
$$
\boxed{
\bm{u} \cdot (\nabla\cdot\bm{\Pi}) = 
\nabla\cdot(u\bm{\Pi}_1 + w\bm{\Pi}_2) - \bm{\Pi}_1\cdot\nabla u - \bm{\Pi}_2\cdot\nabla w
= \nabla\cdot(\bm{u}\cdot\bm{\Pi}) - (\bm{\Pi}\cdot\nabla)\cdot\bm{u}
}.
$$

\subsubsection{Putting it all together}
Putting all of these terms together, we find
\begin{equation}
\boxed{
\begin{aligned}
\partial_t\left(\frac{\rho|u|^2}{2}\right) +
\nabla\cdot\left(\bm{u}\frac{\rho|u|^2}{2} + \bm{u}P + \bm{u}\rho\phi + \bm{u}\cdot\bm{\Pi} \right)
= \\
P\nabla\cdot\bm{u} + \rho\phi\nabla\cdot\bm{u} + \bm{u}\cdot(\phi\nabla\rho)
+ (\bm{\Pi}\cdot\nabla)\cdot\bm{u}
\end{aligned}
}
\end{equation}

So that's the kinetic energy part of the equation from the momentum equation itself.  The other part of
the equation follows...

\subsection{Internal Energy Equation}
This time I'm going to move everything to the LHS except for the viscous term.
\begin{equation}
\rho C_V \partial_t T + \rho C_V \bm{u} \cdot \nabla T +
\rho C_V (\gamma-1)T\nabla\cdot\bm{u} + \nabla\cdot(-\rho\chi\nabla T) = 
-  (\bm{\Pi}\cdot\nabla)\cdot\bm{u}
\end{equation}
Let's break up terms and look at them, again.  Also, $C_V(\gamma-1) = 1$, so that third term isn't so bad.

\subsubsection{$\rho C_V \partial_t T$}
This is another chain rule term,
$$
\boxed{
\rho C_V \partial_t T = C_V \partial_t (\rho T) - C_V T\partial_t \rho }.
$$

\subsubsection{$\rho C_V \bm{u} \cdot \nabla T$}
This term is
$$
\boxed{
\rho C_V \bm{u} \cdot \nabla T = C_V\left(\nabla\cdot(\rho\bm{u}T) - T\nabla\cdot(\rho\bm{u})\right)
= C_V\left(\nabla\cdot(\rho\bm{u}T) - T[\rho\nabla\cdot \bm{u} + \bm{u}\cdot\nabla\rho]\right)
}
$$

\subsubsection{$\nabla\cdot(-\rho\chi\nabla T)$}
This is already in flux form, huzzah.

\subsubsection{Overall equation}
Our energy equation looks like
\begin{equation}
\boxed{
\partial_t(C_V\rho T) + \nabla\cdot\left(C_V\rho\bm{u}T - \rho\chi\nabla T \right)
= C_V T \partial_t\rho + C_V\rho T \Div{u} + C_V\bm{u}T\cdot\nabla\rho + \rho T\Div{u} - (\bm{\Pi}\cdot\nabla)\cdot\bm{u}
}
\end{equation}
As a quick note, with our EOS, $P = \rho T$, and so the second term and second to last
terms on the RHS are really $P\Div{u}$ terms.

\subsection{Adding our two equations together}
Without cancelling anything, if I just add the two sides together, I get
\begin{equation}
\begin{split}
\partial_t\left(\frac{\rho|u|^2}{2} + C_V\rho T\right) +
\nabla\cdot\left(\bm{u}\left[\frac{\rho|u|^2}{2} + P + C_V\rho T + \rho\phi\right] + \bm{u}\cdot\bm{\Pi}  
- \rho\chi\nabla T \right)
=& \\
P\nabla\cdot\bm{u} + \rho\phi\Div{u} + \bm{u}\cdot(\phi\nabla\rho) +(\bm{\Pi}\cdot\nabla)\cdot\bm{u}
+ C_V T\partial_t\rho +&\\
C_V\rho T\Div{u} + C_V\bm{u} T\cdot\nabla\rho - P\Div{u} - (\bm{\Pi}\cdot\nabla)\cdot\bm{u},
\end{split}
\end{equation}
where the $-P\Div{u}$ came from the third term in the internal energy equation and our EOS.  Clearly,
the two $P\Div{u}$ annihilate.  So do the two viscous flux terms.  Then the RHS is
$$
(\phi + C_V T)(\rho\Div{u}) + (\phi + C_V T)(\bm{u}\cdot\nabla\rho) + C_V T\partial_t\rho =
-(\phi + C_V T)(\partial_t\rho) + C_V T\partial_t\rho = -\phi\partial_t\rho.
$$

Or, if I've done my math correctly and I move the $\partial_t$ term to the LHS (and pull
$\phi$ inside the derivative because it's time-invariant),
the overall energy equation we have is:
\eqs
\boxed{
\partial_t\left(\frac{\rho|u|^2}{2} + C_V\rho T + \rho\phi\right) +
\nabla\cdot\left(\bm{u}\left[\frac{\rho|u|^2}{2} + P + C_V\rho T + \rho\phi\right] + \bm{u}\cdot\bm{\Pi}  
- \rho\chi\nabla T \right) = 0
}.
\eqe
We have a consistent set of equations which conserve energy (kinetic, internal, and gravitational
potential) and which have a number of
flux terms, including KE flux ($\rho u^2$ term), Enthalpy flux ($P + C_V\rho T$ term),
GPE flux ($\rho\phi$ term), viscous flux ($\bm{u}\cdot\bm{\Pi}$ term), and radiative/conductive
flux ($\rho\chi\nabla T$ term).

The cool thing about all of this is that I finally understand why the viscous heating term exists.  It's not
there because ``we're so cool that we're accounting for the heat that viscosity creates!'' -- it's actually
because without that term, the viscous term of the momentum equation leaks energy out of the system, which is
a really bad thing to do.  The viscous heating term exists specifically to ensure that we conserve energy
in our system, which is pretty neat.  Over the course of runs, the momentum equation will
lose some energy to viscosity, but the viscous heating term will return that energy into the system
via the energy equation as internal energy.


\section{Deriving the Internal Energy Equation}
Because this needs to be done.  I showed above where the viscous term comes from, but I still
haven't shown where the other terms come in.  Let's explore some.

\subsection{Entropy Equation}
This one's actually quite simple.  I'm a little sad I didn't see it before, or do it before.  Regardless,
we begin with the first law of thermodynamics:
\eqs
dU = TdS - PdV + \cancelto{0}{\mu dN}.
\eqe
I'm dealing with nice, ideal gas particles of one species, so I'm dropping the chemical potential term.
Since volume is just
\eqs
V = \frac{\mu N}{\rho} = \frac{M}{\rho},
\eqe
and
\eqs
dV = d\frac{M}{\rho} = -\frac{M}{\rho^2}d\rho,
\eqe
our first law of thermo becomes
\eqs
dU = TdS - \frac{M P}{\rho^2}d\rho.
\eqe
Dividing by $M$, where $s \equiv S/M$ is the specific entropy and $e = U/M$ is the specific energy,
and rearranging, we have the \emph{actually useful first law of thermodynamics}:
\eqs
\boxed{
Tds = de - \frac{P}{\rho^2} d\rho.
}\label{eqn:good_first_law}
\eqe

For an ideal gas, the EOS is
\eqs
P = \frac{k_B}{\mu} \rho T,
\eqe
where $k_B$ is the Boltzmann constant and $\mu$ is the molecular weight of a given particle.  If the ideal gas is
calorically perfect, it simultaneously follows the EOS
\eqs
e = \frac{1}{\gamma-1}\frac{P}{\rho} = C_V \frac{k_B}{\mu} T,
\eqe
where $C_V$ is the specific heat at constant volume.  Plugging these in to Eqn. \ref{eqn:good_first_law}, we retrieve
\eqs
Tds = C_V\frac{k_B}{\mu} dT - \frac{T}{\rho}d\rho.
\eqe
Dividing through by $T$, we retrieve the \emph{equation for specific entropy}
\eqs
\boxed{
ds = C_V\frac{k_B}{\mu}d\ln T - d\ln\rho
}. \label{eqn:entropy_eqn}
\eqe
In our runs, we have $k_B = \mu = 1$, so it reduces slightly.  Note that this is not the ``specific entropy'' Ben and
I usually talk about, which is $s/C_P,$ which is a non-dimensional version of $s$.  I'm not sure how the lingo
of ``specific entropy'' works, exactly, compared to (e.g.) specific energy.  Need to ask Ben about that one.  Regardless,
$\gamma = c_P/c_V$, so that form of the equation is
\eqs
\frac{ds}{c_P} = \frac{1}{\gamma}\frac{k_B}{\mu}d\ln T - \frac{\gamma-1}{\gamma}d\ln\rho
\eqe

Nailed it.  One of our equations down.

\subsection{Energy Equation}
For an idealized, closed system, the second law of thermodynamics states:
\eqs
ds = \frac{\delta Q}{T},
\eqe
where $Q$ is work/energy being put into the system, and the $\delta$ means that it's a small input of energy relating
to a differential change of entropy.  Plugging in our earlier expression from the entropy equation for $ds$, we retrieve
\eqs
c_V d\ln T - d \ln \rho = \frac{\delta Q}{T}.
\eqe
I'm going to multiply through by $T$ and divide through by $c_V$, as well.  Also, I'm going to expand all differential
changes out as material derivatives:
\eqs
d[x] = \frac{\partial x}{\partial t} + \bm{u}\cdot\nabla x = \frac{D x}{Dt},
\eqe
and my energy equation is now
\eqs
\frac{\partial T}{\partial t} + \bm{u}\cdot\nabla T - \frac{1}{c_V}T\left(\frac{\partial\ln\rho}{\partial t} + 
\bm{u}\cdot\nabla\ln\rho\right) = \frac{1}{c_V} \delta Q.
\eqe
We know from the continuity equation that $\partial_t\ln\rho + \bm{u}\cdot\nabla\ln\rho = -\Div{u}$, and
$c_V^{-1} = (\gamma - 1)$, so the energy equation is
\eqs
\frac{D T}{Dt} + (\gamma - 1)T\Div{u} = c_V^{-1}\delta Q.
\eqe
Obviously, one of the terms of $\delta Q$ is $-(\bm{\Pi}\cdot\nabla)\cdot\bm{u})/\rho$, the viscous
heating term, which arises to ensure that our system conserves energy.  

There is one more term which will arise: the radiative/conductive flux.  I'm going to wave my hands
around because...eh, every explanation I've ever seen just uses physical intuition to include
this term.  Ben says that there's
a nice discussion of this in \cite{landau&lifshitz1959} and also in \cite{lecoanet2014}.  But
just logically, if we were to use the equation in its current state (without radiative flux),
our energy equation that I presented at the end of section 1 would only have enthalpy flux
to move around internal energy.  While that's great and all, even a fluid at rest moves energy 
around when there are temperature gradients.  Hot will conduct towards cool, so there must be
a \emph{countergradient flux} that goes against the temperature gradient.  Depending
on the type of fluid, it can either be really good at getting heat through it or really bad,
and this is wrapped up in $\kappa = \rho\chi$, the conductivity, where $\chi$ is the thermal
diffusivity.  As a result, we have a term like $\nabla\cdot(-\kappa\nabla T)/\rho$, where
since $\kappa\nabla T$ clearly has the units of a flux it needs another derivative (a Div for
consistency -- we want to see how much flux is flowing into/out of a point), and then it'll have
the right units for the energy equation.

With these two terms added in, the internal energy / temperature equation becomes 
\eqs
\boxed{
\frac{D T}{Dt} + (\gamma - 1)T\Div{u} + \frac{1}{\rho c_V}\nabla\cdot(-\kappa\nabla T)
= -\frac{1}{\rho c_V}(\bm{\Pi}\cdot\nabla)\cdot\bm{u}}.
\eqe
With some minor rearrangement of the $\kappa$ term, this is exactly what we solve in Dedalus
in our FC equations.

\bibliographystyle{plain}
\bibliography{../../../biblio.bib}
\end{document}
