\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{Week 1 -- comps 2}
\cfoot{\thepage}
\rfoot{Feb. 1, 2016}

\newcommand{\FT}{\ensuremath{\text{FT}}}

\begin{document}
\section{Fully Compressible Navier Stokes Equations}
The Navier Stokes equations describe the motions of fluid elements.  In order
to fully describe their motion, we require four equations: a continuity
equation, a momentum equation, an energy equation, and an equation of
state (for closure).

The first of these three equations are as follows:
\begin{align}
\partial_t \rho + \nabla\cdot(\rho\bm{u}) =
\partial_t \rho + \bm{u}\cdot\nabla\rho + \rho\nabla\cdot\bm{u} = 0
&\text{ (continuity)}, \label{continuity_full}\\
\rho(\partial_t\bm{u} + \bm{u}\cdot\nabla\bm{u}) + \nabla P =
\bm{g}\rho - \nabla\cdot\bm{\Pi}
&\text{ (momentum)}, \label{momentum_full}\\
\partial_t S + \bm{u} \cdot \nabla S =
-\frac{1}{\rho T}\nabla\cdot\bm{Q} - \frac{1}{\rho T}\Pi_{ij}\partial_{x_i}u_j
&\text{ (energy)} \label{energy_full},
\end{align}
where $\rho$ is the density, $\bm{u}$ is the velocity vector, $P$ is the
pressure field, $\bm{g}$ is the gravity vector, $\bm{\Pi}$ is the stress
tensor, $S$ is the entropy field, $T$ is the temperature field, and
$\bm{Q}$ is the heat flux.  The $\Pi_{ij}$ term in the energy equation is
a summation over all dimensions (x, y, z) of that term for each $i$th and $j$th
dimensional component.  Furthermore, in my derivation which follows,
I will assume that the fluid is an ideal gas, such that
\begin{equation}
P =\frac{R}{m_h}\rho T = R_s \rho T,
\end{equation}
where $\rho$ is density, $P$ is the pressure field, $T$ is temperature, 
$m_h$ is the molecular mass of particles in the system, $R$ is the gas constant (generally
the boltzmann constant, here), and
$R_s$ is the \emph{specific} gas constant.

In our system, we non-dimensionalize this equation such that $R_s = 1$, and the ideal
gas law becomes
\begin{equation}
P = \rho T.
\end{equation}

I'll need one further bit of information to solve for my equations: the stress
tensor.  The stress tensor is defined as
\begin{equation}
\Pi_{ij} = -\mu\left(\partial_{x_i}u_j + \partial_{x_j}u_i 
- \frac{2}{3}\delta_{ij}\nabla\cdot\bm{u}\right)\label{stress_tensor},
\end{equation}
where $\delta_{ij}$ is the Kronecker delta and $\mu$ is the dynamic viscosity.
I will solve for the fully
compressible equations in \emph{two dimensions}, where my velocity vector takes
the form $\bm{u} = u\hat x + w\hat z$, so my tensor takes the form
\begin{equation}
\bm{\Pi} = -\mu
\begin{pmatrix}
\frac{4}{3}\partial_x u - \frac{2}{3}\partial_z w &
\partial_z u + \partial_x w \\
\partial_z u + \partial_x w &
\frac{4}{3}\partial_z w - \frac{2}{3}\partial_x u
\end{pmatrix}. \label{stress_tensor_full}
\end{equation}
A further simplification that I will make is that I will define $\nu = \mu/\rho$, and
I will \emph{set this constant} throughout my domains in my first simulations. As a
result, $\mu$ and $\rho$ (microscopic quantities) will have the same shape throughout
my domain.

One more note: I'm going to break up most of my fields into background fields
and fluctuations (sort of like what you do when you linearize and solve for
wave solutions, but I won't be linearizing).  So as I go along, I'll be
breaking up into background (subscript 0) and fluctuating (subscript 1) fields,
and I'll assume that \emph{background fields are constant in time}.  Further,
I'll assume that \emph{background fields and spacially invariant in the
$x$-direction}.  This will simplify some things.

\section{Initial conditions: Polytrope}
My simulations will have \emph{polytropic} initial conditions, which means that
\emph{temperature is linear throughout the domain} and we assume that our fields
take the form:
\begin{equation}
\begin{split}
T_0 &= T_{00}(z_0 - z) \\
\rho_0 &= \rho_{00}(z_0 - z)^m \\
P_0 &= P_{00}(z_0 - z)^{m+1}.
\end{split}
\end{equation}
Where all of the ``00'' subscripts refer to the values of those variables at
the top of my domain.  Here, $m$ is the polytropic index and $z_0 = L + 1$, where
$L$ is the length of the domain.  Due to our non-dimensionalized ideal gas law,
I have the constraint that $P_{00} = \rho_{00}T_{00}$.  I will take gradients of all 
of these properties to find that
\begin{equation}
\begin{split}
\nabla\ln P &= \frac{\nabla P}{P} = -(m+1)(z_0-z)^{-1} \\
\nabla\ln\rho &= \frac{\nabla\rho}{\rho} = -m(z_0-z)^{-1} \\
\nabla\ln T &= \frac{\nabla T}{T} = -(z_0-z)^{-1}.
\end{split}
\end{equation}
Combining these results with the entropy equation, I find
\begin{equation}
\begin{split}
\frac{1}{C_P}\nabla S &= \frac{1}{\gamma}\nabla\ln P - \nabla\ln \rho\\
&= -\left(\frac{m+1}{\gamma} - m \right)(z_0-z)^{-1} \\
\end{split}
\end{equation}
For an adiabatic system, $\nabla S = 0$, so $(m+1)/\gamma - m$ must be
zero.  In other words, an adiabatic system has the property that
\begin{equation}
m = \frac{1}{\gamma - 1} = \frac{3}{2} \text{ for }\gamma = \frac{5}{3}.
\end{equation}
Thus, we can use this value to determine when a system is adiabatic,
subadiabatic, or superadiabatic.  For $\gamma = 5/3$,
\begin{equation}
m = \left\{
\begin{array}{lr}
1.5 - \epsilon & \nabla S < 0\text{, superadiabatic} \\
1.5 & \nabla S = 0\text{, adiabatic} \\
1.5 + \Delta & \nabla S > 0\text{, subadiabatic}
\end{array}
\right.,
\end{equation}
where $\epsilon$ is some small positive value and $\Delta$ is some positive
value of any magnitude.

In the case of an unstable solution, the polytrope has an adiabatic gradient. Thus,
\begin{equation}
\begin{split}
\frac{\nabla S}{C_P} &= -\left(\frac{1 + (\gamma-1)(1-\epsilon)}{\gamma(\gamma-1)} - 
                            \frac{1}{\gamma-1} + \epsilon\right)(z_0-z)^{-1} \\
&= -\left(\frac{(\gamma - 1)(1 - \epsilon) + 1 - \gamma + \epsilon\gamma(\gamma-1)}
                {\gamma(\gamma-1)}\right)(z_0-z)^{-1} \\
&= -\left(\frac{(\gamma - 1)(1 - \epsilon - 1 + \epsilon\gamma) }
                {\gamma(\gamma-1)}\right)(z_0-z)^{-1} \\
&= -\epsilon\left(\frac{\gamma-1}{\gamma}\right)(z_0-z)^{-1}.
\end{split}
\end{equation}
So the entropy gradient ends up having an order $\epsilon$ change.
Further, integrating this gives us an expression for the entropy,
\begin{equation}
S/C_P = \epsilon\frac{\gamma-1}{\gamma}\ln(z_0-z).
\end{equation}
and since $z_0 = L + 1$, the change in entropy over the domain is:
\begin{equation}
\Delta S/C_P = \epsilon\frac{\gamma-1}{\gamma}(\ln(1) - \ln(L + 1)) 
= \epsilon\frac{\gamma-1}{\gamma}\ln(L + 1).
\end{equation}

Since I assume hydrostatic equilibrium for my polytrope, $\partial_zP = - \rho g$
(for gravity pointing down), and I can find that
\begin{equation}
\begin{split}
\partial_z P &= - \rho g \\
P_{00}(m+1)(z_0-z)^{m} &= \rho_{00}(z_0-z)^m g \\
P_{00}(n+1) &= \rho_{00}g \\
g &= \frac{P_{00}}{\rho_{00}}(m+1).
\end{split}
\end{equation}

\section{Entropy Equation}
Starting with the entropy version of the thermodynamic identity,
\begin{equation}
    dS = \frac{1}{T}dU + \frac{P}{T}dV,
\end{equation}
I plug in $V = T/P$ ($P = \rho T$, with our non-dimensionalization)
to the second term and find that
a\begin{equation}
\begin{split}
    dS &= C_V d\ln T + \frac{P}{T}\left(\frac{dT}{P} - \frac{TdP}{P^2}\right)\\
    &= C_V d\ln T + d\ln T - d\ln P = C_Pd\ln T - d\ln P.
\end{split}
\end{equation}
I will now utilize the facts that $1 + C_V = C_P$ and $P = \rho T$ to show 
\begin{equation}
    dS = C_Pd\ln \left(\frac{P}{\rho}\right) - d\ln P =
    C_Vd\ln P - C_Pd\ln \rho.
\end{equation}
Dividing through by $C_P$, this becomes
\begin{equation}
    \frac{dS}{C_P} = \frac{1}{\gamma}d\ln P - d\ln\rho.
\end{equation}
This is the general entropy equation that we used above.

\section{Our experiment}
OK, so basically my goal is to characterize the thickness of the thermal boundary
layer at the top of the domain when I run a convective simulation of a
superadiabatic gradient.  In order to do this, I'm doing the following:
\begin{enumerate}
\item Run a simulation past the convective overturn
\item Keep everything in the atmosphere the same (flux through the bottom, background
        state), then \emph{change the thermal diffusivity}.
\item Do 5 experiments like this
\item Characterize the thickness of the boundary layer
\item Using the THICKEST boundary layer from the 5 experiments above, create a
        ``miraculous'' flux function through the domain.  This will probably be
        an erf.  It'll be 0 through most of the domain, and equal to the flux
        throughout the thermal boundary layer at the top.
\item Run all 5 experiments again, with this miraculous flux term.
\item See how the thickness of average downflows changes.
\end{enumerate}

\subsection{Thermal Boundary Layer}
What causes the thermal boundary layer at the top of the domain?  Our boundary
conditions!  We set the top of the domain to have a constant temperature.  In order
to allow for that boundary condition, the solution creates a thermal boundary layer
with a steeper temperature gradient.  This steeper temperature gradient manages
to carry the flux through the top of the domain, whereas the convective flux carries
the flux through the majority of the domain.

So basically, this exists because of our boundary condition, and we want to
characterize it.

The boundary layer will be a function of a few things.  The two I think are most
interesting are thermal diffusivity and flux through the domain.  So let's talk
about how to change those two.

\subsubsection{Setting the flux through the domain}
Our bottom boundary condition sets the flux through the domain.  We set the
temperature gradient to zero at the bottom of the domain, and since the convective
motions can't carry flux there (we set a no-slip boundary condition), the radiative
flux enforced by the temperature gradient supplies all the flux through our domain.

\subsubsection{Changing the flux and thermal diffusivity}
We have two real knobs in our system:
\begin{equation}
\text{Ra} = \frac{gL^3 |\Delta s|}{\nu\chi}
\end{equation}
and
\begin{equation}
\text{Pr} = \frac{\nu}{\chi},
\end{equation}
the Rayleigh and Prandtl number.  In order to keep our system comparable (the same
amount of driving) we want to keep the Rayleigh number constant.  In order to do so,
we set $\nu\chi =$ const.  

So what we do is set up a REFERENCE atmosphere, with the Rayleigh number we're
interested in and an initial Prandtl number, Pr$_0$.  Then, we switch to a new
Prandtl number, Pr$_1$, which sets the \emph{new} (increased or decreased)
diffusivities.

In other words, we solve:
\begin{equation}
\begin{split}
\nu_0 &= \sqrt{\frac{\text{Pr}_0 g L^3 |\Delta s|}{\text{Ra}}} \\
\chi_0 &= \frac{\nu_0}{\text{Pr}_0} \\
\nu_1 &= \sqrt{\frac{\text{Pr}_1}{\text{Pr}_0}}\nu_0 \\
\chi_1 &= \sqrt{\frac{\text{Pr}_0}{\text{Pr}_1}}\chi_0.
\end{split}
\end{equation}
So decreasing our Prandtl number will \emph{increase} our \textbf{thermal diffusivity}
and \emph{decrease} our \textbf{viscosity} (kinematic diffusivity?).

since our boundary condition at the bottom is (technically)
\begin{equation}
\frac{\partial T_1}{\partial z} = 0,
\end{equation}
and since the \emph{flux} through the bottom boundary is $\chi_0\partial_z T$,
and since we set $\partial_z T_0 = -1$ in our polytropes, we actually need to change
our bottom boundary condition in order to ensure that the flux through our system is
the same as the flux through our reference atmosphere.   Due to our definition of
the slope of the temperature of our polytrope, the flux through our reference system
is $-\chi_0$.  Thus, in order to maintain this in our new system, we need to change
the flux of our initial polytrope ($-\chi_1$) by changing the temperature gradient
boundary condition from $\partial_z T = -1$ to $\partial_z T = -(\chi_0/\chi_1)
= -\sqrt{\text{Pr}_1/\text{Pr}_0}$.

So...that's pretty much the idea.  I've also discovered that it's easier to start
at low diffusivity.  Then, once that gets past the transient, I can restart that
solution with different diffusivities (rather than starting over again).  It seems
like these sims (when well resolved) take about ~24 hours to get through the transient
due to either the high flux or the high thermal diffusivity creating more complex
thermal structures which throttle the timestepper.


\bibliographystyle{apj}
\bibliography{../biblio.bib}
\end{document}
