\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{Week 4 -- comps 2}
\cfoot{\thepage}
\rfoot{Feb. 21, 2016}

\newcommand{\FT}{\ensuremath{\text{FT}}}

\begin{document}
\section{Mon, Feb. 15, 2016}
Met with Dave today.  He gave me a better means of using ADS, accessible via the link
\texttt{https://ui.adsabs.harvard.edu/}.  We also talked a bit about comps 2 expectations,
one of which is, apparently, a roughly 30 page paper.  Also, I only want to tackle
roughly \emph{one} paper worth of material for comps 2, so I shouldn't even try to 
take on two.  BUT it's OK to talk about future work.  Anyways.  On to some science.

I just read this \cite{brandenburg2005} paper for the second time.  It covers the fact
that we want our convective sims to actually model the Sun, rather than...well,
model convection.  It specifically covers the fact that most simulations with polytropic
indices of roughly 1 don't carry enough of the flux via convection.  This paper goes
down to polytropic indices of about -0.9, which naturally have ``top heavy'' density
tendencies but which have OK pressure profiles.  BUT they also carry almost all of
the flux through convection.  The paper claims that when $F_{rad}$ is big, it starts
affecting the dynamics of the convection.  I'm not sure how it says this or what
exactly the differences were.  Also, is it the \emph{fraction} of the flux that changes
grad S in the convection zone or is it the fact that Axel's running less flux through
the system?  It'd be worth sitting down with this one and really going through the
math.

Next project: Read the relevant parts of the Living Review on Solar Surface Convection
(\cite{nordlund2009}).  It also might not hurt to take a look at the relevant parts of
the review on large-scale dynamics of the convection zone and tachocline
(\cite{miesch2005}).

\section{Tue, Feb. 16, 2016}
Oh, I'm also going to take a look at \cite{hossain1993} on BCs in compressible
convection.

\section{Fri, Feb. 19, 2016: Non-dimensionalizing equations}
Yup, it's that time.  Time for me to figure out how to non-dimensionalize these
equations and really understand how our ``knobs'' on our polytrope equation actually
change the dynamics of our simulations.  It's important for me to note that I'm
NOT done with this process as I type this.  There are still parts that I definitely
don't understand, and that I need to understand better.  But, here goes.

\subsection{Momentum equation}
Our good old friend has the rough form:
$$
\rho \frac{D\bm{u}}{Dt} = -\nabla P + \rho \bm{g} + \mu\nabla^2 \bm{u}.
$$
Yes, I've simplified the viscous term, but it \emph{dimensionally} works out the
same as if we had the stress tensor.  I'm interested in the dimensionality of the
various terms in the equations.

I want to make the pressure term look nicer, so I'll point out that
$$
\nabla P = \frac{\partial P}{\partial \rho}\nabla\rho = c_s^2\nabla\rho.
$$

Dividing the momentum equation by $\rho$, I find
$$
\frac{D\bm{u}}{Dt} = - c_s^2 \nabla\ln\rho + \bm{g} + \nu \nabla^2\bm{u}.
$$

To make things nicer, I'll assume $\nu =$ constant.  I now want to non-dimensionalize
my equation such that
\begin{equation*}
\begin{split}
t &= t_0 t' \\
\nabla &= \nabla'/\ell \\
\bm{u} &= u_0 \bm{u}' \\
u_0 &= \ell / t_0.
\end{split}
\end{equation*}
With this,
$$
\frac{u_0}{t}\frac{D\bm{u}'}{Dt'} = -\frac{c_s^2}{\ell}\nabla'\ln\rho + \bm{g}
+ \frac{\nu u_0}{\ell^2}\nabla'^2\bm{u}.
$$
I now divide the equation by $u_0/t$ such that the LHS (the advective term) is order
1,
$$
D_{t'}\bm{u}' = \frac{c_s^2}{u_0^2}\nabla'\ln\rho + \frac{t^2}{\ell} g
+ \frac{\nu t_0}{\ell^2}\nabla'^2\bm{u}'
$$

\emph{For convenience, I will now drop the primes on my various terms, as I know
that everything is now non-dimensional (including rho, because grad log)}.  
I also want to point out something cool, here.  The pressure term now looks like
$$
\frac{c_s^2}{u_0^2}\nabla\ln\rho = \text{Ma}^{-2}\nabla\ln\rho,
$$
where the Mach number is defined as $\text{Ma} = u_0 / c_s$, and we immediately see that
\textbf{ the pressure term is related to the advective term by the mach number squared}:
$$
\boxed{
\frac{\bm{u}\cdot\nabla\bm{u}}{\nabla P / \rho} \approx \text{Ma}^2
}
$$
This means that \textbf{low Mach number flows have dominant pressure gradients.}

Now I think I'm going to diverge from what Ben did when he non-dimensionalized our
polytropes, but I think it'll be somewhat illustrative for me.  I will define
$\ell = L$, the the length of the domain and $t_0 = \tau_{th}$, the thermal diffusion
time across the domain.  Thus, I can easily define
$$
\chi = \ell^2 / t_0 = L^2 / \tau_{th}.
$$
This means that the term in front of the viscous term is
$$
\text{Pr} = \frac{\nu}{\chi},
$$
and this means that \textbf{the viscous term is stronger than the advective term
by a factor of the Prandtl number}, or
$$
\boxed{
\frac{\bm{u}\cdot\nabla\bm{u}}{\nabla^2\bm{u}} = \text{Pr}^{-1}
}
$$
So what are we left with?
$$
D_{t}\bm{u} = \text{Ma}^{-2}\nabla\ln\rho + \frac{t^2}{\ell}g + \text{Pr}\nabla^2\bm{u}.
$$

Ok, so what's that gross middle term? Let's manipulate it.
$$
\frac{t^2 g}{\ell} = \frac{\ell^3 t^2 g}{\ell^4} = \frac{\ell^3 g}{\chi^2} =
\frac{\nu}{\chi} \frac{\ell^3 g}{\chi\nu} = \text{Pr}\frac{L^3 g |\Delta S|}{\chi\nu |\Delta S|}
= \frac{\text{Pr Ra}}{|\Delta S|}.
$$
And for our superadiabatic polytrope that we've been working with,
$$
|\Delta S| = \epsilon\frac{\gamma-1}{\gamma}\ln(L + 1),
$$
where
$$
L = \text{exp}\left(\frac{n_{\rho}}{m_{ad} - \epsilon}\right) - 1.
$$
so
$$
|\Delta S| = \frac{\epsilon}{n_{\rho}} \frac{\gamma-1}{\gamma} (m_{ad} - \epsilon)
= \frac{\epsilon}{n_\rho} \left(\gamma^{-1} - \epsilon\frac{\gamma-1}{\gamma}\right).
$$
where $\epsilon$ is the level of superadiabaticity and $n_\rho$ is the number of
density scale heights in the polytropic box. Generally, epsilon is small, so this term
will just be $O(\epsilon)$.  In the end, this brings our momentum equation to
\begin{equation}
D_t\bm{u} = \text{Ma}^{-2}\nabla\ln\rho + \frac{n_\rho}{\epsilon}
\frac{\gamma(\text{Ra Pr})}{(1 - \epsilon(\gamma-1))} + \text{Pr}\nabla^2\bm{u}.
\end{equation}
So I can see that
$$
\boxed{
\frac{\bm{u}\cdot\nabla\bm{u}}{\bm{g}} \approx \frac{\epsilon}{n_\rho (\text{Pr Ra})}
}.
$$
So raising $\epsilon$ makes advection more important than buoyancy.  Raising the
number of density scale heights, Pr, and Ra make buoyancy more important than advection.


I've compared everything to $\bm{u}\cdot\nabla\bm{u}$.  Maybe that wasn't wise.
There's still one thing left for me to do: \emph{put the Mach number in terms of
the polytrope}.  I haven't done that yet.  I'll get around to it eventually.

My intuition is saying that $u \propto \epsilon/(n_\rho[\text{Pr Ra}])$, aka the
above term.  But I have to think about this....  Regardless, sound speed is roughly
constant at small epsilon:
$$
c_s^2 = \frac{\partial P}{\partial\rho} = \frac{m+1}{m} \frac{1}{T} = 
\frac{m_{ad} + 1 - \epsilon}{m_{ad} - \epsilon} \frac{1}{T} =
\frac{m_{ad} + 1}{m_{ad}}\frac{1}{T}.
$$



\section{Sat, Feb. 20: Momentum equation non-dimensionalization, take 2}
Ok, so I sent an e-mail to Ben.  He said:
\begin{enumerate}
\item $\ell$ in our non-dimensionalization of the polytrope we're running is
the temperature gradient length scale (how far you have to travel for the temp.
gradient to change by 1 unit, in our non-dimensional units)
\item $t_0$ is the isothermal sound crossing time at the top of the domain.  So
basically, how long it takes a wave to travel horizontally for one of the $\ell$
units.
\end{enumerate}
So given these definitions, I think it's pretty OK for me to make the assumption I
made above, $\chi = \ell^2/t_0$.  I'm going to return to the momentum equation in 
the early stages of non-dimensionalization:
$$
\frac{u_0}{t}\frac{D\bm{u}'}{Dt'} + \frac{P_0}{\rho_0 \ell}\frac{\nabla P'}{\rho'} = \bm{g} +
\frac{u_0}{\ell^2}\nabla^2\bm{u}'.
$$
I'm going to say that that nondimensional pressure is basically a ram pressure,
$P_0 = \rho_0 u_0^2$, or $P_0 / \rho_0 \ell = u_0^2/\ell = u_0 / t$.  This means
that pressure has the same units as the advective derivative.  I'm going to divide
everything by $\nu u_0 / \ell^2 = \nu u_0 / \chi t_0$, such that
$$
\frac{1}{\text{Pr}}\left(\frac{\partial \bm{u}}{\partial t} +
\bm{u}\cdot\nabla\bm{u} + \frac{\nabla P}{\rho} \right) =
\frac{\chi t_0}{\nu u_0}\bm{g} + \nabla^2 \bm{u}.
$$

Once again, that gravity term is pesky, so I'm gonna throw math at it:
$$
\frac{\chi t_0}{\nu u_0}\bm{g} = \frac{\bm{g}\ell^3}{\nu\chi}.
$$
And, for our system, since $\ell = 1$, $L_{cz} = \ell L_{cz}$,
$$
\frac{\bm{g}\ell^3}{\nu\chi} = \frac{1}{L_{cz}^3 |\Delta S/C_P|}
\frac{\bm{g} L_{cz}^3 |\Delta S / C_P | }{\chi \nu} = 
\frac{Ra}{\epsilon}\frac{\gamma}{\gamma-1} [ L_{cz}^3 \ln(L_{cz} + 1)]^{-1} =
\frac{Ra}{n_\rho \epsilon} \frac{m \gamma}{\gamma - 1} \left(e^{n_\rho/m}-1\right)^{-3}
$$

So the momentum equation is, overall, \emph{in terms of our input parameters},
$$
\frac{1}{\text{Pr}}\left(\frac{\partial \bm{u}}{\partial t} +
\bm{u}\cdot\nabla\bm{u} + \frac{\nabla P}{\rho} \right) =
\frac{\text{Ra}}{n_\rho \epsilon}\frac{\gamma}{\gamma-1}(m_{ad}-\epsilon) 
\left(e^{n_\rho/(m_{ad}-\epsilon)}-1\right)^{-3}
+ \nabla^2 \bm{u}.
$$
So The Prandtl number controls the fraction of the LHS to the RHS.  The Rayleigh
number and epsilon have ``equal and opposite'' effects on the power of buoyancy (for
low epsilon).  The number of density scale heights comesi in both as an inverse
and as an exponential decay once it gets large.

Did I do this right?  Maybe!  But it's something.  Need to talk to Ben about it.  Still
not sure how that Rayleigh number is derived, but I am sure that it shows up
everywhere (in some at least very similar form) in all of the compressible convection
papers I've read recently.

\section{Mon, Feb. 22: Non-dimensionalizing Temperature Equation}
Ok, the temperature equation FEELS like it should be easier than the momentum
equation, but I'm struggling with it a \emph{lot}.  Not in terms of getting
it non-dimensionalized, but in terms of getting it in terms of control parameters.

The temperature equation looks like:
\begin{equation}
\partial_t T + \bm{u}\cdot \nabla T + (\gamma-1)T\nabla\cdot\bm{u} =
\frac{\kappa}{\rho C_V}\nabla^2 T + \frac{\nu}{C_V} \nabla^2 u.
\end{equation}
Note: I \emph{know}, I \emph{know}.  The last two terms don't quite look like that.
But they \emph{dimensionally} do actually look like that.  So I have something that
looks like:
$$
\frac{T_0}{t_0}\left(\partial_t T + \bm{u}\cdot \nabla T + (\gamma-1)T\nabla\cdot\bm{u}\right)
= \frac{T_0}{\rho_0\ell^2}\frac{\kappa}{\rho C_V}\nabla^2 T +
\frac{\nu u_0}{\ell^2} \frac{1}{C_V}\nabla^2 u.
$$
Multiplying by $t_0/T_0$, the LHS becomes order 1.  The RHS is what I'm going to (try)
to examine and get in term of imput parameters.  The first term has something like
$t_0 \kappa / \rho_0 \ell^2$ in front.  But $\ell^2/t_0 = \chi = \kappa$, so really
the term in front of $\nabla^2 T$ is just $\rho_0^{-1}$.  

Moral of the story, I need a clever way to define $\rho_0$.  I know that 
$P_0 = \rho_0 u_0^2$ and that $P_0 = \rho_0 T_0$.  Thus, $\rho_0 = P_0 / u_0^2
= \rho_0 T_0/u_0^2$.  This means that $T_0 = u_0^2 = \chi^2 / \ell^2$.  
Also, by my polytrope definition, $\rho_0 = T_0^m$, so $\rho_0 = (\chi^2/\ell^2)^m$.

Ok, we're getting useful.  $\chi$ is an input parameter. $\ell$ is a non-dimensionalization
parameter, and it's \emph{1} in our current units.  So this term is proportional to
$\chi^{2m} \approx \chi^3$ for small $\epsilon$.  More precisely,
$$
\chi^{2m} = \left( \frac{g(\epsilon) L(n_\rho, \epsilon)^3 |\Delta S(\epsilon, L)/c_p|}
{\text{Pr Ra}}\right)^m.
$$
For my case, $m \approx 1.5$, and for the following parameters:
\begin{equation*}
\begin{split}
\text{Pr} &= 0.5 \\
\text{Ra} &= 10^5 \\
\epsilon &= 10^{-4} \\
n_\rho &= 3.5 \\
m_{ad} &= 1.5,
\end{split}
\end{equation*}
I find this term to be
$$
\chi^{2m} \approx \left(
\frac{(m+1)(e^{n_\rho/m}-1)^3\epsilon(\gamma-1)(n_\rho/m)}
{\text{Ra Pr }\gamma}\right)^m
\approx 7 \times 10^{-9} \approx 10^{-10}.
$$

So this means that the radiative diffusion term is about \emph{ten orders of magnitude
larger than the advection of temperature or work}.  Dang.  That's HUGE.  I either
did something wrong with my non-dimensionalization or for these parameters, this term
is DOMINANT in determining how temperature changes.

Just for the sake of doing it, the viscous term has the form $\nu/C_V$, which is
$Pr \chi C_V$, which is on the order of about $10^{-3}-10^{-4}$ for these parameters.
Which is to say: radiative diffusion wins over advection by about 10 orders of magnitude,
and over viscous heating by 13 to 14 orders of magnitude.

\bibliographystyle{plain}
\bibliography{../../biblio.bib}
\end{document}
\grid
