\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{Week 2 -- comps 2}
\cfoot{\thepage}
\rfoot{Feb. 7, 2016}

\newcommand{\FT}{\ensuremath{\text{FT}}}

\begin{document}
\section{New experiment from Ben: run a simulation until it \emph{fully} equilibrates}
As in, get to the point in a simulation where fluxes (averaged over a sensible number
of timesteps) basically don't change.  This probably means running over a few
\emph{thermal} times, not just the few buoyancy times we often run for complex
simulations.  

So I'm going to run a \emph{long} polytropic simulation at the following parameters
\begin{equation*}
\begin{split}
\text{Pr} &= 0.5 \\
\text{Ra} &= 10^5 \\
n_{\rho} &= 3.5 \text{ (density scale heights)} \\
\epsilon &= 10^{-4} \text{ (Mach number roughly }10^{-2}\text{)}\\
n_z &= 256 \\
n_x &= 1024.
\end{split}
\end{equation*}
Then I need to ask myself a few questions:
\begin{enumerate}
\item How long do I need to average over in order to get a sensible average flux? In
other words, how many timesteps do I need to average over in order to make the
relative error (quantified by the $\ell_2$ norm):
$$
\frac{||x - \hat{x}||}{||\hat{x}||}.
$$
So I suppose that my $\hat{x}$ is going to be the mean that I've calculated so far,
and $x$ is the profile that I'm adding to the mean.  So let's say I'm averaging over
500 timesteps, and I've gotten through 300 of those so far.  The average of those will
be $\hat{x}$, and the $x$ will be the 301st timestep.  I'm trying to see how well the
average quantifies the current flux, \emph{and how well the flux converges}.  If
it changes wildly between each time step, the $\ell_2$ norm won't actually converge
on a small number.
\item The fundamental question: Does our solution converge?  What we're testing here
is \emph{not} whether the total energy of the system changes, but rather the
nonlinear transport in the system converges to a ``known.''  We also want to test
the standard deviation of the flux profiles at each timestep (should, theoretically,
go down like $\sqrt{N}$, because we're calculating a sample mean).
\end{enumerate}

Ben says these are the fluxes I need to track:
\begin{equation*}
\begin{split}
\text{Enthalpy Flux} &= w[\rho T (1 + C_V)] = w[\text{Internal Energy + Pressure}] \\
\text{KE Flux} &= w[(1/2)\rho|\vec{u}|^2] \\
\text{PE Flux} &= w\rho\phi \\
\text{Kappa Flux} &= -\rho\chi\frac{\partial T}{\partial z}.
\end{split}
\end{equation*}
In the PE flux, $\phi$ is the gravitational potential, defined by $g = -\nabla\phi$.
Also, I probably only want to plot the fluctuating part of kappa flux.  Not the total,
as that includes a much larger background term from the polytrope's gradient.

One last note: two numbers are really important in our simulations.  These are the
Reynolds number and the Peclet number.  They're defined such that:
\begin{equation*}
\begin{split}
\text{Re} &= \frac{U L}{\nu} \\
\text{Pe} &= \frac{U L}{\chi} \\
\text{Pe} &= \text{Pr Pe}.
\end{split}
\end{equation*}
If either of these numbers is less than 1, our system should be stable to convective
motions.  This is because diffusive transport is more effective than advective
transport, and so our solution should be....well, boring.  Fortunately, for
the parameters specified above 


\section{Derivation of fluxes}
OK, so now let's actually mathematically prove that those fluxes Ben gave me come
out of our equations.  I don't want to just accept it -- I want to prove it to myself.

We'll examine the momentum equation and the energy equation.  In order to turn the
momentum equation into a (scalar) energy equation, we will \emph{dot the entire
equation with the velocity vector}.  Thus, we'll examine:
\begin{align}
\bm{u}\cdot\left(
\rho(\partial_t\bm{u} + \bm{u}\cdot\nabla\bm{u}) + \nabla P \right) =
\bm{u}\cdot\left( \bm{g}\rho - \nabla\cdot\bm{\Pi}\right)
&\text{ (KE)}, \label{momentum_full}\\
\rho C_V \partial_t T + \rho C_V \bm{u} \cdot \nabla T + 
\rho C_V (\gamma-1)T\nabla\cdot\bm{u} =
\nabla\cdot(\rho\chi\nabla T) - \Pi_{ij}\partial_{x_i}u_j
&\text{ (energy)} \label{energy_full},
\end{align}

The stress tensor is defined as
\begin{equation*}
\Pi_{ij} = -\mu\left(\partial_{x_i}u_j + \partial_{x_j}u_i 
- \frac{2}{3}\delta_{ij}\nabla\cdot\bm{u}\right)\label{stress_tensor},
\end{equation*}
where $\delta_{ij}$ is the Kronecker delta and $\mu$ is the dynamic viscosity.
I will solve for the fully
compressible equations in \emph{two dimensions}, where my velocity vector takes
the form $\bm{u} = u\hat x + w\hat z$, so my tensor takes the form
\begin{equation*}
\bm{\Pi} = -\mu
\begin{pmatrix}
\frac{4}{3}\partial_x u - \frac{2}{3}\partial_z w &
\partial_z u + \partial_x w \\
\partial_z u + \partial_x w &
\frac{4}{3}\partial_z w - \frac{2}{3}\partial_x u
\end{pmatrix}. \label{stress_tensor_full}
\end{equation*}

My goal is to put my equations into conservation form:
\begin{equation}
\partial_t(\text{quantity}) + \nabla\cdot(\text{flux of quantity}) 
= \text{sources - sinks}.
\end{equation}

As a note, I'm going to be relying \emph{heavily} upon the NRL plasma formulary
and all of its vector identities.

\subsection{Kinetic energy equation}
To start, I'm going to move everything in my momentum equation to the LHS,
\begin{equation}
\bm{u}\cdot\left(
\rho(\partial_t\bm{u} + \bm{u}\cdot\nabla\bm{u}) + \nabla P
+ \rho\nabla\phi + \nabla\cdot\bm{\Pi}\right) = 0.
\end{equation}
where I've replaced $g$ with $-\nabla\phi$. Let's do this step by step.

\subsubsection{$\bm{u}\cdot(\rho\partial_t\bm{u})$}
Just using the chain rule, this is
$$
\boxed{
\bm{u}\cdot(\rho\partial_t\bm{u}) = 
\frac{1}{2}\left(\partial_t[\rho |u|^2] - |u|^2\partial_t\rho\right)  }.
$$

\subsubsection{$\bm{u}\cdot\rho(\bm{u}\cdot\nabla)\bm{u}$}
Ok, before we dot this, let's just examine $(\bm{u}\cdot\nabla\bm{u})$:
$$
(\bm{u}\cdot\nabla)\bm{u} = \frac{1}{2}\nabla(\bm{u}\cdot\bm{u}) - \bm{u}\times(\nabla\times\bm{u}).
$$
Multiplying by $\rho$ and then dotting with $\bm{u}$, the second term disappears
(it is definitionally orthogonal to $\bm{u}$., so
$$
\bm{u}\cdot\rho(\bm{u}\cdot\nabla)\bm{u} = \frac{1}{2}\bm{u}\cdot\rho\nabla(|u|^2) = 
\frac{1}{2}\bm{u}\cdot(\nabla(\rho|u|^2) - |u|^2\nabla\rho) =
\nabla\cdot(\frac{1}{2}\rho|u|^2\bm{u}) - 
\frac{1}{2}\rho|u|^2\nabla\cdot\bm{u} - \frac{1}{2}|u|^2\bm{u}\cdot\nabla\rho.
$$
and thanks to the continuity equation,
$$
\partial_t\rho = -\rho\nabla\cdot\bm{u} - \bm{u}\cdot\nabla\rho.
$$
Thus,
$$
\boxed{
\bm{u}\cdot\rho(\bm{u}\cdot\nabla)\bm{u} = \nabla\cdot(\frac{1}{2}\rho|u|^2\bm{u})
+ \frac{1}{2}|u|^2\partial_t\rho    }.
$$

\subsubsection{$\bm{u}\cdot\nabla P$}
This is just
$$
\boxed{
\bm{u}\cdot\nabla P = \nabla\cdot(\bm{u}P) - P\nabla\cdot\bm{u}
} .
$$

\subsubsection{$\bm{u}\cdot(\rho\nabla\phi)$}
There are two steps to this:
$$
\rho\nabla\phi = \nabla(\rho\phi) - \phi\nabla(\rho)
$$
And then
$$
\boxed{
\bm{u}\cdot(\nabla(\rho\phi) - \phi\nabla(\rho)) = 
\nabla\cdot(\rho\phi\bm{u}) - \rho\phi\nabla\cdot\bm{u} - \bm{u}\cdot(\phi\nabla\rho)
} .
$$

\subsubsection{$\bm{u}\cdot(\nabla\cdot\bm{\Pi})$}
The first thing I'm going to do is define the two rows of the stress tensor:
\begin{equation}
\begin{split}
\bm{\Pi}_1 &= \begin{pmatrix}\Pi_{11} \\ \Pi_{21}\end{pmatrix} \\
\bm{\Pi}_2 &= \begin{pmatrix}\Pi_{12} \\ \Pi_{22}\end{pmatrix}.
\end{split}
\end{equation}

Further, 
$$
\nabla\cdot\bm{\Pi} = \nabla\cdot\bm{\Pi}_1\hat{x} + \nabla\cdot\bm{\Pi}_2\hat{z}.
$$
As a result, using some vector identities, and noting that $u = u_x$ and $w = u_z$,
$$
\boxed{
\bm{u} \cdot (\nabla\cdot\bm{\Pi}) = 
\nabla\cdot(u\bm{\Pi}_1 + w\bm{\Pi}_2) - \bm{\Pi}_1\cdot\nabla u - \bm{\Pi}_2\cdot\nabla w
}.
$$

\subsubsection{Putting it all together}
Putting all of these terms together, we find
\begin{equation}
\begin{split}
\partial_t\left(\frac{\rho|u|^2}{2}\right) +
\nabla\cdot\left(\bm{u}\frac{\rho|u|^2}{2} + \bm{u}P + \bm{u}\rho\phi + u\bm{\Pi}_1 + w\bm{\Pi}_2 \right)
= \\
P\nabla\cdot\bm{u} + \rho\phi\nabla\cdot\bm{u} + \bm{u}\cdot(\phi\nabla\rho)
+ \bm{\Pi}_1\cdot\nabla u + \bm{\Pi}_2\cdot\nabla w.
\end{split}
\end{equation}

The terms inside of the divergence are flux terms.  Since we only care about the
divergences going into and out of our system (top and bottom), we're only
worried about the $\hat{z}$ component of the divergences.  Here I've derived the
KE flux, the PE flux, and part of the enthalpy flux.  Also, there's an extra term
here that Ben didn't tell me to track, but I'll track it: the viscous flux.  This
term looks like in the $\hat{z}$:
$$
u\Pi_{21} + w\Pi_{22} =
-\mu\left( u[\partial_z u + \partial_x w] + w\left[\frac{4}{3}\partial_z w - \frac{2}{3}\partial_x u\right]\right).
$$
I want to note that (in my current dedalus sims) I'm tracking this quantity, but
with a \textbf{POSITIVE $\nu$} in front, not a $-\mu$.  I'll have to account for that
when I plot, since $\nu = \mu/\rho$.

\subsubsection{Dealing with the slop}
There's a lot of terms on the RHS.  They probably disappear when we horizontally
average or take an integral over the whole volume of the domain or something, but
I'm not sure.  That's something I should certainly check.


\subsection{Internal Energy Equation}
This time I'm going to move everything to the RHS except for the viscous term, because
I honestly have no idea what to do with that term.  So we're looking at:
\begin{equation}
\rho C_V \partial_t T + \rho C_V \bm{u} \cdot \nabla T + 
\rho C_V (\gamma-1)T\nabla\cdot\bm{u} - \nabla\cdot(\rho\chi\nabla T) = 
- \Pi_{ij}\partial_{x_i}u_j
\end{equation}
Let's break up terms and look at them, again.

\subsubsection{$\rho C_V \partial_t T$}
This is another chain rule term,
$$
\boxed{
\rho C_V \partial_t T = C_V \partial_t (\rho T) - C_V T\partial_t \rho }.
$$

\subsubsection{$\rho C_V \bm{u} \cdot \nabla T$}
This term is
$$
\rho C_V \bm{u} \cdot \nabla T = C_V\left(\nabla\cdot(\rho\bm{u}T) - T\nabla\cdot(\rho\bm{u})\right)
= C_V\left(\nabla\cdot(\rho\bm{u}T) - T[\rho\nabla\cdot \bm{u} + \bm{u}\cdot\nabla\rho]\right)
$$

And now I can just add this term to its term next door, $\rho C_V (\gamma-1)T\nabla\cdot\bm{u}$,
which comes out to
$$
\rho C_V \bm{u} \cdot \nabla T + \rho C_V (\gamma-1)T\nabla\cdot\bm{u} =
C_V\left(\nabla\cdot(\rho\bm{u}T) + (\gamma-2)T\rho\nabla\cdot \bm{u} - T\bm{u}\cdot\nabla\rho\right)
$$

And, applying continuity, $\rho\nabla\cdot\bm{u} = -\partial_t\rho - \bm{u}\cdot\nabla\rho$,
$$
\boxed{
\rho C_V \bm{u} \cdot \nabla T + \rho C_V (\gamma-1)T\nabla\cdot\bm{u} =
C_V\left(\nabla\cdot(\rho\bm{u}T) - (\gamma-2)T\partial_t\rho - (\gamma-1)T\bm{u}\cdot\nabla\rho\right) }.
$$

\subsubsection{$\nabla\cdot(\rho\chi\nabla T)$}
This is already in flux form!

\subsubsection{Overall equation (without the viscous part figured out)}
Our energy equation looks like
\begin{equation}
\partial_t(C_V\rho T) + \nabla\cdot\left(C_V\rho\bm{u}T - \rho\chi\nabla T \right)
= C_V(\gamma-1)T(\partial_t\rho + \bm{u}\cdot\nabla\rho) - \Pi_{ij}\partial_{x_i}u_j
\end{equation}
Note that our constants work out nicer once we put the first three terms together, so
un-doing my continuity equation nonsense, I write
\begin{equation}
\partial_t(C_V\rho T) + \nabla\cdot\left(C_V\rho\bm{u}T - \rho\chi\nabla T \right)
= C_V(\gamma-1)\rho T\nabla\cdot\bm{u} - \Pi_{ij}\partial_{x_i}u_j
\end{equation}
Here I've derived our IE flux, which is the other part of the enthalpy flux, and
the ``kappa'' flux.

\section{Adding our two equations toghether}
If I add all of this work so far, I find that 
\begin{equation}
\begin{split}
\partial_t\left(\frac{\rho|u|^2}{2} + C_V\rho T\right) +
\nabla\cdot\left(\bm{u}\frac{\rho|u|^2}{2} + \bm{u}P + \bm{u}\rho\phi + u\bm{\Pi}_1 + 
w\bm{\Pi}_2 + C_V\rho\bm{u}T - \rho\chi\nabla T \right)
= \\
P\nabla\cdot\bm{u} + \rho\phi\nabla\cdot\bm{u} + \bm{u}\cdot(\phi\nabla\rho)
+ \bm{\Pi}_1\cdot\nabla u + \bm{\Pi}_2\cdot\nabla w + C_V(\gamma-1)\rho T\nabla\cdot\bm{u} 
- \Pi_{ij}\partial_{x_i}u_j \\
= (P + \rho\phi + C_V(\gamma-1)\rho T)\nabla\cdot\bm{u} + \bm{u}\cdot(\phi\nabla\rho)
+ \bm{\Pi}_1\cdot\nabla u + \bm{\Pi}_2\cdot\nabla w - \Pi_{ij}\partial_{x_i}u_j
\end{split}
\end{equation}

\subsection{Examining the viscous terms}
So in the total energy equation, this comes down to
\begin{equation*}
\begin{split}
\bm{\Pi}_1\cdot\nabla u + \bm{\Pi}_2\cdot\nabla w - \Pi_{ij}\partial_{x_i}u_j = \\
\Pi_{11}\partial_x u + \Pi_{21}\partial_z u + \Pi_{12}\partial_x w + \Pi_{22}\partial_z w
- \Pi_{11}\partial_x u - \Pi_{21}\partial_z u - \Pi_{12}\partial_x w - \Pi_{22}\partial_z w \\
\boxed{ = 0}.
\end{split}
\end{equation*}
The viscous terms in the KE equation cancels out with the viscous term in the IE
equation.

\subsection{The energy equation with viscosity removed (legitimately)}
\begin{equation*}
\begin{split}
\partial_t\left(\frac{\rho|u|^2}{2} + C_V\rho T\right) +
\nabla\cdot\left(\bm{u}\frac{\rho|u|^2}{2} + \bm{u}P + \bm{u}\rho\phi + u\bm{\Pi}_1 + 
w\bm{\Pi}_2 + C_V\rho\bm{u}T - \rho\chi\nabla T \right)
= \\
(P + \rho\phi + C_V(\gamma-1)\rho T)\nabla\cdot\bm{u} + \bm{u}\cdot(\phi\nabla\rho)
\end{split}
\end{equation*}
If we did an integral over our box, I'm pretty sure that the $\nabla\cdot\bm{u}$ terms
will disappear (but I need to check with Ben/Mark).  That leaves the awkward term
with the gravitational potential, $\bm{u}\cdot(\phi\nabla\rho)$.  Not sure what
happens there.  Need to think about it later.

Time to actually watch the super bowl.

\bibliographystyle{apj}
\bibliography{../biblio.bib}
\end{document}
\grid
