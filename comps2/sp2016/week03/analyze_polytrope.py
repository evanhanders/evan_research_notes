import numpy as np
import matplotlib.pyplot as plt


def L(n_rho, epsilon, m_ad):
    return np.exp(n_rho/(m_ad - epsilon)) - 1

def delta_S(n_rho, epsilon, m_ad):
    return -(n_rho / (m_ad - epsilon)) * ( (m_ad * (1-gamma) + 1)/gamma + epsilon * (gamma - 1)/gamma )

def g(epsilon, m_ad, kb_over_mu, T00):
    return kb_over_mu * T00 * (m_ad + 1 - epsilon)

def diff_base(n_rho, epsilon, m_ad, kb_over_mu, T00):
    return np.sqrt( g(epsilon, m_ad, kb_over_mu, T00) * L(n_rho, epsilon, m_ad)**3 *\
                np.abs(delta_S(n_rho, epsilon, m_ad)) )

def kappa(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00):
    return diff_base(n_rho, epsilon, m_ad, kb_over_mu, T00)/np.sqrt(Ra*Pr)

def nu(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00):
    return np.sqrt(Pr/Ra) * diff_base(n_rho, epsilon, m_ad, kb_over_mu, T00)


#Vary epsilon
gamma = 5/3
m_ad = 1 / (gamma - 1)
kb_over_mu = 1
T00 = 1
epsilon = np.logspace(-4, 0, 1000)
n_rho = 3.5
Ra = 1e6
Pr = 5e-1

Ls = L(n_rho, epsilon, m_ad)
delta_Ss = delta_S(n_rho, epsilon, m_ad)
gs = g(epsilon, m_ad, kb_over_mu, T00)
kappas = kappa(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00)
nus = nu(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00)

fig = plt.figure(figsize=(20,16))
axis1 = plt.subplot2grid((3,2), (0,0), colspan=2)
axis2 = plt.subplot2grid((3,2), (1,0))
axis3 = plt.subplot2grid((3,2), (1,1))
axis4 = plt.subplot2grid((3,2), (2,0))
axis5 = plt.subplot2grid((3,2), (2,1))

axis1.loglog(epsilon, Ls)
axis2.loglog(epsilon, np.abs(delta_Ss))
axis3.loglog(epsilon, gs)
axis4.loglog(epsilon, kappas)
axis5.loglog(epsilon, nus)
axis1.set_xlabel('Epsilon')
axis2.set_xlabel('Epsilon')
axis3.set_xlabel('Epsilon')
axis4.set_xlabel('Epsilon')
axis5.set_xlabel('Epsilon')
axis1.set_ylabel('Domain depth')
axis2.set_ylabel('Delta Entropy')
axis3.set_ylabel('Gravity')
axis4.set_ylabel('Kappa')
axis5.set_ylabel('Nu')
plt.suptitle('Ra={:1.3e}; Pr={:1.3e}; n_rho={:3f}; eps={:1.3e}-{:1.3e}'\
                .format(Ra, Pr, n_rho, np.min(epsilon), np.max(epsilon)))
plt.savefig('polytrope_vary_eps.png')


#Vary n_rho
epsilon = 1e-2
n_rho = np.linspace(1,10,1000)

Ls = L(n_rho, epsilon, m_ad)
delta_Ss = delta_S(n_rho, epsilon, m_ad)
gs = g(epsilon, m_ad, kb_over_mu, T00)*np.ones(len(Ls))
kappas = kappa(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00)
nus = nu(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00)

fig = plt.figure(figsize=(20,16))
axis1 = plt.subplot2grid((3,2), (0,0), colspan=2)
axis2 = plt.subplot2grid((3,2), (1,0))
axis3 = plt.subplot2grid((3,2), (1,1))
axis4 = plt.subplot2grid((3,2), (2,0))
axis5 = plt.subplot2grid((3,2), (2,1))

axis1.loglog(n_rho, Ls)
axis2.loglog(n_rho, np.abs(delta_Ss))
axis3.loglog(n_rho, gs)
axis4.loglog(n_rho, kappas)
axis5.loglog(n_rho, nus)
axis1.set_xlabel('n_rho')
axis2.set_xlabel('n_rho')
axis3.set_xlabel('n_rho')
axis4.set_xlabel('n_rho')
axis5.set_xlabel('n_rho')
axis1.set_ylabel('Domain depth')
axis2.set_ylabel('Abs(Delta Entropy)')
axis3.set_ylabel('Gravity')
axis4.set_ylabel('Kappa')
axis5.set_ylabel('Nu')
plt.suptitle('Ra={:1.3e}; Pr={:1.3e}; n_rho={:3f}-{:3f}; eps={:1.3e}'\
                .format(Ra, Pr, np.min(n_rho), np.max(n_rho), epsilon))
plt.savefig('polytrope_vary_n_rho.png')

#Vary Ra
n_rho = 3.5
Ra = np.logspace(2,8, 1000)

Ls = L(n_rho, epsilon, m_ad)*np.ones(len(Ra))
delta_Ss = delta_S(n_rho, epsilon, m_ad)*np.ones(len(Ra))
gs = g(epsilon, m_ad, kb_over_mu, T00)*np.ones(len(Ra))
kappas = kappa(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00)
nus = nu(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00)

fig = plt.figure(figsize=(20,16))
axis1 = plt.subplot2grid((3,2), (0,0), colspan=2)
axis2 = plt.subplot2grid((3,2), (1,0))
axis3 = plt.subplot2grid((3,2), (1,1))
axis4 = plt.subplot2grid((3,2), (2,0))
axis5 = plt.subplot2grid((3,2), (2,1))

axis1.loglog(Ra, Ls)
axis2.loglog(Ra, np.abs(delta_Ss))
axis3.loglog(Ra, gs)
axis4.loglog(Ra, kappas)
axis5.loglog(Ra, nus)
axis1.set_xlabel('Ra')
axis2.set_xlabel('Ra')
axis3.set_xlabel('Ra')
axis4.set_xlabel('Ra')
axis5.set_xlabel('Ra')
axis1.set_ylabel('Domain depth')
axis2.set_ylabel('Abs(Delta Entropy)')
axis3.set_ylabel('Gravity')
axis4.set_ylabel('Kappa')
axis5.set_ylabel('Nu')
plt.suptitle('Ra={:1.3e}-{:1.3e}; Pr={:1.3e}; n_rho={:3f}; eps={:1.3e}'\
                .format(np.min(Ra), np.max(Ra), Pr, n_rho, epsilon))
plt.savefig('polytrope_vary_Ra.png')




#Vary Pr
Ra = 1e6
Pr = np.logspace(-3,5, 1000)

Ls = L(n_rho, epsilon, m_ad)*np.ones(len(Pr))
delta_Ss = delta_S(n_rho, epsilon, m_ad)*np.ones(len(Pr))
gs = g(epsilon, m_ad, kb_over_mu, T00)*np.ones(len(Pr))
kappas = kappa(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00)
nus = nu(Ra, Pr, n_rho, epsilon, m_ad, kb_over_mu, T00)

fig = plt.figure(figsize=(20,16))
axis1 = plt.subplot2grid((3,2), (0,0), colspan=2)
axis2 = plt.subplot2grid((3,2), (1,0))
axis3 = plt.subplot2grid((3,2), (1,1))
axis4 = plt.subplot2grid((3,2), (2,0))
axis5 = plt.subplot2grid((3,2), (2,1))

axis1.loglog(Pr, Ls)
axis2.loglog(Pr, np.abs(delta_Ss))
axis3.loglog(Pr, gs)
axis4.loglog(Pr, kappas)
axis5.loglog(Pr, nus)
axis5.set_xlabel('Pr')
axis5.set_xlabel('Pr')
axis5.set_xlabel('Pr')
axis5.set_xlabel('Pr')
axis5.set_xlabel('Pr')
axis1.set_ylabel('Domain depth')
axis2.set_ylabel('Abs(Delta Entropy)')
axis3.set_ylabel('Gravity')
axis4.set_ylabel('Kappa')
axis5.set_ylabel('Nu')
plt.suptitle('Ra={:1.3e}; Pr={:1.3e}-{:1.3e}; n_rho={:3f}; eps={:1.3e}'\
                .format(Ra, np.min(Pr), np.max(Pr), n_rho, epsilon))
plt.savefig('polytrope_vary_Pr.png')


