\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}
\usepackage{cancel}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{Week 5 -- comps 2}
\cfoot{\thepage}
\rfoot{Feb. 28, 2016}

\newcommand{\FT}{\ensuremath{\text{FT}}}
\newcommand{\Div}{\ensuremath{\nabla\cdot}}
\newcommand{\Lap}{\ensuremath{\nabla^2}}

\begin{document}
\section{Epsilon runs}
OK, so Ben and Axel (and Mark?) disagree about the importance of how much of the
total flux convection carries.  Axel thinks its very important that the convection
in the polytrope carry ``all'' of the flux, so that it is very much like the Sun.
Ben disagrees, thinking that the Mach number is most important to get right.  Thus,
sacrificing the amount of flux carried by the convection is OK.  This is partially
due to the conversation Ben \& Mark had a little while back in which they talked about
the effects of the pressure gradient forces near the top boundary layer.

Anyways, the idea is this: \emph{let's test the effects of epsilon}.  I'm running
simulations at the following parameters:
\begin{equation}
\begin{split}
\text{Ra} &= 10^6 \\
\text{Pr} &= 1 \\
\epsilon &= \{10^{-8}, 10^{-6}, 10^{-4}, 10^{-2}, 0.1, 0.5, 1\},
\end{split}
\end{equation}
and seeing how the fluxes look.  I want to:
\begin{enumerate}
\item Characterize the magnitude of the flux to see how it compares to $\epsilon$.  Is
there a simple scaling relation here?
\item Characterize the \emph{shape} of the flux.  Does this change drastically?
Is this very different at low epsilon than it is at high epsilon?
\item Look for other interesting things that happen.
\end{enumerate}
I'm going to try to evolve the systems past the transient, but not really any further.
Further would take too long.  I've already finished 24 hours on janus at all but
the two border values of $\epsilon$ (1, $10^{-8}$).  These required some timestep
changing.  Unfortunately, it's now no longer Saturday, and the Janus queue is
slammed and impenetrable.  Again.

\section{Navier Stokes equations -- revisited}
OK, so I have a better understanding of how are equations are formed and entered
in dedalus now.  Time to look at them again.

\subsection{Continuity equation}
This is just
$$
\partial_t \rho + \nabla\cdot(\rho\bm{u}) = 0
$$
Breaking up the divergence and dividing by $\rho$,
\begin{equation}
\boxed{
\partial_t \ln\rho + \Div\bm{u} + \bm{u}\cdot\nabla\ln\rho = 0
}.
\end{equation}
The nice thing about this equation is that it's \emph{already non-dimensional}.

Note that when we break up our parameters into background (0) and fluctuating (1)
components, we have
$$
\ln \rho = (\ln\rho)_0 + (\ln\rho)_1.
$$
Thus,
$$
\rho_{tot} = \text{exp}\left[(\ln\rho)_0 + (\ln\rho)_1\right]
=  \rho_0 \text{exp}\left[(\ln\rho)_1\right]
$$

\subsection{Momentum equation}
So the key here is my revelation in the buoyance force below (in the review).  We
subtract out hydrostatic equlibrium to see the buoyancy force.  Similarly, in
Polytrope, \emph{we explicitly subtract hydrostatic equilibrium out of our equations}.
The basic momentum equation looks like:
$$
\partial_t \bm{u} + \bm{u}\cdot\nabla\bm{u} 
= -\frac{\nabla P}{\rho} + \bm{g} - \Div \Pi.
$$
Our atmospheric constraints \emph{require that the background be in hydrostatic
equilibrium}.  Thus,
$$
-\frac{\nabla P}{\rho} + \bm{g} = -\nabla T - T \nabla\ln\rho + \bm{g}
= -\nabla T_0 - T_0 \nabla(\ln\rho)_0 + \bm{g} - \nabla T_1
- T_0 \nabla(\ln\rho)_1 - T_1 \nabla(\ln\rho)_0 - T_1 \nabla(\ln\rho)_1.
$$
And the first three terms go away due to hydrostatic equilibrium:
$$
\nabla T_0 + T_0 \nabla(\ln\rho)_0 - \bm{g} = 0.
$$
So the momentum equation that we're solving is \emph{really}
\begin{equation}
\boxed{
\partial_t \bm{u} + \bm{u}\cdot\nabla\bm{u} =
- \nabla T_1 - T_0 \nabla(\ln\rho)_1 - T_1 \nabla(\ln\rho)_0 - T_1 \nabla(\ln\rho)_1
- \Div \Pi
}.
\end{equation}
So now I can see how that buoyancy term is expanded in our equations, and \emph{this
is what I should try to figure out the Ra on}.  Also, since this is what we have
implemented in Dedalus, we can see that \textbf{it is necessary to have our background
P, rho, and T in hydrostatic equilibrium}.  Otherwise what we're evolving is bogus.

\subsection{Temperature equation}
Nothing particularly new to mention here, but we evolve the \emph{temperature
equation}, not the entropy equation.  This takes the form
\begin{equation}
\boxed{
\partial_t T + \bm{u}\cdot\nabla T + (\gamma - 1) T \Div \bm{u} = \\
\frac{1}{\rho C_V}\Div(\rho\chi \nabla T) + 
\frac{1}{\rho C_V}\sum_{ij}\Pi_{ij}\frac{\partial u_i}{\partial x_j}
}.
\end{equation}
I think.  I'm not 100\% sure that the viscous term is right.  But I think it is. The
first two LHS terms are the advection of temperature.  The next term is PdV work.  The
first term on the RHS is the divergence of radiative flux. 
The stress tensor is defined as
\begin{equation}
 \Pi_{ij} = -\mu\left(\partial_{x_i}u_j + \partial_{x_j}u_i 
 - \frac{2}{3}\delta_{ij}\Div\bm{u}\right)\label{stress_tensor},
\end{equation}

But the problem is, this isn't in terms of stuff that we can actually solve in our
set of parameters.  We solve using $\ln \rho$.  The viscous term is OK ($\rho$ gets
absorbed into $\mu$ to become $\nu$.  But the radiative flux term needs to be broken
up.  It turns into:
\begin{equation*}
\begin{split}
\frac{1}{\rho}\Div(\rho\chi \nabla T) = &\underbrace{\chi(\cancelto{0}{\Lap T_0} + \nabla T_0 \cdot\nabla\ln\rho_0) 
+ \nabla\chi\cdot\nabla T_0}_{
\text{source terms}}\\
&+ \underbrace{\chi(\Lap T_1 + \nabla T_0\cdot\nabla\ln\rho_1 + \nabla T_1\cdot\nabla\ln\rho_0)
+ \nabla\chi\cdot\nabla T_1}_{
\text{linear terms}}\\
&+ \underbrace{\chi\nabla T_1\cdot\nabla\ln\rho_1}_{\text{nonlinear term}},
\end{split}
\end{equation*}
where the laplacian of the background temperature field disappears for the 
polytropic case.  This is what we actually have coded up in dedalus for this term.
\textbf{If I want to add in a constant cooling/flux type term, this will enter the
equation set in the ``source terms'' section}.


\section{Notes on Solar Surface Convection Living Review (\cite{nordlund2009})}
On page 11, the buoyancy force is described.  This is the first time this has ever
actually clicked for me.  If we set up an atmosphere in hydrostatic balance, we can
then subtract off $\rho_0 g$ from the momentum equation to see what gravity's
\emph{really} doing.  The background gravity term is cancelled by the background
pressure term.  What's left is the fluctuating pressure term.  Terms with negative
density perturbations (low density regions) have a negative sign out in front
of gravity, so they buoyantly rise.  Terms with positive density perturbations
(high density regions) have a positive sign in front of gravity, and they buoyantly
sink.  Neat.

(Pg. 12) I also want to note that the $(\gamma-1)T\nabla\cdot\bm{u}$ term in our equation is
really a $P(\nabla\cdot\bm{u})$ term, which acts like a $PdV$ work term.  The
reason why it looks os different for us is because of the vact that, for an 
ideal gas, $P/\rho = (\gamma-1)e$, where $e$ is the internal energy, which is just
(I think? I'll verify later) $C_V T$.

(Pg. 17) After talking with Mark a few weeks ago, the whole motivation for this
thermal boundary layer at the top is rooted somewhat in the ionization and recombination
of hydrogen acting as an exceptionally effective cooling term.  The paper describes
this process here: `` Plasma that reaches the layer where a typical photon's
mean free path equals the distance to space has some of its thermal energy carried
away by photons. As a result, the plasma cools, so that hydrogen ions capture electrons
to become neutral hydrogen atoms and, in the process, release a large amount of 
ionization energy that is also carried away to space by photons.  The escaping
photons, as they remove energy, also remove entropy from the plasma that reaches the
surface, producing overdense fluid which is pulled down by gravity.'' (see description
of buoyancy in first paragraph above).  This goes on to state that solar convection
is driven primarily by buoyancy work on cool, dense downflows from the surface.

(Pg. 20) It's mentioned that the \emph{ascending} plasma is roughly perfectly
isentropic but that the \emph{descending} plasma is not.  The overall average structure
is therefore Schwarzschild unstable, but this argument is made through ``smearing out''
the downflows with the upflows.  We don't necessarily want to do that.  If the downflows
persist and carry their low entropy down through the convection zone without mixing,
then we are actually stable (despite not being so on average).

\section{Notes on Turbulent Compressible Convection (\cite{cattaneo1991})}
This is really a pretty nice paper on compressible convection, and some of its basics.
Or maybe I just think that since I already have some idea of what's happening.  In this
paper, convection on a superadiabatic polytrope ($m = 1, \gamma= 5/3$) is studied at
four different values of viscosity $\nu$, although the 4th case is done using a
different type of code and I don't buy any of its results except quantitatively.
Still, 3 good cases are given which are very comparable.  In order to vary $\nu$
but keep everything else in the atmosphere, they happen to have (Pr Ra) = const.

One interesting note is that as they \emph{lowered} Prandtl number, they saw much
more complex flow structures (lots of smaller, more narrow downflows)
and turbulence.  I get that this happens when you lower viscosity, but it's kind of
weird.  When I did 3 different runs (Pr = $(0.5, 0.05, 0.005)$), I found that
lower prandtl number meant thicker structures and fewer downflow lanes rather than
more complex, thin, turbulent structures.  Of course, I held Ra constant.

My main concern is: what's more important in our studies?  The physical parameters
of the atmosphere or the balances of the terms in the equations?  If the former,
then their analysis is more meaningful.  If the latter, then their analysis doesn't
really mean much, because in lowering Pr and raising Ra, they're actually examining
different balances of their equation terms.  But maybe that's their goal, as they
\emph{are} aiming for turbulence.

One other important note that this paper brought up and I had forgotten:
\emph{in a statistically steady state, the convection causes the mean temperature gradient
away from the boundary to approach that of an adiabat}.  They also give an interesting
flux scaling relation in eqn 2.10 that's similar to what Axel's paper did.

OH, and they defined a weird Ra.  I should see how they got it and figure out how it
differs from ours.

When they discuss the Reynolds number, they also discuss the Taylor microscale.  That's
something Mark mentioned to me and it's something I should understand.  It's a much
better prescription for the reynold's number length scale than just the domain length.

The paper concludes with an in-depth and robust (including a point-by-point scatter
plot, kind of cool) description of the fluxes.  They say that there is local and nonlocal
motion in their turbulent convection, but that the nonlocal motions \emph{do not
carry any energy!} They go on to say it's possible that the nonlocal motions carry
magnetic field or angular momentum, but not energy.  Finally, they say future work should
look at higher resolution and lower dissipation.  They also say future work should
crank up the Rayleigh number to see how the power spectrum scales.


\bibliographystyle{plain}
\bibliography{../../biblio.bib}
\end{document}
\grid
