\documentclass[letterpaper,12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[small]{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{bm}
\usepackage{empheq}
\usepackage{natbib}

\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Research Notes}
\rhead{Evan Anders}
\lfoot{Creating an Adiabatic Polytrope}
\cfoot{\thepage}
\rfoot{Feb. 2, 2017}

\newcommand{\FT}{\ensuremath{\text{FT}}}
\newcommand{\grad}{\ensuremath{\nabla}}
\newcommand{\Div}[1]{\ensuremath{\nabla\cdot\left({#1}\right)}}

\begin{document}
\section{Adding internal Heating to a polytrope}
Internally heated polytropes are physically interesting.  In nature, changing opacities
in convection zones (such as the sun) make it so that radiative flux ceases carrying
the sun's luminosity outward, and this acts like a heating term.  The flux is no longer
being carried, so it is deposited in the surrounding material until another sort of flux
(convection) carries it.  So we want to study internally heated convection.

Goluskin wrote a great Springer short on internally heated convection (and RB convection),
and it gave me some great places to start from.  My big takeaway from it is that
\textbf{I always need to start thinking about this problem from an adiabatic polytrope}.
That's what boundary conditions will make sense in light of.

\subsection{Initial state}
I'll always assume that I \emph{start} with an adiabatic polytrope of the form
\begin{equation}
\begin{split}
\rho_0(z) = (z_0 - z)^{m_{ad}} \\
T_0(z) = (z_0 - z),
\end{split}
\end{equation}
where $z_0 \equiv L_z + 1$, with $L_z$ being the depth of the atmosphere,
and $m_{ad} \equiv 1/(\gamma - 1)$, the adiabatic polytropic index.

\subsection{Energetics}
Once I have an initial state defined, I want to set up my new energy equation with
internal heating,
$$
\frac{\partial}{\partial t} \left(\rho c_V T + \rho u^2 / 2 + \rho \phi\right)
+ \Div{\bm{F}_{\text{cond}} + \bm{F}_{\text{conv}}} = \text{IH},
$$
where IH is a source term from internal heating.  Previously, this was in pure conservation
form, and we have a 0 on the RHS.  Now we want to add a basic internal heating term,
and just for simplicity, I'll assume it's constant in \emph{this form of the equation},
so it's a constant term heating the internal energy, it's not a constant term heating
the temperature.  To figure out what we have to do with that, let's define:
$$
\text{IH} \equiv A \equiv \Div{(\text{const} + Az)\hat{z}},
$$
and let's examine an initial, steady, static state, where velocities are zero and there's
no time changes.  This yields:

$$
\Div{\bm{F}_{\text{cond}}} = \Div{(\text{const} + Az)\hat{z}},
$$
or, for a horizontally uniform atmosphere,
$$
F_{\text{cond,z}} = -\kappa \grad T = \text{const} + Az.
$$
To understand what the magnitude of $A$ should be, we must first understand what the first
constant is.  In the absence of internal heating, our polytrope has $\grad T = -1$,
and so in that situation, $\text{const} = \kappa$.  We're now going to add internal heating
to that atmosphere, and we want this to basically be the same knob as $\epsilon$ was in our
previous equations: how vigorous is this convection going to be insofar as how much it
modifies the atmosphere and how much it modifies thermal fluctuations.  To make this a
direct analogue, we need to look at a standard polytrope.  The amount of flux that those
carry in excess of the adiabatic value is $\kappa - (-\kappa\grad T_{ad}) = \kappa(1 - g/c_P)$.
In such atmospheres, $g = m_{ad} + 1 - \epsilon$, and $c_P = \gamma / \gamma - 1 = m_{ad} + 1$,
so we get flux = $\kappa ( 1 - (1 - \epsilon/c_P)) = \kappa \epsilon / c_P$ flux in excess
of the adiabatic.  That seems like a good magnitude to use here, too.


In summary, our internally heated convection's static state should satisfy the following:
\begin{equation}
\boxed{
-\kappa\grad T = \kappa\left(1 + \frac{\epsilon}{c_P}z\right)}.
\end{equation}

I need to do more thorough testing, but this retrieves results about like I'd expect
($\epsilon = 0.5$ gives Ma $\sim 0.1$, and I think $\epsilon = 10^{-4}$ appropriately
scales it down, but I need to test this more).  Hydrostatic equilibrium also needs to be updated to
this new temperature profile, and $T$ needs to be integrated from here.

\subsection{Adding a subadiabatic lower layer}
In the previous section, we derived the quantity of flux that we need to push through the
system, which is \emph{perfectly adiabatic} at the bottom of the atmosphere ($z = 0$),
but which then begins to increase as you move higher in the atmosphere, as a result of
the internal heating.  The important thing to note here is that the entire atmosphere
must carry an amount of flux equal to or greater than the adiabatic reference flux,
and that means that the whole layer is either unstable to convection or marginally unstable.

...so what if we play with the total amount of flux going through the system?  What if
we now say:
\begin{equation}
-\kappa\grad T = \kappa\left([1 + Q] + \frac{\epsilon}{c_P}z\right).
\end{equation}
Now we have the adiabatic value ($\kappa$) modified by some amount
($\kappa Q$).  If $Q \geq 0$, the whole system is unstable to convection, and
as $Q$ increases in that direction we just get a more and more unstable system
that's carrying more of the total flux it MUST carry through convection.  But if $Q$
is negative?  That's where things get interesting.

There's a limiting case here: if $Q < -(\epsilon L_z / c_P)$, then the entire atmosphere
is carrying a subadiabatic amount of flux, and we don't need to worry about convection.
But if we set up $0 > Q > -(\epsilon L_z / c_P)$, we have \emph{parts} of the atmosphere
where the total flux that must be carried is subadiabatic (in the bottom of the atmosphere),
and other parts where the total flux that must be carried is superadiabatic (at the top of
the atmosphere).

So if we define:
\begin{equation}
Q \equiv -f\frac{\epsilon}{c_P} L_z,
\end{equation}
where $f$ is a number between 0 and 1, we can tune what fraction ($f$) of the atmosphere
is carrying a subadiabatic amount of flux, and what fraction (1 - $f$) of the atmosphere
is carrying a superadiabatic amount of flux.

In theory, this is a parameter that we can straight up adjust to increase the size of 
a subadiabatic layer at the bottom of the atmosphere.

\subsubsection{A further extension: ``genuine" multitropes}
The thing that we've learned here is that the flux that's being driven through the
atmosphere, \emph{with respect to the adiabatic amount}, is what determines the
dynamics of convection.  with that being said, I'm basically positive multitropes can
just be defined as "region of subadiabatic flux" attached to "region of superadiabatic
flux."  Which means that we might \emph{not} need all those NCCs, and we might just
be able to create multitropes out of adiabatic atmospheres with the proper boundary
conditions and the proper shape of internal heating.


\end{document}
